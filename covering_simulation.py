import numpy as np
from covering import quadratic_covering, cond_entropy, quadratic_covering_proximal, quadratic_covering_FW
from covering import cmim2

## Test quadratic covering on mixtures of Gaussian
## N: Number of samples
## m: number of classes
## d: dimension
## c: number of active variables

N = 100
m = 5
d = 50
c = 10
p = 0.00001

## generating random class variables
Y = np.random.choice(m, size=N)

## generating binary variables using thresholded Gaussian on first c coordinates
X0 = np.random.normal(0, 1, size= (N,d))
X = np.random.choice(2, size = (N,d))
mj = 1.75 * np.random.choice((-1, 1), size=[m,c])
X0 = np.random.normal(0, 1, size=[N, c])  # + np.random.normal(0, 1, c)
for j in range(N):
    X[j, :c] = ((X0[j, :] + mj[Y[j], :]) > 0)

noise = np.random.choice(2, p=(p, 1-p), size=(N,d))
X = noise * X + (1-noise)*(1-X)

## computing mutual information
tr = 'HResidual'
entropy0, entropy, selection = cond_entropy(Y, X, type_result=tr, selectThreshold=0.99)
print(entropy[:10, :10])
print(entropy0)
print(selection)
#entropy, x = cond_entropy_alt(Y, X, type_result=tr)[0]
#J = quadratic_covering(entropy, c, solver='SCS', MI=MI)
#J = quadratic_covering_proximal(entropy, c, type_result=tr)
J = cmim2(entropy, entropy0, c)
#print(x)

