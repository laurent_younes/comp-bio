import numpy as np
import pandas as pd
import pprint
from divergence import learnDivergenceScalar, divergenceTransform
from rank_space import computeQuantiles

clinical = pd.read_csv("Data/rna_clinicalv0.7.0.csv", index_col="ppcg_donor_id")
#ppcg_donor_id = pd.read_csv("Data/ppcg_donor_id.csv")["x"].values

# ppcg_count = pd.read_csv("Data/ppcg_rna_count.csv", index_col = "Unnamed: 0").T
# ppcg_meta_dict = {}
# for i,ind in enumerate(ppcg_count.index.values):
#     donor_id = ppcg_donor_id[i]
#     try:
#         ppcg_meta_dict[ind] = clinical.loc[donor_id].values
#     except: # We throw away all the sample in the RNA-seq data of which the ppcg_donor_id does not match that provided in the clinical variable file.
#         continue
#
#

counts = pd.read_csv('Data/PPCG_RNAseqData_Tier2.csv', index_col=0).T
ppcg_meta_dict = {}
count_id = []
for i,ind in enumerate(counts.index.values):
    donor_id = counts.index[i][:8]
    try:
        ppcg_meta_dict[ind] = clinical.loc[donor_id].values
    except:  # We throw away all the sample in the RNA-seq data of which the ppcg_donor_id does not match that provided in the clinical variable file.
        continue
# for k in range(counts.index.shape[0]):
#     count_id.append(counts.index[k][:8])
# count_id, I = np.unique(count_id, return_index=True)
# counts = counts.iloc[I]
# counts = counts.set_index(pd.Index(count_id))

ppcg_meta = pd.DataFrame(ppcg_meta_dict, index=clinical.columns).T
inboth = counts.index.intersection(ppcg_meta.index)
counts = counts.loc[inboth, :]
ppcg_meta = ppcg_meta.loc[inboth]

baseline = 'benign'
alpha = 0.005
pp = pprint.PrettyPrinter(indent=4)
if baseline == 'benign':
    benign = pd.read_csv('Data/baseline_benign_samples.txt', index_col=0)
    benign = benign.loc[benign.loc[:, 'baseline'], :]
    I = counts.index.intersection(benign.index)
    normal = counts.loc[I, :]
else:
    normal = pd.read_csv('Data/ppcg_baseline.csv', index_col=0).T
z = computeQuantiles(normal.to_numpy())
z2 = z['Without_zeros']
# pp.pprint(z2)
dvg = learnDivergenceScalar(z2, alpha=alpha)
# ppcg_count = pd.read_csv("Data/ppcg_rna_count.csv", index_col = "Unnamed: 0")
pp = computeQuantiles(counts.to_numpy())
ppcg_ranks = pp['Without_zeros']
ppcg_div_np = divergenceTransform(ppcg_ranks, dvg)
ppcg_div = pd.DataFrame(data=ppcg_div_np.T, index=counts.T.index, columns=counts.T.columns)
ppcg_div.T.to_csv(f'Data/ppcg_divergence_{baseline}_{alpha:.3f}.csv')
print('Computed divergence')
#ppcg_normalized = pd.read_csv("Data/ppcg_rna_normalized.csv", index_col = "Unnamed: 0").T
#assert (np.array([i.split("_")[0][:-1] for i in ppcg_count.index.values]) == ppcg_donor_id).sum()
#assert (np.array([i.split("_")[0][:-1] for i in ppcg_normalized.index.values]) == ppcg_donor_id).sum()

# ppcg_div = pd.read_csv("ppcg_rna_count.csv", index_col = "Unnamed: 0").T
# ppcg_meta_dict = {}
# for i,ind in enumerate(ppcg_div.index.values):
#     donor_id = ppcg_donor_id[i]
#     try:
#         ppcg_meta_dict[ind] = clinical.loc[donor_id].values
#     except: # We throw away all the sample in the RNA-seq data of which the ppcg_donor_id does not match that provided in the clinical variable file.
#         continue
# ppcg_meta = pd.DataFrame(ppcg_meta_dict).T
# ppcg_meta.columns = clinical.columns.values #This is the clinical data
# ppcg_div_meta = ppcg_div.loc[ppcg_meta.index.values]
# ppcg_normalized = ppcg_normalized.loc[ppcg_meta.index.values]
# assert (set(ppcg_meta.index).issubset(set(ppcg_div.index)))
#assert (ppcg_meta.index.values == ppcg_normalized.index.values).sum() == len(ppcg_meta)

#This is the column for the metastasis.
#mets: there is metastasis;
#no mets: there is NO metastasis
#not applicable: benign samples (used as reference for divergence)
#missing: missing labels that we currently throw away for the analysis
# ppcg_meta["mets_ind"].value_counts()
ppcg_meta.to_csv('Data/ppcg_metadata.csv')
# ppcg_div.to_csv((f'Data/ppcg_divergence_{baseline}_{alpha:.3f}.csv'))


# baseline = ppcg_div_meta.loc[ppcg_meta["mets_ind"] == "not applicable"]
# dataf = ppcg_div_meta.loc[(ppcg_meta["mets_ind"] == "no mets") | (ppcg_meta["mets_ind"] == "mets")]
# baseline.T.to_csv("Data/ppcg_baseline_met_div.csv")
# dataf.T.to_csv("Data/ppcg_div_met.csv")
#
# baseline = ppcg_div_meta.loc[ppcg_meta["relapse_ind"] == "not applicable"]
# dataf = ppcg_div_meta.loc[(ppcg_meta["relapse_ind"] == "no relapse") | (ppcg_meta["relapse_ind"] == "relapsed")]
# baseline.T.to_csv("Data/ppcg_baseline_rel_div.csv")
# dataf.T.to_csv("Data/ppcg_div_rel.csv")
