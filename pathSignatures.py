import numpy as np
import scipy as sp
from scipy.sparse import coo_matrix
from scipy.spatial.distance import pdist, squareform
import pandas as pd
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
from sklearn.manifold import SpectralEmbedding, LocallyLinearEmbedding, TSNE
from sklearn.cluster import KMeans
from scipy.stats import chisquare, chi2_contingency
from methylation import plotHist
from mpl_toolkits.mplot3d import Axes3D
from pylatex import Document, Section, Subsection, Subsubsection, Tabular, Math, TikZ, Axis, \
    Plot, Figure, Matrix, Alignat
from pylatex.utils import italic
import os


class options:
    def __init__(self, ttype='breast', stype = 'source'):
        if ttype == 'breast':
            self.pheno_file = 'BREAST/RNASeq/TCGA/TCGA_Pheno.csv.gz'
            if stype == 'source':
                self.sign_file = 'source/result/BreastSourceSig.txt'
                self.data_file = 'source/data/BreastCmbSourceBinary.txt'
            elif stype == 'pair':
                self.sign_file = 'Pair/result/BreastPairSig.txt'
                self.data_file = 'Pair/data/BreastCombPairsBinary.txt'
            elif stype == 'target':
                self.sign_file = 'target/result/BreastTargetSig.txt'
                self.data_file = 'target/data/BreastCmbTargetBinary.txt'
            self.subtype = ('PAM50_mRNA_nature2012','ER_Status_nature2012')
        elif ttype == 'lung':
            self.pheno_file = 'LUNG/RNASeq/TCGA_Adenocarcinoma/TCGA_Pheno.csv.gz'
            if stype == 'source':
                self.sign_file = 'source/result/LungSourceSig.txt'
                self.data_file = 'source/data/LungCmbSourceBinary.txt'
            elif stype == 'pair':
                self.sign_file = 'Pair/result/LungPairSig.txt'
                self.data_file = 'Pair/data/LungCombPairsBinary.txt'
            elif stype == 'target':
                self.sign_file = 'target/result/LungTargetSig.txt'
                self.data_file = 'target/data/LungCmbTargetBinary.txt'
            self.subtype = ('pathologic_stage','tobacco_smoking_history')



if __name__ == "__main__":
    emb = "TSNE"
    doc = Document()


    for ttype in ('breast', 'lung'):
        for stype in ('source','pair', 'target'):
            opt = options(ttype=ttype, stype=stype)
            df_sign = pd.read_csv(MechPred+'/USER/QIAN/PathwayCommonsAnalysis/DATA_V2/95%cover/'+opt.sign_file, sep='\t')
            df_data = pd.read_csv(MechPred+'/USER/QIAN/PathwayCommonsAnalysis/DATA_V2/95%cover/' + opt.data_file, sep='\t')
            df_pheno = pd.read_csv(MechPred+'/DATA/' + opt.pheno_file, index_col=1)
            df_pheno = df_pheno.reindex(df_data.columns)

            sample_type = df_pheno['sample_type'].to_numpy(dtype=str)


            all_var_sig = np.array(np.dot(df_sign, df_data))
            best_var_sig = np.argmax(all_var_sig.mean(axis=1)) + 1

            #selected_var = df_sign.sum(axis=0) > 0
            selected_var = df_sign.loc[best_var_sig,:].to_numpy(dtype=bool)
            #selected_var = np.ones(df_sign.shape[1], dtype = bool)

            a = 1
            b = 3
            c = (a+b)**(a+b)/(a**a * b**b)
            Xsel = df_data.loc[selected_var, :]
            X0 = Xsel.to_numpy(dtype=float)
            p = X0.mean(axis=1)
            w = c * p**a * (1-p)**b
            X = X0 * p[:, np.newaxis]

            nclusters = 6
            if emb == "spectral":
                embedding = SpectralEmbedding(n_components=nclusters) # affinity='precomputed')
                #aa = squareform(pdist(X.T, metric='jaccard'))
                #aa = np.exp(-aa/10)
                s2 = embedding.fit_transform(X.T)
            elif emb == "TSNE":
                embedding = TSNE(n_components=3, learning_rate= 20, method='exact') # metric='jaccard')
                s2 = embedding.fit_transform(X.T)
            elif emb == 'LLE':
                embedding = LocallyLinearEmbedding(n_components=nclusters, n_neighbors= 50, method='ltsa')  # affinity='precomputed')
                s2 = embedding.fit_transform(X.T)
            #X -= m[:, np.newaxis]
            #N = (X**2).sum(axis=0) + 2
            #X /= np.sqrt(np.log(N))[np.newaxis,:]
            print(X.shape, df_pheno.shape)
            print(s2.shape)

            with doc.create(Section('Tissue: ' + ttype)):
                with doc.create(Subsection('Parameters')):
                    doc.append('Embedding: {0:s},   Tissue: {1:s}, Variable: {2:s}\n'.format(emb, ttype, stype) )
                    doc.append('{0:d} subjects; {1:d} variables'.format(X.shape[1], X.shape[0]))

                v0 = 0
                v1 = 1
                v2 = 2
                colors = ['r', 'g', 'b', 'm', 'k', 'y','k','0.75']



                kmeans = KMeans(n_clusters=nclusters, random_state=0).fit(s2)
                cls = kmeans.predict(s2)

                doc.append(' ')


                with doc.create((Subsection('Clusters'))):
                    clusters_filename = ttype + '_' + stype + '_'+ 'clusters.png'
                    with doc.create(Figure(position='h!')) as clusters:
                        clusters.add_image(clusters_filename, width='9cm')
                        clusters.add_caption('Clusters')

                    with doc.create(Tabular('ll')) as tab:
                        tab.add_row(['Cluster', 'Top variables'])
                        for k in range(nclusters):
                            j = np.argsort(X0[:, cls==k].mean(axis=1))
                            row = []
                            str0 = 'Cluster {0:d}:'.format(k+1)
                            row += ['{0:d}'.format(k+1)]
                            str1 = ' '
                            for jj in range(min(5, len(j))):
                                str1 += '{0:20s}'.format(Xsel.index[j[-1-jj]])
                            row += [str1]
                            print(str0 + str1)
                            tab.add_row(row)


                if s2.shape[1] > 2:
                    plt.pause(0.1)
                    fig = plt.figure()
                    ax = fig.add_subplot(111, projection='3d')
                    for k in range(nclusters):
                        st = (cls==k)
                        ax.scatter(s2[st, v0], s2[st, v1], s2[st, v2], label= 'Cluster {0:d}'.format(k+1),
                                   color=colors[k], marker='o', alpha=0.5)

                    # stNan = statusData.isna()
                    # ax.scatter(s2[stNan, v0], s2[stNan, v1], s2[stNan, v2], label='missing', color=colors[-1], marker='o', alpha=0.5)
                    ax.legend()
                    plt.title('Clusters')
                    plt.axis('off')
                    plt.savefig('Reports/'+clusters_filename)
                    plt.pause(0.1)
                else:
                    fig = plt.figure()
                    for k in range(nclusters):
                        st = (cls==k)
                        plt.scatter(s2[st, v0], s2[st, v1], s2[st, v2], label= 'Cluster {0:d}'.format(k),
                                   color=colors[k], marker='o', alpha=0.5)
                    #           alpha=0.5)
                    plt.legend()
                    plt.title('Clusters')
                    plt.axis('off')
                    plt.pause(0.1)

                for statusColumn in opt.subtype:
                    status_filename = ttype + '_' + stype + '_'+ statusColumn +'.png'
                    with doc.create(Figure(position='h!')) as statusFig:
                        statusFig.add_image(status_filename, width='9cm')
                        statusFig.add_caption(statusColumn)

                    statusData = df_pheno[statusColumn]
                    classes = np.unique(df_pheno[statusColumn].dropna())
                    nc = classes.size
                    status = []
                    for cl in classes:
                        status.append(statusData == cl)
                    if s2.shape[1] > 2:
                        fig = plt.figure()
                        ax = fig.add_subplot(111, projection='3d')
                        for k,st in enumerate(status):
                            ax.scatter(s2[st, v0], s2[st, v1], s2[st,v2], label = classes[k], color=colors[k], marker='o', alpha=0.5)

                        #stNan = statusData.isna()
                        #ax.scatter(s2[stNan, v0], s2[stNan, v1], s2[stNan, v2], label='missing', color=colors[-1], marker='o', alpha=0.5)
                        ax.legend()
                        plt.title(statusColumn)
                        plt.axis('off')

                        plt.pause(0.1)
                    else:
                        fig = plt.figure()
                        #ax = fig.add_subplot(111, projection='2d')
                        for k, st in enumerate(status):
                            plt.scatter(s2[st, v0], s2[st, v1], label=classes[k], color=colors[k], marker='o',
                                       alpha=0.5)

                        stNan = statusData.isna()
                        #plt.scatter(s2[stNan, v0], s2[stNan, v1], label='missing', color=colors[-1], marker='o',
                        #           alpha=0.5)
                        plt.legend()
                        plt.title(statusColumn)
                        plt.axis('off')
                        plt.pause(0.1)
                    plt.savefig('Reports/'+status_filename)

                    cmatrix = np.zeros((nclusters,nc))
                    pval = np.zeros(nc)
                    prior = np.zeros(nclusters)
                    freq = np.zeros((nclusters, nc), dtype=int)
                    # for l in range(nc):
                    #     prior[l] = (statusData == classes[l]).sum()
                    for k in range(nclusters):
                        for l in range(nc):
                            freq[k,l] = np.logical_and(cls==k, statusData == classes[l]).sum()
                            cmatrix[k,l] = 100 * freq[k,l]/(cls==k).sum()
                    for k in range(nclusters):
                        prior[k] = (cls==k).sum()
                    prior /= prior.sum()
                    for l in range(nc):
                        pval[l] = chisquare(f_obs=freq[:,l], f_exp=prior*(statusData == classes[l]).sum())[1]
                        #cmatrix[k,nc] = 100 * np.logical_and(cls==k, stNan).sum()/(cls==k).sum()

                    with doc.create(Subsection(statusColumn)):
                        with doc.create(Subsubsection('Subtype description')):
                            with doc.create(Tabular('ll')) as table0:
                                row = ['Subtype', 'Description']
                                table0.add_row(row)
                                for l in range(nc):
                                    row = [l+1, classes[l]]
                                    table0.add_row(row)

                        with doc.create(Subsubsection('Subtypes in clusters')):
                            with doc.create(Tabular('lc'+'c'*nclusters+'c')) as table:
                                row = ['Subtype','Samples']
                                str1 = '{0:75}'.format('')
                                for k in range(nclusters):
                                    str1 += "{0:10}".format("Cluster " + "{0:d}".format(k+1))
                                    row += ["Cluster " + "{0:d}".format(k+1)]
                                row += ['p-value']
                                table.add_row(row)
                                print(str1)

                                row = ['','']
                                str1 = '{0:75}'.format('Size')
                                for k in range(nclusters):
                                    str1 +=  "{0:10d}".format((freq[k,:]).sum())
                                    row += [(freq[k,:]).sum()]
                                row += ['']
                                table.add_row(row)
                                print(str1)

                                #str += "missing"
                                row = ['Prior Probabilities', '']
                                str1 = '{0:75}'.format('Prior Probabilities: ')
                                for l in range(nclusters):
                                    str1 += '{0:10.2f}'.format(prior[l])
                                    row += ['{0:.2f}'.format(prior[l])]
                                row += ['']
                                #table.add_row(row)
                                print(str1)

                                for l in range(nc):
                                    row = [l+1, "{0:d}".format((freq[:,l]).sum())]
                                    str1 = '{0:50}'.format(classes[l]) + '{0:25}'.format("{0:d} samples".format((freq[:,l]).sum()))
                                    for k in range(nclusters):
                                        str1 += "{0:10d}".format(freq[k,l])
                                        row += ["{0:d}".format(freq[k,l])]
                                    str1 += "      -- pval = {0:5.4f}".format(pval[l])
                                    row += ['{0:.3f}'.format(pval[l])]
                                    table.add_row(row)
                                    print(str1)


                        str1 = 'chi2 contingency: {0:4f}'.format(chi2_contingency(freq)[1])

                        print(str1+'\n')
                        doc.append(str1+'\n')

    doc.generate_pdf('Reports/Summary', clean_tex=False)
    plt.ioff()

    plt.show()

