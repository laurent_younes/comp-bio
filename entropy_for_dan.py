## Trains an MRF on binary data (+1 or -1) with pairwise interactions
## the two functions are train_MRF for an SGD implementation and
## train_MRF_exact for exact optimization (not to be used in large dimensions).
## X is an Nxd matrix with N: number of samples and d: number of variables
##
## Returns parameters in the form (par0, par1) where par0 is d-dimensional and
## corresponds to uni-variate statistics and par1 has size d(d-1)/2
# for the upper triangular array that encodes interactions

import numpy as np
from numba import jit, int64
from scipy.optimize import minimize

@jit(nopython=True)
def probExact(par0, par1):
    d = par0.shape[0]
    x = np.ones((2,1))
    x[1,0] = -1
    n0 = 2
    for k in range(1,d):
        _x = np.zeros((2*n0,k+1))
        _x[:n0,:k] = x
        _x[n0:,:k] = x
        _x[:n0,k] = 1
        _x[n0:,k] = -1
        n0 *= 2
        x = _x

    E = np.dot(x,par0)
    i = 0
    for k in range(d):
        for l in range(k+1,d):
            E += x[:,k]*x[:,l]*par1[i]
            i+=1

            #        E += x[:,k] * (x[:,(k+1):]*(par1[k,(k+1):])[np.newaxis,:]).sum(axis=1)

    P = E-E.max()

    P = np.exp(P)
    P /= P.sum()

    return P, E, x


@jit(nopython=True)
def logLikelihood(par, X, U):
    d = X.shape[1]
    N = X.shape[1]

    # compute sufficient statistic
    if U is None:
        H0 = X.sum(axis=0)/N
        H1 = np.zeros(((d*(d-1))//2))
        ij=0
        for i in range(d):
            for j in range(i + 1, d):
                H1_ = 0
                for k in range(N):
                    H1_ += X[k, i] * X[k, j]
                H1[ij] = H1_/N
                ij+=1
        U = np.concatenate((H0,H1))

    par0 = par[:d]
    par1 = par[d:]
    P, E, x = probExact(par0, par1)
    pow2 = 2**(np.arange(d))
    L = 0
    for k in range(N):
        IX = 0
        for j in range(d):
            IX += (X[k,j]<0) * pow2[j]
        L += np.log(P[IX])

    g0 = np.zeros(d)
    for k in range(d):
        for j in range(x.shape[0]):
            g0[k] += x[j,k] * P[j]

    g1 = np.zeros((d*(d-1))//2)
    kl = 0
    for k in range(d):
        for l in range(k+1, d):
            for j in range(x.shape[0]):
                g1[kl] += x[j, k] * x[j,l] * P[j]
            kl += 1
    #g1 = (x[:,:,np.newaxis]*x[:,np.newaxis,:]*P[:, np.newaxis,np.newaxis]).sum(axis=0)
    #g1 = g1[np.triu_indices(d,1)]
    #print(-L)
    return -L, -U+np.concatenate((g0, g1))



## Trains an MRF on binary data with pairwise interactions
## X is an Nxd matrix with N: number of samples and d: number of variables
## pr, npr: parameters for Dirichlet prior in estimating sufficient statistics
## in order to avoid singular solutions
## Training uses maximum likelihood with stochastic gradient descent with at most
## n_iter steps
## if size_limit > 0, the training set is sub-sampled to that size.
##
## Returns optimal parameters in the form (par0, par1) where par0 is d-dimensional and
## corresponds to uni-variate statistics and par1 has size d(d-1)/2
# for the upper triangular array that encodes interactions
@jit(nopython=True)
def trainMRF(X, pr = 0.5, npr = 1, n_iter=100000, sizeLimit = -1, replacement=False):
    N = X.shape[0]
    if sizeLimit>0:
        if not replacement and N > sizeLimit:
            I = np.random.permutation(N)
            X = X[I[:sizeLimit], :]
            N = sizeLimit
        else:
            I = np.random.randint(0,N,sizeLimit)
            X = X[I,:]
            N = sizeLimit

    # d = X.shape[1]
    # if d<21:
    #     par0, par1 = trainMRF_exact(X, pr, npr)
    #     return par0, par1

    prior = 2*pr - 1

    d2 = (d*(d-1)//2)
    H0 = (X.sum(axis=0) + npr*prior)/(N+npr)
    H1 = np.zeros((d,d))
    for i in range(d):
        for j in range(i+1,d):
            H1[i,j] = ((X[:,i]*X[:,j]).sum(axis=0) +npr*prior**2)/(N+npr)
    # X1 = X[..., np.newaxis]
    # X2 = X[:, np.newaxis, :]
    # H1 = ((X1*X2).sum(axis=0)+npr*prior**2)/(N+npr)


    par0 = np.zeros(d)
    par1 = np.zeros((d,d))

    step = .01
    a = 0.001
    x = np.ones(d, dtype=int64)
    hx0 = np.zeros(d)
    hx1 = np.zeros((d,d))
    hx0m = np.zeros(d)
    hx1m = np.zeros((d,d))

    for it in range(n_iter):
        uni = np.random.uniform(0,1,d)
        for i in range(d):
            u0 = np.exp(2*(par0[i] + (par1[i,(i+1):]*x[(i+1):]).sum()+ (par1[:i,i]*x[:i]).sum()))
            if uni[i] < u0/(1+u0):
                x[i] = 1
                hx0[i] = 1
                hx1[i, (i+1):] = x[i+1:]
                hx1[:i, i] = x[:i]
            else:
                hx0[i] = -1
                hx1[i, (i+1):] = -x[(i+1):]
                hx1[:i, i] = -x[:i]
                x[i] = -1

        s = (step/(1+a*it))
        par0 += s * (H0-hx0)
        hx0m += (hx0-hx0m)/(it+1)
        hx1m += (hx1-hx1m)/(it+1)
        par1 += s * (H1 - hx1)
        #print('par1:', par1[tr1])
        #print('x:', x)
        #print('hx1:', hx1[tr1])
        # prs = "it: {0:d} par0 ".format(it)
        # for u in par0:
        #     prs += "{0:2f} ".format(u)
        # print(prs)
        # print(par1)

    print('H0:', ((H0-hx0m)**2).sum(), 'H1: ', ((H1-hx1m)**2).sum())
    # print('H0:', H0, 'H1: ', H1)
    # print('H0:', hx0m, 'H1: ', hx1m)
    # print(hx1m)

    par1_ = np.zeros(d2)
    ij = 0
    for i in range(d):
        for j in range(i+1,d):
            par1_[ij] = par1[i,j]
            ij += 1

    return par0, par1_

@jit(nopython=True)
def get_stats(X, pr = 0.5, npr = 1):
    prior = 2*pr - 1
    H0 = (X.sum(axis=0) + npr*prior)/(N+npr)
    H1 = np.zeros((d*(d-1))//2)
    k = 0
    for i in range(d):
        for j in range(i + 1, d):
            H1[k] = ((X[:, i] * X[:, j]).sum(axis=0) +npr*prior**2)/ (N+npr)
            k += 1
    # H1 = ((X[:, :, np.newaxis]*X[:, np.newaxis, :]).sum(axis=0)+npr*prior**2)/(N+npr)
    # H1 = H1[tr1]

    return np.concatenate((H0,H1))


## Trains an MRF on binary date with pairwise interactions
## X is an Nxd matrix with N: number of samples and d: number of variables
## pr, npr: parameters for Dirichlet prior in estimating sufficient statistics
## in order to avoid singular solutions
## Training uses maximum likelihood with exact evaluations and is suitable for small d
## Returns optimal parameters in the form (par0, par1) where par0 is d-dimensional and
## corresponds to bi-variate statistics and par1 is upper triangular and encodes interactions
def trainMRF_exact(X, pr = 0.5, npr = 1):
    N = X.shape[0]
    d = X.shape[1]
    d2 = (d * (d - 1)) // 2

    ## sufficient statistics
    # prior = 2*pr - 1
    # H0 = (X.sum(axis=0) + npr*prior)/(N+npr)
    # H1 = np.zeros(d2)
    # k = 0
    # for i in range(d):
    #     for j in range(i + 1, d):
    #         H1[k] = ((X[:, i] * X[:, j]).sum(axis=0) +npr*prior**2)/ (N+npr)
    #         k += 1
    # H1 = ((X[:, :, np.newaxis]*X[:, np.newaxis, :]).sum(axis=0)+npr*prior**2)/(N+npr)
    # H1 = H1[tr1]

    H = get_stats(X, pr, npr)

    par0 = np.zeros(d)
    par1 = np.zeros(d2)
    par = np.concatenate((par0, par1))

    res = minimize(logLikelihood, par, args=(X,H),method='BFGS', jac=True, tol=1e-5, options={'gtol':1e-4})
    #print('BFGS:', res.fun)
    par = res.x
    par0 = par[:d]
    par1 = par[d:]

    return par0, par1


## Entropy for MRF
## the normalizing constant is estimated as the integral of its derivative
# for an interpolation between independent data and the atrget interactions
@jit(nopython=True)
def entropyMRF(par0, par1_, n = 10, ns=10000, exact=False):
    d = par0.shape[0]
    if exact == True or d<21:
        return entropyExact(par0,par1_)
    t = np.linspace(0,1,num=n+1)
    T = len(t)
    x = np.ones(d)
    tr1 = np.triu_indices(d,1)
    par1 = par1_[tr1]
    Emean = np.zeros(T)
    Emean0 = np.zeros((T,d))
    for k, _t in enumerate(t):
        for it in range(ns):
            uni = np.random.uniform(0,1,d)
            for i in range(d):
                E = 2 * (par0[i] + _t*((par1[i, (i + 1):] * x[(i + 1):]).sum() + (par1[:i, i] * x[:i]).sum()))
                u0 = 1/(1+np.exp(-E))
                x[i] = 2*(uni[i] < u0) - 1
            E = (par1[tr1]*(x[np.newaxis,:]*x[:,np.newaxis])[tr1]).sum()
            Emean[k] += (E-Emean[k])/(it+1)
            Emean0[k, :] += (x -Emean0[k,:])/(it+1)


    p0 = (Emean0[0,:]+1)/2 #np.exp(par0)/(np.exp(par0) + np.exp(-par0))
    Entropy0 = - (p0*np.log(p0)).sum() - ((1-p0)*np.log(1-p0)).sum()
    Entropy = - (par0*Emean0[-1,:]).sum() - Emean[-1] + (np.log(np.exp(-par0)+np.exp(par0))).sum()
    for k in range(T-1):
        Entropy += 0.5*(Emean[k+1] + Emean[k])*(t[k+1]-t[k])

    l2 = np.log(2)
    return Entropy/l2, Entropy0/l2


## Computes entropy after fitting an MRF on the maxVar variables with highest variance
def entropyMRF_fit(X, maxVar = 16, n_sde=5000, n_sim=10000, sizeLimit=-1):
    p = X.mean(axis=0)
    s = p * (1 - p)
    if X.shape[1] > maxVar:
        sth = max(np.sort(s)[-maxVar - 1], 0.001)
    else:
        sth = 0.001
    p1 = np.minimum(np.maximum(p[s < (sth + 1e-10)], 1e-10), 1 - 1e-10)
    H1 = -(p1 * np.log2(p1)).sum() - ((1 - p1) * np.log2(1 - p1)).sum()
    keep = (s > sth)
    X_ = 2 * X[:, keep] - 1
    par = trainMRF(X_, npr=1, n_iter=n_sde, sizeLimit=sizeLimit)
    H_, H0 = entropyMRF(par[0], par[1], n=n_sim)
    result = {'entropy':H_+H1}
    return result

@jit(nopython=True)
def entropyExact(par0, par1):
    P, E, x = probExact(par0, par1)
    I= P > 1e-100

    H = - (P[I]*np.log(P[I])).sum()

    p = (1+(x * P[:,np.newaxis]).sum(axis=0))/2
    I= p > 1e-100
    H0 = - (p[I]*np.log(p[I]) + (1-p[I])*np.log(1-p[I])).sum()
    l2 = np.log(2)
    return H/l2, H0/l2






if __name__=="__main__":
    N = 1000
    d = 10
    u = np.random.uniform(0, 1, size=(N,d))
    X = (2*(u>0.5)-1).astype(int)

    print('SGD')
    par0, par1 = trainMRF(X, n_iter=100000)
    print(par0, '\n')
    print(par1, '\n')

    print('exact computation (not recommended for d>20)')
    par0, par1 = trainMRF_exact(X)
    print(par0, '\n')
    print(par1, '\n')
