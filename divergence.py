import numpy as np
from sklearn.neighbors import NearestNeighbors


def estimateSupport(X, alpha = 0.01, gamma=.1, q = 0.95):
### X is an Nxd array
### Support estimation based on k nearest neighbors where k
### is the smallest between gamma*X.shape[0] and the value allowing for a ration of outliers less than alphs
### If d=1, the smallest interval containing the support is returned
    n = X.shape[0]
    d = X.shape[1]
    ell = 1 + int(np.floor(gamma * n))

    nbrs = NearestNeighbors(n_neighbors=ell).fit(X)
    distances, indices = nbrs.kneighbors(X)
    r = np.quantile(distances, q, axis=0)
    inNN = np.zeros(indices.shape)
    for j in range(indices.shape[1]):
        inNN[distances[:,j] < r[j]+1e-10,j] = 1
    sNN = inNN.sum(axis=0)/n
    k = 0
    while k < sNN.shape[0]-1:
        if sNN[k] > 1-alpha:
            k+=1
        else:
            break

    distances = distances[:, k]
    r = np.quantile(distances, q)
    J = np.nonzero(distances < r+1e-10)[0]
    distances = distances[J]

    res = {}
    res['indices'] = J
    res['centers'] = X[J, :]
    res['radii'] = distances
    if d==1:
        # imax = np.argmax(distances)
        # res['upper'] = res['centers'][imax] + distances[imax]
        res['upper'] = np.minimum((res['centers'] + distances).max(), 1)
        # imin = np.argmin(distances)
        res['lower'] = np.maximum((res['centers'] - distances).min(), 0)
    else:
        res['upper'] = None
        res['lower'] = None
    return res

# def estimateSupportScalar(X0, , gamma = .05, alpha = 0.95):
#     n = X0.shape[0]
#     ell = 1 + int(np.floor(gamma * n))
#     y0 = np.sort(X0)


def learnDivergenceScalar(X0, gamma = .1, alpha = 0.001):
    g = X0.shape[1]
    res = []
    # res['upper'] = np.zeros(g)
    # res['lower'] = np.zeros(g)
    # res['centers'] = []
    # res['indices'] = []
    # res['radii'] = []

    for i in range(g):
        support = estimateSupport(X0[:, i][:, None], gamma=gamma, alpha=alpha)
        res.append(support)
        # res['upper'][i] = support['upper']
        # res['lower'][i] = support['lower']

    return res


def divergenceTransform(X0, dvg, ternary = False, forceBallUnion=False):
    ## X0 must be either an array (Nsub, Nvar) or (Nsub, Nvar, dim) where Nsub is the number of subjects,
    # Nvar the number of variables and dim the dimension of each variable (taken as 1 in the former case). dim
    # must coincide with the dimension of the transforms
    if X0.ndim == 2:
        X = X0[:,:,None]
    else:
        X = X0

    if not isinstance(dvg, list):
        dvg_ = [dvg]
    else:
        dvg_= dvg

    if X.shape[2] != dvg_[0]['centers'].shape[1]:
        print(f"Cannot compute divergence transforms with shapes {X.shape} and {dvg_[0]['centers'].shape}")
        return None

    dim = X.shape[2]
    g = X.shape[1]

    res = np.zeros((X.shape[0], X.shape[1]))
    if dim == 1 and not forceBallUnion:
        ## scalar transform applied to each row of X
        for i in range(g):
            res[:, i] = (X[:, i, 0] > dvg[i]['upper']).astype(int) - (X[:, i, 0] < dvg[i]['lower']).astype(int)
        if not ternary:
            res = np.abs(res)
    else:
        for i in range(g):
            dst = (np.sqrt(((X[:, [i], :] - dvg[i]['centers'][None,:,:])**2).sum(axis=2))
                   - dvg[i]['radii'][None, :]).max(axis=1)
            res[:,i] = (dst < 0).astype(int)

    return res

