import numpy as np
import pandas as pd


def makeColorTable(X, cNames=None, rNames=None, scale = None, color='red'):
    if scale is None:
        scale = 1.25*X.max()
    if rNames is not None:
        print('\\begin{tabular}{' + 'c'*(1+X.shape[1]) + '}')
        cmidrule = '\\cmidrule' + '{2-' + str(X.shape[1]+1) + '}'
    else:
        print('\\begin{tabular}{|' + 'c|' * (X.shape[1]) + '}')
        cmidrule = '\\midrule'

    #print(cmidrule)
    if cNames is not None:
        if rNames is not None:
            print('&')
        for k, c in enumerate(cNames):
            if k < X.shape[1]-1:
                print(c, '&')
            else:
                print(c,'\\\\')
        print(cmidrule)

    for r in range(X.shape[0]-1, -1, -1):
        u = ''
        if rNames is not None:
            u += rNames[r] + '&'
        for k in range(X.shape[1]):
            u += '\\cellcolor{' + color + '!' + str(int(100 * X[r, k] / scale)) + '}'
            if k < X.shape[1]-1:
                u += '{0:.3f}'.format(X[r,k]) + '&'
            else:
                u += '{0:.3f}'.format(X[r,k]) + '\\\\'
                print(u)
        #print(cmidrule)

    print('\\end{tabular}')

if __name__ == "__main__":
    rName = ['TP53','MYC','PTEN', 'ZFHX3', 'FGF17']
    dir = '/Users/younes/Dropbox (MechPred)/MechPred/USER/WIKUM_PDF/DivergenceAnalysis5/analysis_153/csv/'
    df = pd.read_csv(dir+'Prostate_primary_gleason_target_probs.txt', index_col=0)
    X = df.to_numpy()
    # X = np.array([[0.071,0.143,0.286],
    #               [0.036,0.094,0.245],
    #               [0.133,0.245,0.347],
    #               [0.056,0.102,0.184],
    #               [0.102,0.139,0.163]])
    makeColorTable(X, rNames=df.index, cNames=df.columns, color='Maroon')