import numpy as np
from covering import quadratic_covering, quadratic_covering_proximal


### This script runs the relaxed quadratic covering method on matrices
### with dominant quadrant (toy example)
### and checks the correctness of the solution

## d: number of variables
## c: size of the optimal subset
d = 200
c = 25

### Matrix coeff interval within optimal subset
h00 = -3
h01 = 3.5

## Matrix coeff interval between optimal subset and the rest
h10 = 3.5
h11 = 4

## Matrix coeff interval outside of the optimal subset
h21 = 4.1
h22 = 4.5

for repeat in range(1000):
    A00 = np.random.uniform(h00, h01, (c,c) )
    a0 = np.random.uniform(h10, h11, (c, 1)) * np.ones((1,d-c))
    a1 = np.random.uniform(h21, h22, 1) * np.ones((d-c,d-c))

    A = np.zeros((d,d))
    A[:c,:c] = A00
    A[:c, c:] = a0
    A[c:, :c] = a0.T
    A[c:, c:] = a1

    x = quadratic_covering_proximal(A, c, weight=10)
    if ((1-x[1][:c]).max() > 1e5) or (1+x[1][:c]).max() > 1e5:
        print(f'Test {repeat+1} found sub optimal solution')
        print(x[1])
    else:
        print(f'Test {repeat+1} Found correct solution')