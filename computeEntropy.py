from inputData import InputData
from getCoverings import getCovering
from pylatex import Document, Section, Command, NoEscape, Subsection, Subsubsection, Tabular, Math, TikZ, Axis

if __name__=="__main__":
    #stypes = ('pathway', 'source', 'pair', 'target')
    secondRun = False
    n_sde = 5000
    n_sim = 20
    repeat = 30
    sl = 30
    maxVar0 = 16
    maxSize = 4
    #entropyFun = distEntropy
    entropyFunName = 'entropyUB'
    entropyFun = partial(entropyUB, maxSize=maxSize)

    bootstrap = False
    #entropyFun = entropyMRF_fit
    stypes = ( 'source', 'target', 'pair','pathway')
    #stypes = ('target', )
    #stypes = ('pair',)
    ttypes = ('Lung', 'Breast',  'Liver','KIRC','Prostate', 'Colon')
    #ttypes = ('Liver','KIRC','KIRP','Prostate', 'Colon')
    ms = dict()
    ms['target'] = 3
    ms['pathway'] = 1
    ms['source'] = 1
    ms['pair'] = 1
    #ttypes = ('Colon',)
    for ttype in ttypes:
        doc = Document()
        inp = InputData(ttype=ttype)
        doc.preamble.append(Command('title', 'Tissue: ' + ttype + '; J={0:d}'.format(ms['source']) +
                                    '(target: {0:d});'.format(ms['target']) + ' Max size={0:d}'.format(maxSize)
                                    ))
        doc.append(NoEscape(r'\maketitle'))
        for stype in stypes:
    #for ttype in ('lung',):
            with doc.create(Section(stype)):
                #for stype in ('source', 'pair', 'target'):
                print('***********************************')
                if stype != 'pathway':
                    J = getCovering(ttype, stype, minSize=ms[stype], result=None)
                    inp.X[stype] = inp.df_data[stype].loc[inp.df_data[stype].index[J], :].to_numpy().T
                    # if stype == 'pair':
                    #     inp.X[stype] = inp.X[stype][2:,:].astype(bool)
                    #     cover = inp.df_data[stype].loc[J, ['source','target']]
                    #     #inp.X[stype] = inp.df_data[stype].loc[inp.df_data[stype].index[J], :].to_numpy().T
                    # else:
                    cover = inp.df_data[stype].index[J]
                print(cover)
                # df_pheno = df_pheno.reindex(df_data.columns)
                print(ttype,stype, inp.X[stype].shape[1], 'variables')
                tr1 = np.triu_indices(inp.X[stype].shape[1], 1)
                doc.append('Tumor type: {0:s}, {1:s} analysis, with {2:d} variables.\n\n'.format(ttype,stype,inp.X[stype].shape[1]))
                row_sl=[]
                with doc.create(Tabular('lllll')) as tab:
                    tab.add_row(('Subtype', 'Value', 'N', 'Entropy', 'CI'))
                    tab.add_hline()
                    if inp.X[stype].shape[0] > 20:
                        RES0 = entropyFun(inp.X[stype], withCI=True)
                        H_ = RES0['entropy']
                        H__ = RES0['ent0']
                        tab.add_row(('All', '', '{0:d}'.format(inp.X[stype].shape[0]), '{0:.2f}'.format(H_),
                                     #'{0:.2f}'.format(H_ + H__ -RES0['mc'].mean()),
                                     #'{0:.2f}'.format(mad(RES0['mc'])),
                                     # '{0:.2f}'.format(H_+ H__ - 2*RES0['mc'].mean() + np.quantile(RES0['mc'], 0.05)),
                                     # '{0:.2f}'.format(H_+H__ -2* RES0['mc'].mean() + np.quantile(RES0['mc'], 0.95))
                                     '[{0:.2f}, {1:.2f}]'.format(H_+ H__ - np.quantile(RES0['mc'], 0.95),
                                                               H_+H__ - np.quantile(RES0['mc'], 0.05))
                                     ))
                        print(inp.X[stype].shape[0], 'subjects; Global Entropy:', H_)
                        if not bootstrap:
                            print('CI:', np.quantile(RES0['mc'], 0.05), np.quantile(RES0['mc'], 0.95))
                            H = RES0['mc']
                            sl_ = inp.X[stype].shape[0]
                            row_sl.append(('All', '', '{0:d}'.format(sl_),
                                           '{0:.2f}'.format(RES0['ent0']),
                                           '{0:.2f}'.format(RES0['mc'].mean()),
                                           '{0:.2f}'.format(np.quantile(RES0['mc'], 0.05)),
                                           '{0:.2f}'.format(np.quantile(RES0['mc'], 0.95))
                                           ))
                        else:
                            H = np.zeros(repeat)
                            for k in range(repeat):
                                I = np.random.randint(0 ,inp.X[stype].shape[0] ,sl)
                                X_ = inp.X[stype][I ,:]
                                RES = entropyFun(X_)
                                H[k] = RES['entropy']
                                print(min(inp.X[stype].shape[0],sl), 'subjects; Global Entropy:', H[k])
                            sl_ = min(inp.X[stype].shape[0],sl)
                            row_sl.append(('All', '', '{0:d}'.format(sl_), '{0:.2f}'.format(H.mean()),
                                           '{0:.2f}'.format(H.min()), '{0:.2f}'.format(H.max())))
                    tab.add_hline()


                    all_rs = []
                    all_ptab = []
                    for st in inp.classes.keys():
                        print('**', st, '**')
                        allH = []
                        pTab = [[st]]
                        for k, cl in enumerate(inp.classes[st].names):
                            X0 =inp.X[stype][inp.classes[st].labels==k,:]
                            if X0.shape[0] > 20:
                                pTab[0].append(cl)
                                pTab.append([cl])
                                RES = entropyFun(X0, withCI=True)
                                H_ = RES['entropy']
                                H__ = RES['ent0']
                                HH = entropyTuple(X0, size=maxSize)
                                print(cl, X0.shape[0], 'subjects; Entropy:', H_, 'tuple', HH)
                                    #print('CI:', RES['CI'])
                                tab.add_row((st, cl, '{0:d}'.format(X0.shape[0]), '{0:.2f}'.format(H_),
                                             #'{0:.2f}'.format(H_ + H__ - RES['mc'].mean()),
                                             #'{0:.2f}'.format(mad(RES['mc'])),
                                             # '{0:.2f}'.format(H_ + H__ - 2*RES['mc'].mean() + np.quantile(RES['mc'], 0.05)),
                                             # '{0:.2f}'.format(H_ + H__ - 2*RES['mc'].mean() + np.quantile(RES['mc'], 0.95))
                                             '[{0:.2f}, {1:.2f}]'.format(H_ + H__ -  np.quantile(RES['mc'], 0.95),
                                                                       H_ + H__ -  np.quantile(RES['mc'], 0.05))
                                             ))
                                if not bootstrap:
                                    print('CI:', np.quantile(RES['mc'], 0.05), np.quantile(RES['mc'], 0.95))
                                    allH.append(RES['mc'])
                                    sl_ = X0.shape[0]
                                    row_sl.append((st, cl, '{0:d}'.format(sl_),
                                                   '{0:.2f}'.format(RES['ent0']),
                                                   '{0:.2f}'.format(RES['mc'].mean()),
                                                   '{0:.2f}'.format(np.quantile(RES['mc'], 0.05)),
                                                   '{0:.2f}'.format(np.quantile(RES['mc'], 0.95))))
                                else:
                                    H = np.zeros(repeat)
                                    for k in range(repeat):
                                        I = np.random.randint(0 ,X0.shape[0] ,sl)
                                        X_ = X0[I ,:]
                                        RES = entropyFun(X_)
                                        H[k] = RES['entropy']
                                        print(cl, min(X0.shape[0],sl), 'subjects; Entropy:', H[k])
                                    sl_ = min(inp.X[stype].shape[0], sl)
                                    allH.append(H)
                                    row_sl.append((st, cl, '{0:d}'.format(sl_), '{0:.2f}'.format(H.mean()),
                                                 '{0:.2f}'.format(H.min()), '{0:.2f}'.format(H.max())))
                        row_sl.append(())
                        tab.add_hline()
                        rs = np.zeros((len(allH), len(allH)))
                        for k1,H1 in enumerate(allH):
                            for k2,H2 in enumerate(allH):
                                a, rs[k1,k2] = ranksums(H1,H2)
                                pTab[k1+1].append('{0:.4f}'.format(rs[k1,k2]))
                        all_rs.append(rs)
                        print(pTab)
                        all_ptab.append(pTab)

                doc.append('\n')
                doc.append('\n')
                if entropyFunName == 'entropyUB':
                    if stype != 'pathway':
                        print(RES0['subsets'])
                        doc.append('\n GROUPS (ENTROPY ESTIMATION)\n\n')
                        for l in RES0['subsets']:
                            prt = ''
                            for j in l:
                                prt += cover[j] + '; '
                            print(l, prt)
                            doc.append(prt + '\n')


                doc.append('\n SIMULATION RESULTS\n\n')
                with doc.create(Tabular('lllllll')) as tab:
                    tab.add_row(('Subtype', 'Value', 'N', 'sample entropy', 'Mean Entropy', '5% Entropy', '95% Entropy'))
                    tab.add_hline()
                    for r in row_sl:
                        if len(r) > 0:
                            tab.add_row(r)
                        else:
                            tab.add_hline()


                for pTab in all_ptab:
                    doc.append('\n')
                    doc.append('\n')

                    with doc.create(Tabular('l'*len(pTab))) as tab:
                        for r in pTab:
                            tab.add_row(r)

        #doc.generate_tex('Reports/'+ entropyFunName+'_core_J1_M' + str(maxSize) + '_'+ttype)
        doc.generate_pdf('Reports/'+ entropyFunName+'_J1_M' + str(maxSize) + '_'+ttype, clean_tex=False)
