import TSP
import diff_exp
import pandas as pd
import numpy as np
from fdr import fdr
from sklearn.decomposition import PCA
import time
from matplotlib import pyplot as plt
import networkx as nx


#data_root_dir = '/Users/younes/OneDrive - Johns Hopkins University'
data_root_dir = '/cis/home/younes/Development/Data'

if __name__=="__main__":
    #analysis = 'enigma_thickness'
    analysis = 'coding'
    Wilcoxon = True
    normalize = True
    symmetric = False
    permutations = 5000
    multiprocess = True
    function = TSP._lessThan
    if analysis in ('coding', 'all', 'SZ'):
        if analysis == 'coding':
            rna_file = data_root_dir + '/Core_C_data_analysis_2022/RNA-Seq/coding_gene_logTPM_adjusted.csv'
            df_rna = pd.read_csv(rna_file, index_col=0, engine='python', delimiter=',')
            df_rna = df_rna.transpose()
            df_rna.rename(lambda s: s.split('_')[0], axis='index', inplace=True)
        elif analysis == 'all':
            rna_file = data_root_dir + '/Core_C_data_analysis_2022/RNA-Seq/gene_logTPM_adjusted.csv'
            df_rna = pd.read_csv(rna_file, index_col=0, engine='python', delimiter=',')
            df_rna = df_rna.transpose()
            df_rna.rename(lambda s: s.split('_')[0], axis='index', inplace=True)
        elif analysis == 'SZ':
            rna_file = data_root_dir + '/Core_C_data_analysis_2022/RNA-Seq/gene_logTPM_adjusted.csv'
            presel_file = data_root_dir + '/Core_C_data_analysis_2022/RNA-Seq/GWAS_SZ_genes.csv'
            df_presel = pd.read_csv(presel_file, index_col=0, engine='python', delimiter=',')
            df_rna = pd.read_csv(rna_file, index_col=0, engine='python', delimiter=',')
            inter = df_rna.index.intersection(df_presel['sym'])
            df_rna = df_rna.loc[inter, :]
            df_rna = df_rna.transpose()
            df_rna.rename(lambda s: s.split('_')[0], axis='index', inplace=True)
            Wilcoxon = False
        else:
            df_rna = None

        group_file = data_root_dir + '/Core_C_data_analysis_2022/RNA-Seq/demo.csv'
        df_group = pd.read_csv(group_file, index_col=0, engine='python', delimiter=',')
        df_group.rename(lambda s: s.split('_')[0], axis='index', inplace=True)
        inter = df_rna.index.intersection(df_group.index)
        df_rna = df_rna.loc[inter, :]
        df_group = df_group.loc[inter, :]
        statusData = df_group['group']
        names = ('Patient', 'Control')  # np.unique(statusData.dropna())
        labels = 100 * np.ones(statusData.shape[0], dtype=int)
        for k, cl in enumerate(names):
            labels[statusData == cl] = k
        # X = np.log(1+df_rna.to_numpy())
        X = df_rna.to_numpy()
        if normalize:
            mX = np.mean(np.fabs(X), axis=0)
            X = X/mX[None, :]

        var_names= df_rna.columns
        npairs = 500
    elif analysis in ('enigma_volume', 'enigma_thickness', 'enigma_surface'):
        if analysis == 'enigma_volume':
            data_file = data_root_dir + '/Core_C_data_analysis_2022/ENIGMA/brain_volume_adjusted.csv'
        elif analysis == 'enigma_surface':
            data_file = data_root_dir + '/Core_C_data_analysis_2022/ENIGMA/surface_area_adjusted.csv'
        elif analysis == 'enigma_thickness':
            data_file = data_root_dir + '/Core_C_data_analysis_2022/ENIGMA/thickness_adjusted.csv'
        else:
            data_file = None

        df_data = pd.read_csv(data_file, index_col=0, engine='python', delimiter=',')
        statusData = df_data['group']
        names = ('Patient', 'Control')  # np.unique(statusData.dropna())
        labels = 100 * np.ones(statusData.shape[0], dtype=int)
        for k, cl in enumerate(names):
            labels[statusData == cl] = k
        df_data = df_data.iloc[:,13:]
        X = df_data.to_numpy()
        if normalize:
            mX = np.mean(np.fabs(X), axis=0)
            X = X / mX[None, :]
        var_names = df_data.columns
        npairs = 200
        Wilcoxon = False
    elif analysis in ('mouse_IOI'):
        rna_file = data_root_dir + '/Core_C_data_analysis_2022/IOI_mouse/gene_FPKM.csv'
        pheno_file = data_root_dir + '/Core_C_data_analysis_2022/IOI_mouse/pheno.csv'
        df_rna = pd.read_csv(rna_file, index_col=0, engine='python', delimiter=',')
        var_names = df_rna.index
        df_rna = df_rna.iloc[:,2:]
        df_rna = df_rna.transpose()
        df_group = pd.read_csv(pheno_file, index_col=0, engine='python', delimiter=',')
        inter = df_rna.index.intersection(df_group.index)
        df_rna = df_rna.loc[inter, :]
        df_group = df_group.loc[inter, :]
        statusData = df_group['group']
        labels = 100 * np.ones(statusData.shape[0], dtype=int)
        names = ('IOI', 'control')  # np.unique(statusData.dropna())
        for k, cl in enumerate(names):
            labels[statusData == cl] = k
        # X = np.log(1+df_rna.to_numpy())
        X = df_rna.to_numpy()
        if normalize:
            mX = np.mean(np.fabs(X), axis=0)
            X = X/mX[None, :]
        npairs = 30
        Wilcoxon = False

    pca = PCA(n_components=1)


    tasks = ['TSP']
    #tasks = ['TSP','diff_exp']
    fd = 0.23
    if 'TSP' in tasks:
        nperm = 500
        pairs, diff, val, fe = TSP.TSP_train(X, labels, npairs=npairs, with_perm=permutations, comparison='absDiff', PLS=False,
                                              Wilcoxon=Wilcoxon, function=function, multiprocess = multiprocess)

        selected = {}
        for k in range(diff.shape[0]):    var_names = df_rna.index
        df_rna = df_rna.iloc[:,2:]
        df_rna = df_rna.transpose()
        df_group = pd.read_csv(pheno_file, index_col=0, engine='python', delimiter=',')
        inter = df_rna.index.intersection(df_group.index)
        df_rna = df_rna.loc[inter, :]
        df_group = df_group.loc[inter, :]
        statusData = df_group['group']
        labels = 100 * np.ones(statusData.shape[0], dtype=int)
        names = ('IOI', 'control')  # np.unique(statusData.dropna())
        for k, cl in enumerate(names):
            labels[statusData == cl] = k
        # X = np.log(1+df_rna.to_numpy())
        X = df_rna.to_numpy()
        if normalize:
            mX = np.mean(np.fabs(X), axis=0)
            X = X/mX[None, :]
        npairs = 30
        Wilcoxon = False

        print(var_names[pairs[0][k]] + '  ' + var_names[pairs[1][k]]
              + f'  {diff[k]:.4f} {val[0][k]:.4f} {val[1][k]:.4f} {fe[k]:.6f} {fe[k] < .05}')
        print('\n')
        G = nx.Graph()
        for k in range(diff.shape[0]):
            if fe[k] < .05:
                #G.add_edge(pairs[0][k], pairs[1][k])
                G.add_edge(var_names[pairs[0][k]], var_names[pairs[1][k]])
                print(var_names[pairs[0][k]] + '  ' + var_names[pairs[1][k]]
                      + f'  {diff[k]:.4f} {val[0][k]:.4f} {val[1][k]:.4f} {fe[k]:.6f}')
                for j in (0,1):
                    if var_names[pairs[j][k]] in selected:
                        selected[var_names[pairs[j][k]]] = selected[var_names[pairs[j][k]]] + 1
                    else:
                        selected[var_names[pairs[j][k]]] = 1
        # for nd in G.nodes.items():
        #     G.nodes[nd[0]]['name'] = var_names[nd[0]]

        print('selected variables')
        s = ''
        s1 = ''
        for n in sorted(selected.keys()):
            #s += f' {n};'
            s += f' {n} ({selected[n]});'
        print(s)

        nx.draw_networkx(G, pos=nx.circular_layout(G, scale=2), font_size=10, node_color=(1,1,1), edge_color=(1,0,0))
        plt.draw()
        plt.show()

    if 'diff_exp' in tasks:
        J, s, p = diff_exp.diff_exp(X, labels, test='t', perm=1000, PLS=False)
        qv = fdr(p)
        for k in range(min(1000, J.size)):
            print(f'{k}: ' + var_names[J[k]]  + f'  {s[k]:.4f} {p[k]:.6f} {qv[k]:.6f}')

