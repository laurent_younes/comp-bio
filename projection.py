import math

import numpy as np
from numba import jit

@jit(nopython=True)
def project_admm(y, r, alpha=0.1, maxiter = 100, tol=1e-8):
    N = y.shape[0]
    ll = np.zeros(N)
    x = np.zeros(N)
    z = np.zeros(N)
    ones = np.ones(N)

    for it in range(maxiter):
        u = (y+z -ll)/(1+alpha)
        x = u - ((u.sum() - r)/N) * ones
        z = np.maximum(0, np.minimum(ll + alpha* x, 1))
        ll += alpha * (x-z)
        if np.fabs(x-z).max() < tol:
            #print(f'stop after {it} iterations')
            break

    return x, z, ll

@jit(nopython=True)
def project(y, r):
    N = y.shape[0]
    if r > N:
        print(f'Projection: r={r} N={N} No solution')
        return
    y2 = np.concatenate((y, y-1), axis=0)
    yy = np.sort(y2)
    S = np.zeros(2*N)
    for k in range(2*N):
        S[k] = np.maximum(0, np.minimum(1, y-yy[k])).sum()
    I = np.nonzero(S >= r)[0]
    if len(I) == 0:
        lbd = yy[0] - (r-S[0])/(N-S[0])
    else:
        k = I[-1]
        lbd = yy[k] + (yy[k+1] - yy[k]) * (r - S[k])/(S[k+1] - S[k])

    res = np.maximum(0, np.minimum(1, y-lbd))
    return res
def project_(y, r):
    N = y.shape[0]
    if r > N:
        print(f'Projection: r={r} N={N} No solution')
        return

    res = np.zeros(N)
    y2 = np.concatenate((y, y-1), axis=0)
    # I = np.argsort(y2)
    # yy = y2[I]
    yy = np.sort(y2)
    k0 = 2*N-1
    c = yy[k0]
    rtry = np.minimum(np.maximum(y - c, 0), 1).sum()
    while rtry < r and k0 >=0:
        rtry_old = rtry
        k0 -= 1
        c = yy[k0]
        rtry = np.minimum(np.maximum(y - c, 0), 1).sum()
    ## rho between yy[k0] and yy[k0+1]
    t = (r - rtry)/(rtry_old - rtry)
    rho = yy[k0] + t*(yy[k0+1]-yy[k0])
    J1 = y > yy[k0+1]+1 - 1e-10
    res[J1] = 1
    res[y < yy[k0] + 1e-10 ] = 0
    J = np.logical_and(y< yy[k0+1] + 1, y > yy[k0])
    #rho2 = y[J].mean() - (r-J1.sum())/J.sum()
    res[J] = y[J] - rho
    #print(res.sum(), res.min(), res.max())
    return res


def project_FW(grad, c):
    n = grad.shape[0]
    if c > n:
        print('No solution')
        return
    x = np.zeros(n)
    J = np.argsort(grad)
    ci = int(c)
    x[J[:ci]] = 1
    if ci < n:
        x[J[ci]] = c-ci
    return x