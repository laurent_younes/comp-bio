import scipy.stats as sps
import numpy as np
from scipy.special import digamma

def entropy(X, with_correction=True):
    val,counts = np.unique(X, return_counts=True, axis=0)
    prob = counts/X.shape[0]
    H__ = - (prob*np.log2(prob)).sum()
    H_ = np.log2(np.e)*(prob *(np.log(X.shape[0]) - digamma(counts) - (1 - 2*(counts%2))/(counts*(counts+1)))).sum()
    #H = - (prob*np.log2(prob)).sum() + (2**X.shape[1] - 1)/(2*X.shape[0])
    if with_correction:
        return H_
    else:
        return H__


for L in (3,4,5,6):
    N = 2**L
    n_ = [20, 30, 40, 50, 75, 100, 150, 200]
    K = 5000
    a = 2*np.ones(N)
    p = sps.dirichlet.rvs(a)
    H0 = -(p * np.log2(p)).sum()
    print(f'True entropy: {H0:.4f}')

    for n in n_:
        H = np.zeros(K)

        for j in range(K):
            x = np.random.choice(N, size=n, p=p[0])
            H[j] = entropy(x, with_correction=True)

        print(f'L = {L}, n={n}, Bias: {H.mean() - H0:.4f}, std: {np.std(H):.4f}')