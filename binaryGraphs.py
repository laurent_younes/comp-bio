import numpy as np
import scipy.stats as stats
from sklearn.linear_model import LogisticRegression
from statsmodels.discrete.discrete_model import Logit

N0 = 10
N1 = 2*N0
N2 = 3*N0
N3 = 4*N0
X = np.zeros((N3, 3))
X[:,0] = 1
X[N0:N1,1] = 1
X[N2:, 1] = 1
X[N1:, 2] = 1
alpha = -0.5
beta0 = 1
beta1 = -0.5
c = 3
Y = np.zeros(N3, dtype=int)
P = np.exp(c*(alpha + beta0*X[:,1] + beta1*X[:,2]))
P = P/(1+P)
U = stats.uniform.rvs(0, 1, size=N3)
Y[:] = U < P

# lr = LogisticRegression()
lr = Logit(Y,X).fit()
print(lr.summary())
#Z = lr.predict(X)

#print((Y!=Z).sum())
