import numpy as np
from scipy.sparse import csr_matrix, csc_matrix
import gurobipy as grb
import cvxpy as cp
from projection import project, project_FW
from entropy import cond_entropy, entropy as compute_entropy
from sklearn.feature_selection import mutual_info_classif

import os
os.environ['SPYCIAL_CACHE'] = "0"


# from spycial import digamma

## Computes optimal coverings
## Z: Nxd binary matrix where N is the number of observations and d the number of variables. Alternatively, each
## column of Z determines a subset of the rows (and a covering is a subset of the columns).
##   If minSize is scalar:
##       Finds a minimal subset of variables such that 100(1-alpha)% of observation have at least
##       minSize variable in that subset equal to 1.
##   If minSize is array-like and provides a series of L increasing integers
##       Finds a L minimal nested subsets of variables such that for each level l
##       100(1-alpha[l])% of observation have at least
##       minSize[l] variable in that subset equal to 1.
## weights: specifies an optional weight vector of variables (default: all variables have the same weight).
##          If weights is specified as 'prob', variables with high probability of occurrence are preferred.
## Additional parameters control the call to Gurobi (refer to Gurobi documentation for details).

def covering_greedy(Z, minSize=1, weights = 1., alpha = 0.05):
    # N: number of samples; d: number of sets
    N = Z.shape[0]
    d = Z.shape[1]

    if type(weights) == str and weights=='prob':
        w = 1 - 0.01 * np.mean(Z, axis=0)
    elif np.isscalar(weights):
        w = weights * np.ones(d)
    else:
        w = weights

    x = np.zeros(d, dtype=int)
    t = minSize * np.ones(N, dtype=int)
    P = Z.sum(axis=0)

    while (t>0).mean() > alpha:
        k = np.argmax(P/weights)
        Pk = np.nonzero(Z[:,k]>0)[0]
        tm = t[Pk].min()
        x[k] += tm
        t[Pk] -= tm
        t0 = np.nonzero(t[Pk] == 0)[0]
        for j0 in t0:
            i = Pk[j0]
            Z[i, :] = 0
        P = Z.sum(axis=0)

    return x




def covering(Z, minSize=1, alpha=0.05, weights = 1., greedy = False, return_gurobi=False, output=None, callBack = None,
             poolSolutions=None, poolSearchMode=None, poolGap = None, timeLimit=None, restart=None):

    if greedy:
        return covering_greedy(Z, minSize=minSize, weights=weights, alpha=alpha)

    if restart is not None:
        cov = restart
        if output is not None:
            cov.Params.OutputFlag = output
        if poolSolutions is not None:
            cov.Params.PoolSolutions = poolSolutions
        if poolSearchMode is not None:
            cov.Params.PoolSearchMode = poolSearchMode
        if poolGap is not None:
            cov.Params.PoolGap = poolGap
        if timeLimit is not None:
            cov.Params.TimeLimit = timeLimit
        if callBack is None:
            cov.optimize()
        else:
            cov.optimize(callBack)
        return cov

    if np.isscalar(minSize):
        minSize = [minSize]
    if np.isscalar(alpha):
        alpha = [alpha]*len(minSize)
    N = Z.shape[0]
    d = Z.shape[1]
    if type(weights) == str and weights=='prob':
        w = 1 - 0.01 * np.mean(Z, axis=0)
    elif np.isscalar(weights):
        w = weights * np.ones(d)
    else:
        w = weights
    cov = grb.Model()
    if output is not None:
        cov.Params.OutputFlag=output
    if poolSolutions is not None:
        cov.Params.PoolSolutions = poolSolutions
    if poolSearchMode is not None:
        cov.Params.PoolSearchMode = poolSearchMode
    if poolGap is not None:
        cov.Params.PoolGap = poolGap
    if timeLimit is not None:
        cov.Params.TimeLimit = timeLimit

    nlevels = len(minSize)
    x = []
    y = []
    for l in range(nlevels):
        x.append(cov.addMVar(d, vtype=grb.GRB.BINARY))
    for l in range(nlevels):
        y.append(cov.addMVar(N, vtype=grb.GRB.BINARY))

    # x = cov.addVars(d*nlevels, vtype=grb.GRB.BINARY)
    # y = cov.addVars(N*nlevels, vtype=grb.GRB.BINARY)
    for l in range(nlevels):
        #expr = grb.LinExpr([1]*N, y.values()[l*N:(l+1)*N])
        #expr = grb.LinExpr()
        #u = np.ones(N)
        expr = y[l].sum()
        # for k in range(N):
        #      expr += y[k+l*N]
        #cov.addMConstr(u, y[l], 'Coverage_'+str(l))
        cov.addConstr(expr >= N*(1-alpha[l]), 'Coverage_'+str(l))
        # cov.addLConstr(expr, sense=grb.GRB.GREATER_EQUAL, rhs=N*(1-alpha[l]), name='Coverage_'+str(l))

    for l in range(nlevels):
        expr = Z @ x[l] - minSize[l]*y[l]
        #Zl = np.concatenate((Z, -minSize[l]*np.eye(N)), axis=1)
        #cov.addMConstr(Zl, x[l].tolist() + y[l].tolist(), '>', np.zeros(N), name= 'covered_' + str(l))
        cov.addConstr(expr >= 0, 'covered_' + str(l))
        # for k in range(N):
        #     #expr = grb.LinExpr(list(Z[k,:]), x.values()[l*d:(l+1)*d])
        #     # c = {j: Z[k,j] for j in range(d)}
        #     # expr = x[l].prod(c)
        #     # expr = grb.LinExpr()
        #     # for j in range(d):
        #     #     expr += Z[k,j] * x[j+l*d]
        #     cov.addLConstr(expr, sense=grb.GRB.GREATER_EQUAL, rhs=0, name= 'Covered'+str(k)+'_'+str(l))

    for l in range(nlevels-1):
        # print(f'l={l}')
        for j in range(d):
            #expr = grb.LinExpr(x[l+1][j] - x[l][j])
            cov.addConstr(x[l+1].tolist()[j] - x[l].tolist()[j] >= 0, name= 'Nesting'+str(j)+'_'+str(l))

    #expr = grb.LinExpr(list(w), x.values[(nlevels-1)*d:])
    expr = grb.LinExpr()
    # c = {j: w[j] for j in range(d)}
    for l in range(nlevels):
        expr += (w * x[l]).sum()
    # for j in range(d):
    #     expr += w[j]*x[j + (nlevels-1)*d]
    cov.setObjective(expr, grb.GRB.MINIMIZE)

    if callBack is None:
        cov.optimize()
    else:
        cov.optimize(callBack)

    if return_gurobi:
        return cov
    else:
        return np.array(cov.x[:N]) > 0.5

##Extracting covering gene names from Gurobi result
def getCoveringVariables(cov, ngenes, geneNames = None, nlevels = 1):
    covx = np.array(cov.x)
    genes = []
    if geneNames is None:
        geneNames = [str(x) for x in range(ngenes)]
    for l in range(nlevels):
        I = np.nonzero(covx[ngenes*l:ngenes*(l+1)] > 0.5)[0]
        genes.append([geneNames[k] for k in I])
    print(genes)
    return genes


## Enables iterated cals to the covering function: if cov is None, the problem is initialized, otherwise
## Gurobi is restarted with the problem and current solution stored in cov.
def covering_run(Z0, w, timeLimit=1000, cov = None, minSize=10, alphaMax = 0.95):
    if cov is None:
        alpha = (Z0.sum(axis=1) >= minSize).mean()
        print(f'alpha = {alpha:.2f}')
        cov = covering(Z0, weights=w, minSize=minSize, callBack=None, timeLimit=timeLimit,
                       alpha=max(1 - alpha, 1-alphaMax))
    else:
        cov.optimize()
    return cov



## Computes an optimal panel of size r based on the matrix A minimizing the pairwise sum of
## weights between these elements provided by the matrix A using an SDP relaxation.
## If MI=True, maximizes the sum of weights instead.
def quadratic_covering(A, r, solver='SCS', type_result='H', verbose = False):
    N = A.shape[0]
    AA = np.zeros((N+1, N+1))
    AA[0, 1:] = A.sum(axis=1)
    AA[1:, 0] = A.sum(axis=0)
    AA[1:, 1:] = A
    X = cp.Variable((N+1, N+1), symmetric=True)
    constraints = [X>>0]
    b0 = np.zeros(N+1)
    b1 = np.ones(N+1)
    b1[0] = N - 2*r
    constraints += [X @ b1 == b0]

    # b0 = np.zeros((N+1, N+1))
    # b0[1:, 0] = 0.5
    # b0[0, 1:] = 0.5
    # constraints += [cp.trace(b0 @ X) == 2*r - N]
    for k in range(N+1):
        bb = np.zeros((N+1, N+1))
        bb[k, k] = 1
        constraints += [cp.trace(bb @ X) == 1]

    print('using ' + type_result)
    if type_result == 'MI':
        prob = cp.Problem(cp.Maximize(cp.trace(AA @ X)), constraints)
    else:
        prob = cp.Problem(cp.Minimize(cp.trace(AA @ X)), constraints)
    #print('Running SDP')
    prob.solve(solver=solver, verbose = verbose)
    x = X.value[0, 1:]
    print(f' Quadratic covering: Rank={np.linalg.matrix_rank(X.value, tol=np.fabs(2*r - N)*1e-3, hermitian=True)}')
    #print(x.sum()-2*r + N)
    #print(x)
    return np.nonzero(x>0.)[0], x


def quadratic_covering_proximal(A, r, type_result = 'H', weight = 0., init = None, tol = 1e-3, verbose = False):
    N = A.shape[0]
    if init is None:
        x = np.zeros(N)
    else:
        x = init.astype(float)

    x = project(x,r)
    obj = (x.T @ (A @ x) + 0.5 * weight * (x*(1-x))).sum()
    c1 = 0.1
    alpha0 = 0.01
    if type_result in ('MI', 'HResidual'):
        weight = - weight
        alpha0 = -alpha0
    for k in range(1000):
        alpha = alpha0
        xold = x
        Ax = A@x + weight * (0.5 - x)
        x2 = project(x - alpha*Ax, r)
        obj2 = (x2.T @ (A @ x2) + 0.5 * weight * (x2 * (1 - x2))).sum()
        if type_result in ('MI', 'HResidual'):
            tr = 0
            while tr < 25 and obj2 < obj - c1*((x2 - x) * Ax).sum():
                alpha /= 2
                x2 = project(x - alpha * Ax, r)
                obj2 = (x2.T @ (A @ x2) + 0.5 * weight * (x2 * (1 - x2))).sum()
                tr += 1
        else:
            tr = 0
            while tr < 25 and obj2 > obj + c1*((x2-x)*Ax).sum():
                alpha /= 2
                x2 = project(x - alpha*Ax, r)
                obj2 = (x2.T @ (A @ x2) + 0.5 * weight * (x2 * (1 - x2))).sum()
                tr += 1

        if tr == 25:
            print('reached maximum backtracking iterations')
            break
        else:
            obj = obj2
            x = x2
            if verbose:
                print(f'Objective after {k} iterations: {obj:.4f} {np.fabs(x-xold).max():.6f}')
        if (np.fabs(x-xold).max() < tol):
            break

    #print(x)
    print(f'Objective after {k} iterations: {(x.T@(A@x)).sum():.4f}')
    J = np.argpartition(x, x.shape[0] - r)[-r:]
    return J, x


def quadratic_covering_FW(A, r, type_result = 'H', weight = 0., init = None, tol = 1e-3,
                          iterMax = 10000, verbose = False):
    N = A.shape[0]
    if init is None:
        x = np.zeros(N)
    else:
        x = init.astype(float)

    x = project(x,r)
    obj = (x.T @ (A @ x) + 0.5 * weight * (x*(1-x))).sum()
    c1 = 0.1
    alpha0 = 1
    if type_result == 'MI':
        weight = - weight
        alpha0 = -alpha0
    for k in range(iterMax):
        alpha = 2/(2+k)
        xold = x
        Ax = A@x + weight * (0.5 - x)
        x2 = project_FW(Ax, r)
        x += alpha * (x2 - x)
    # if verbose:
        print(f'Objective after {k} iterations: {obj:.4f} {np.fabs(x-xold).max():.6f}')
        if (np.fabs(x-xold).max() < tol):
            break

    #print(x)
    print(f'Objective after {k} iterations: {(x.T@(A@x)).sum():.4f}')
    J = np.argpartition(x, x.shape[0] - r)[-r:]
    return J, x


def quadratic_covering_greedy(A0, r, type_result = 'H', CMIM = False, weight = 0, verbose = False):
    N = A0.shape[0]
    x = np.zeros(N, dtype=bool)
    if type_result == 'MI':
        A = A0
    else:
        A = A0.max() - A0

    I = np.unravel_index(np.argmax(A), A.shape)
    x[I[0]] = True
    x[I[1]] = True
    for k in range(2,r):
        if CMIM:
            U = A[:, x].max(axis=1)
        else:
            U = A[:, x].sum(axis=1)
        U[x] = 0
        i = np.argmax(U)
        x[i] = True

    return np.nonzero(x>0.)[0], x

def cmim_old(Z, r, output = None, callBack = None,poolSolutions=None, poolSearchMode=None, poolGap = None,
                timeLimit=None):

    N = Z.shape[0]-1
    M = Z.max()
    path = grb.Model()
    I1 = np.argsort(Z[:N, -1])
    x0 = np.zeros(N)
    x0[I1[-1]] = 1
    X0 = path.addVars(N, vtype=grb.GRB.BINARY, name="X0")
    X0.Start = x0
    x = np.zeros(N)
    X = path.addVars(N, vtype=grb.GRB.BINARY, name="X")
    x[I1[-r:-1]] = 1
    X.Start = x
    P = path.addVars(N, vtype=grb.GRB.CONTINUOUS, lb=0.0, name="P")
    expr = grb.LinExpr()
    for i in range(N):
        expr += P[i] + X0[i] * Z[i,N]

    path.setObjective(expr, grb.GRB.MAXIMIZE)

    for i in range(N):
        path.addConstr(X.sum() == r-1, name = f"in {i}")
        path.addConstr(X0.sum() == 1, name=f"first {i}")
        path.addConstr(X[i] + X0[i] <= 1, name=f"in or first {i}")
        for j in range(N):
            if j==i:
                path.addConstr(P[i] - (X[j]+X0[j]) * M <= 0, name=f"min {i} {j}")
            else:
                path.addConstr(P[i] - (X[j]+X0[j]) * Z[i,j]  - (1-X[j] - X0[j])*M <= 0, name = f"min {i} {j}")


    if output is not None:
        path.Params.OutputFlag=output
    if poolSolutions is not None:
        path.Params.PoolSolutions = poolSolutions
    if poolSearchMode is not None:
        path.Params.PoolSearchMode = poolSearchMode
    if poolGap is not None:
        path.Params.PoolGap = poolGap
    if timeLimit is not None:
        path.Params.TimeLimit = timeLimit

    # path.Params.MIPFocus = 3

    if callBack is None:
        path.optimize()
    else:
        path.optimize(callBack)

    res = np.zeros(r, dtype=int)
    res[0] = np.nonzero(np.array(path.X[:N]) > 0.5)[0][0]
    res[1:] = np.nonzero(np.array(path.X[N:2*N]) > 0.5)[0]

    return res

def cmim2(Z, Z0, r, nseeds = 1, relax=False, output = None, callBack = None,poolSolutions=None, poolSearchMode=None, poolGap = None,
                timeLimit=None, nodeLimit=None, start=None):
    return cmim(Z, Z0, r, nseeds, relax, output, callBack, poolSolutions, poolSearchMode,
             poolGap, timeLimit, nodeLimit, start)


def cmim(Z, Z0, r, nseeds = 1, relax=False, output = None, callBack = None,poolSolutions=None, poolSearchMode=None, poolGap = None,
                timeLimit=None, nodeLimit=None, start=None):

    ## Z[i,j] contains H(Y|X(j)) - H(Y|X(i),X(j))
    ## Z0[i] contains I(Y| X[i])
    N = Z.shape[0]
    H = Z0/nseeds
    print(H.min(), H.max())
    M = Z.max()
    smallZ = (Z<1e-3).sum()/Z.size
    q = np.quantile(np.ravel(Z), 0.5)
    print(f'CMIM: small CMI: {100 * smallZ:.4f}%, 50% quantile: {q:0.4f}')
    if smallZ > 0.5:
        Zs = Z * (Z > 1e-3)
        Z = csr_matrix(Zs)
    path = grb.Model()

    if relax:
        X = path.addVars(N, vtype=grb.GRB.CONTINUOUS, lb=0.0, ub=1.0,  name="X")
    else:
        X = path.addVars(N, vtype=grb.GRB.BINARY, name="X")

    if start is not None:
        for i in X.keys():
            X[i].setAttr('Start', start[i])
    U = path.addVars(N, vtype=grb.GRB.BINARY, name='U')
    P = path.addVars(N, vtype=grb.GRB.CONTINUOUS, lb=0.0, name="P")
    expr = grb.LinExpr()
    for i in range(N):
        expr += P[i] + U[i] * H[i]

    path.setObjective(expr, grb.GRB.MAXIMIZE)

    path.addConstr(X.sum() == r, name='sum')
    path.addConstr(U.sum() == nseeds, name='seed_sum')
    path.addConstrs((U[i] - X[i] <= 0
                     for i in range(N)))
    path.addConstrs((P[i] - X[j] * Z[i, j] - (1 - X[j]) * M <= 0
                     for i in range(N)
                     for j in range(N)
                     if i != j),
                    name=f"min")
    path.addConstrs((P[i] - X[i] * M <= 0
                     for i in range(N)),
                    name=f"if selected")
    # Uncomment to sum only over non-seed variables
    # path.addConstrs((P[i] - (1-U[i]) * M <= 0
    #                  for i in range(N)),
    #                 name=f"if not seed")

    if output is not None:
        path.Params.OutputFlag=output
    if poolSolutions is not None:
        path.Params.PoolSolutions = poolSolutions
    if poolSearchMode is not None:
        path.Params.PoolSearchMode = poolSearchMode
    if poolGap is not None:
        path.Params.PoolGap = poolGap
    if timeLimit is not None:
        path.Params.TimeLimit = timeLimit
    if nodeLimit is not None:
        path.Params.NodeLimit = nodeLimit

    # path.Params.MIPFocus = 3

    if callBack is None:
        path.optimize()
    else:
        path.optimize(callBack)

    res = dict()
    res['seeds'] = np.nonzero(np.array(path.X[N:2*N]) > 0.5)
    if relax:
        res['panel'] = np.array(path.X[:N])
    else:
        res['panel'] = np.nonzero(np.array(path.X[:N]) > 0.5)[0]
    # x = np.array(path.X[N:])

    return res
def learn_covering(Y, X, r, nseeds = 1, type_result='HResidual', minCount=1, selectThreshold=0.99, method='cmim',
                   maxselect=1000, relax = False, balanced = False, timeLimit = 1000):
    d = X.shape[1]
    cls, YY = np.unique(Y, return_inverse=True)
    Z = X >= minCount
    entropy0, entropy, selection = cond_entropy(YY, Z, type_result=type_result, selectThreshold=selectThreshold,
                                                    balanced=balanced, maxSelect=maxselect)
    if r > len(selection):
        r_ = len(selection)
    else:
        r_ = r

    if nseeds > r_:
        nseeds = r

    cov ={}
    if method == 'quadratic':
        J, x_ = quadratic_covering_proximal(entropy, r_, type_result = type_result, weight=0.)
        J, x_ = quadratic_covering_proximal(entropy, r_, type_result = type_result, weight=10., init=x_)
        x = np.zeros(Z.shape[1])
        J = selection[J]
        x[selection] = x_
        cov['panel'] = J.astype(int)
        cov['x'] = np.zeros(d)
        cov['x'] = x
    elif method=='cmim':
        res = cmim2(entropy, entropy0, r_, nseeds=nseeds, timeLimit=timeLimit, relax=relax)
        if relax:
            cov['panel'] = selection.astype(int)
            cov['x'] = res['panel']
            cov['seeds'] = res['seeds']
        else:
            cov['panel'] = selection[res['panel']].astype(int)
            cov['seeds'] = selection[res['seeds']].astype(int)
            cov['x'] = None
    elif method=='HCovering':
        H = compute_entropy(YY, with_correction=False)
        epsilon = np.quantile(entropy.ravel(), 0.05 + 1/entropy.shape[0])
        G = np.zeros(entropy.shape)
        for i in range(G.shape[0]):
            G[i, :] = entropy[i, :] < epsilon
        res = covering(G, minSize=1, alpha=0.05, weights=H-entropy0, timeLimit=1000)
        cov['panel'] = selection[res.X[:entropy.shape[0]] > 0.5]

    else:
        print('Unknown Method ' + method)
        return

    print(f"Selected {cov['panel'].shape[0]} variables")
    return cov


