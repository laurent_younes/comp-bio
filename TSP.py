import numpy as np
from heapq import nlargest
from numba import jit, prange, int64
from scipy.stats import fisher_exact, mannwhitneyu
from sklearn.cross_decomposition import PLSCanonical
import multiprocessing as mp
from dask.distributed import LocalCluster

@jit(nopython=True)
def topn(n, X):
    return nlargest(n, X)


@jit(nopython=True)
def _lessThan(x,y):
    return x<y

@jit(nopython=True)
def _lessThanOrEqual(x,y):
    return x<=y

@jit(nopython=True)
def _posPart(x,y):
    return max(y-x,0)

@jit(nopython=True)
def _sigmoid(x,y):
    u = np.exp(min(max(10000*(y-x), -100), 100))
    return u/(1+u)


@jit(nopython=True, parallel=False)
def order_stats(X, f = _lessThan):
    N = X.shape[0]
    d = X.shape[1]
    res = np.zeros((d,d))
    for i in prange(d):
        for j in range(d):
            for k in range(N):
                res[i, j] += f(X[k, i], X[k, j]) #(X[k, i] < X[k, j])
            res[i,j] /= N
    return res

# @jit(nopython=True, parallel=False)
# def order_stats_seq(X):
#     N = X.shape[0]
#     d = X.shape[1]
#     res = np.zeros((d,d))
#     for i in range(d):
#         for j in range(d):
#             for k in range(N):
#                 res[i, j] += (X[k, i] < X[k, j])
#             res[i,j] /= N
#     return res

#@jit(nopython=True)
def permute_pairs(X0_, X1_, nperm = 100, PLS=True, npairs = 10, Wilcoxon = False, comparison = 'absDiff',
                  function=_lessThan, presel=0.05, maxvar=5000):
    """
    :param X0_: (N,d) array first population
    :param X1_: (N,d) array second population
    :param nperm: number of random permutations
    :param npairs: integer: maximum number of pairs to return
    :param PLS: boolean: if True, common trend in each population is removed using partial least squares
    :param comparison: in ('absDiff', 'absDiffSym', 'diffAbs'): comparison statistics
            'absDiff': |E(function | 1) - E(function|0)|
            'absDiffSym': |E(function | 1) - E(function|0)| - |E(function.T | 1) - E(function|0)|
            'diffAbs': |E(function | 1)| - |E(function|0)|
    :param Wilcoxon: Performs a pre-selection of pairs based on univariate differential expression
    :param function: function of two variables of which the differential expression is computed
                    default function(x,y) = (x<y)

    :return
    maxDiff: (nperm) array with maximum statistic for each permutation
    """
    N0 = X0_.shape[0]
    N1 = X1_.shape[0]
    N = N0 + N1
    X_ = np.concatenate((X0_, X1_), axis = 0)
    maxdiff = np.zeros(nperm)
    for n in range(nperm):
        #print('Permutation ' + str(n+1))
        J = np.random.permutation(N)
        Y00_ = X_[J[:N0], :]
        Y01_ = X_[J[N0:], :]
        if Wilcoxon:
            pv = mannwhitneyu(Y00_, Y01_)
            p = max(presel, np.quantile(pv.pvalue, min(1., maxvar / pv.pvalue.size)))
            varSel = np.nonzero(pv.pvalue < p)[0]
            Y00 = Y00_[:, varSel]
            Y01 = Y01_[:, varSel]
        else:
            Y00 = Y00_
            Y01 = Y01_
        if PLS:
            N0 = Y00.shape[0]
            R0 = Y00 - Y00.mean(axis=0)[None, :]
            R1 = Y01 - Y01.mean(axis=0)[None, :]
            R = np.concatenate((R0, R1), axis=0)
            Y = np.concatenate((Y00, Y01), axis=0)
            pls = PLSCanonical(n_components=1)
            pls.fit(Y, R)
            Y -= np.dot(pls.transform(Y), pls.x_loadings_.T)
            Y0 = Y[:N0, :]
            Y1 = Y[N0:, :]
        else:
            Y0 = Y00
            Y1 = Y01

        res = get_pairs(Y0, Y1, npairs=npairs, comparison=comparison, function=function)
        maxdiff[n] = res[3]
        c = min(1.0, 0.95*(n+2)/(n+1))
        c1 = np.quantile(maxdiff[:n+1], c)
        print(f'Multiple tests {n+1}: {c1:.4f}')

    return maxdiff

def permute_pairs_mp(X0_, X1_, nperm = 100, PLS=True, npairs = 10, Wilcoxon = False, comparison = 'absDiff',
                  function=_lessThan):
    maxdiff = np.zeros(nperm)
    tasks = []
    for n in range(nperm):
        tasks.append((n, X0_, X1_, PLS, npairs, Wilcoxon, comparison, function))
    with mp.Pool(4) as pr:
        im = pr.imap(_permute_pairs, tasks)
        for k,i in enumerate(im):
            maxdiff[k] = i.get()
    return maxdiff

    # q = mp.Queue()
    # # def foo(q):
    # #     c = _permute_pairs(X0_, X1_, PLS, npairs, Wilcoxon, comparison, function)
    # #     q.put(c)
    #
    # procGrd = []
    # for kk in range(nperm):
    #     procGrd.append(mp.Process(target=_permute_pairs, args=(q,X0_, X1_, PLS, npairs, Wilcoxon, comparison, function)))
    # for kk in range(nperm):
    #     print(f'{kk}')
    #     procGrd[kk].start()
    # print("end start")
    # for kk in range(nperm):
    #     print(f"join {kk}")
    #     procGrd[kk].join()


def _permute_pairs(n, X0_, X1_, PLS=True, npairs = 10, Wilcoxon = False, comparison = 'absDiff',
                  function=_lessThan, presel=0.05, maxvar=5000):
    """
    :param X0_: (N,d) array first population
    :param X1_: (N,d) array second population
    :param nperm: number of random permutations
    :param npairs: integer: maximum number of pairs to return
    :param PLS: boolean: if True, common trend in each population is removed using partial least squares
    :param comparison: in ('absDiff', 'absDiffSym', 'diffAbs'): comparison statistics
            'absDiff': |E(function | 1) - E(function|0)|
            'absDiffSym': |E(function | 1) - E(function|0)| - |E(function.T | 1) - E(function|0)|
            'diffAbs': |E(function | 1)| - |E(function|0)|
    :param Wilcoxon: Performs a pre-selection of pairs based on univariate differential expression
    :param function: function of two variables of which the differential expression is computed
                    default function(x,y) = (x<y)

    :return
    maxDiff: (nperm) array with maximum statistic for each permutation
    """

    # print(n)
    N0 = X0_.shape[0]
    N1 = X1_.shape[0]
    N = N0 + N1
    X_ = np.concatenate((X0_, X1_), axis = 0)
    #print('Permutation ' + str(n+1))
    J = np.random.permutation(N)
    Y00_ = X_[J[:N0], :]
    Y01_ = X_[J[N0:], :]
    if Wilcoxon:
        pv = mannwhitneyu(Y00_, Y01_)
        p = max(presel, np.quantile(pv.pvalue, min(1., maxvar/pv.pvalue.size)))
        varSel = np.nonzero(pv.pvalue < p)[0]
        Y00 = Y00_[:, varSel]
        Y01 = Y01_[:, varSel]
    else:
        Y00 = Y00_
        Y01 = Y01_
    if PLS:
        N0 = Y00.shape[0]
        R0 = Y00 - Y00.mean(axis=0)[None, :]
        R1 = Y01 - Y01.mean(axis=0)[None, :]
        R = np.concatenate((R0, R1), axis=0)
        Y = np.concatenate((Y00, Y01), axis=0)
        pls = PLSCanonical(n_components=1)
        pls.fit(Y, R)
        Y -= np.dot(pls.transform(Y), pls.x_loadings_.T)
        Y0 = Y[:N0, :]
        Y1 = Y[N0:, :]
    else:
        Y0 = Y00
        Y1 = Y01

    res = get_pairs(Y0, Y1, npairs=npairs, comparison=comparison, function=function)

    # q.put(res[3])
    return res[3]


@jit(nopython=True, parallel=False)
def get_pairs(X0, X1, npairs,  function = _lessThan, comparison = 'absDiff'):
    """
    Computes differentially expressed pairs based on training data X0 and X1
    Input
        X0: (N0,d) array. Each row is a d-dimensional sample in the first sub-population
        X1: (N1,d) array. Each row is a d-dimensional sample in the second sub-population
        function: function of two variables of which the differential expression is computed
                    default function(x,y) = (x<y)
        npairs: (integer) maximum number of pairs computed
        comparison in ('absDiff', 'absDiffSym', 'diffAbs'): comparison statistics
            'absDiff': |E(function | 1) - E(function|0)|
            'absDiffSym': |E(function | 1) - E(function|0)| - |E(function.T | 1) - E(function|0)|
            'diffAbs': |E(function | 1)| - |E(function|0)|

    return:
        pairs: top-scoring pairs (tuple of index arrays)
        diff: comparison statistic for each pair
        values: expected value of function evaluated at each pair (tuple of float array)
        maxDiff: maximum difference
    """
    d = X0.shape[1]
    order0 = order_stats(X0, f=function)
    order1 = order_stats(X1, f=function)

    if comparison == 'absDiff':
        diff = np.fabs(order0 - order1)
    elif comparison == 'diffAbs':
        diff = np.fabs(order0) - np.fabs(order1)
    elif comparison == 'absDiffSym':
        diff = np.fabs(order0 - order1) - np.fabs(order0.T - order1)
    else:
        print('invalid comparison statistic')
        return

    # computes maximum deviation
    maxDiff = diff.max()

    # computes highest ranked pairs
    order0 = np.ravel(order0)
    order1 = np.ravel(order1)
    # s_ = np.arange(d*d, dtype=int64)
    s_ = np.arange(d*d).astype(int64)
    p0 = s_ // d
    p1 = s_ - p0*d
    I0 = np.nonzero(p0>p1)[0]

    order0 = order0[I0]
    order1 = order1[I0]
    diff_r = np.ravel(diff)[I0]
    z = topn(npairs, diff_r)
    pairs_r = np.nonzero(diff_r > z[-1] - 1e-8)[0]
    J0 = np.argsort(diff_r[pairs_r])
    pairs_r = pairs_r[J0]
    values0 = order0[pairs_r]
    values1 = order1[pairs_r]
    diff = diff_r[pairs_r]
    pairs_r = I0[pairs_r]
    pairs0 = pairs_r // d
    pairs1 = pairs_r - pairs0*d
    pairs = [pairs0, pairs1]
    values = [values0, values1]

    return pairs, diff, values, maxDiff


def TSP_train(X_, Y0, npairs = 10, with_perm = None, PLS=False, comparison = 'absDiff', Wilcoxon=False, function = _lessThan,
              presel = 0.05, maxvar = 5000, multiprocess = False):
    """
    Computes top-scoring pairs and p-values
    :param X_: (N,d) array
    :param Y0: (N) array (class variable)
    :param npairs: integer: maximum number of pairs to return
    :param with_perm: number of permutations in permutation test (if None: permutation testing is not performed and
                      a Fisher exact test is used for each pair -- conservative)
    :param PLS: boolean: if True, common trend in each population is removed using partial least squares
    :param comparison: in ('absDiff', 'absDiffSym', 'diffAbs'): comparison statistics
            'absDiff': |E(function | 1) - E(function|0)|
            'absDiffSym': |E(function | 1) - E(function|0)| - |E(function.T | 1) - E(function|0)|
            'diffAbs': |E(function | 1)| - |E(function|0)|
    :param Wilcoxon: Performs a pre-selection of pairs based on univariate differential expression
    :param function: function of two variables of which the differential expression is computed
                    default function(x,y) = (x<y)
    :return:
        pairs: top-scoring pairs (tuple of index arrays)
        diff: comparison statistic for each pair
        pVal: test p-value
    """
    Z, indices = np.unique(Y0, return_inverse=True)
    if Z.size != 2:
        print('TSP: more than two classes provided. Only using first two')
    J0 = indices==0
    J1 = indices==1
    X0_ = X_[J0, :]
    X1_ = X_[J1, :]

    if Wilcoxon:
        pv = mannwhitneyu(X0_, X1_)
        p = max(presel, np.quantile(pv.pvalue, min(1., maxvar/pv.pvalue.size)))
        varSel = np.nonzero(pv.pvalue < p)[0]
        X00 = X0_[:, varSel]
        X01 = X1_[:, varSel]
        X = X_[:, varSel]
    else:
        X00 = X0_
        X01 = X1_
        X = X_
        varSel = None

    d = X.shape[1]
    if PLS:
        N0 = X00.shape[0]
        R0 = X00 - X00.mean(axis=0)[None, :]
        R1 = X01 - X01.mean(axis=0)[None, :]
        R = np.concatenate((R0, R1), axis=0)
        pls = PLSCanonical(n_components=1)
        pls.fit(X, R)
        XX = X- np.dot(pls.transform(X), pls.x_loadings_.T)
        X0 = XX[:N0,:]
        X1 = XX[N0:, :]
    else:
        X0 = X00
        X1 = X01

    pairs, diff0, values, maxDiff =  get_pairs(X0, X1, npairs, comparison=comparison, function=function)
    print(f'Original Data: {maxDiff:.4f}')
    if with_perm is not None:
        print('Starting permutations')
        if multiprocess:
            client = LocalCluster().get_client()
            diff = np.zeros(with_perm)
            tasks = []
            chunks = 50
            loops = with_perm // chunks
            for kk in range(loops):
                L = [[]]*with_perm
                for k in range(chunks):
                    L[k] = client.submit(_permute_pairs, k, X0_, X1_, PLS, npairs, Wilcoxon, comparison, function, presel, maxvar)
                for k in range(chunks):
                    kkl = kk*chunks + k 
                    if  kkl < with_perm:
                        diff[kkl] = L[k].result()
                    print(f'{kkl}: {diff[kkl]:.5f}')

        else:
            diff = permute_pairs(X0_, X1_, nperm=with_perm, PLS=PLS, comparison=comparison, npairs=npairs,
                                    Wilcoxon=Wilcoxon, function=function, presel=presel, maxvar=maxvar)
    else:
        diff = None

    for i in (0,1):
        pairs[i] = np.flip(pairs[i], axis=0)
        values[i]= np.flip(values[i], axis=0)
    diff0 = np.flip(diff0, axis=0)
    npairs_ = diff0.shape[0]
    pval = np.zeros(npairs_)
    pval2 = np.zeros(d)
    N0 = X0.shape[0]
    N1 = X1.shape[0]

    if with_perm is not None:
        for i in range(npairs_):
            df = diff0[i]
            pval[i] = (1 + np.sum(diff>df))/(1+diff.shape[0])
    else:
        for i in range(npairs_):
            n0 = int(values[0][i]*N0)
            n1 = int(values[1][i]*N1)
            ct = np.array([[n0, N0-n0],[n1, N1-n1]])
            o, p = fisher_exact(ct)
            pval[i] = p * d * (d-1)/2

    if Wilcoxon:
        pairs = [varSel[pairs[0]], varSel[pairs[1]]]
    return pairs, diff0, values, pval

