import matplotlib
#matplotlib.use("QT5Agg")
import numpy as np
from covering import covering, covering_run, learn_covering
import mnist
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
import xgboost as xgb
from skimage import feature
plt.ion()

p = 1e-4
r = 100
nseeds = 10
Global = True
relax = False
covType = 'pixel'
#covType = 'pairs'
one_vs_all = [[i] for i in range(10)]

def plot_cov_pixels(cov0, figname = 'Fig', image = None, title = 'Quadratic covering', relax = None):
    if image is None:
        ns = 1
    else:
        ns = 2
    pts = np.zeros((cov0.shape[0], 2))
    for j in range(pts.shape[0]):
        pts[j, 0] = 27 - int(np.floor(cov0[j] / 28))
        pts[j, 1] = cov0[j] % 28
    plt.figure(figname)
    plt.subplot(1, ns, 1)
    plt.plot([-1,-1, 28, 28, -1], [-1, 28, 28, -1, -1], 'k')
    if relax is None:
        plt.scatter(pts[:,1], pts[:,0])
    else:
        print(f'relax: {relax.sum()} {(relax > 0.01).sum()}')
        plt.scatter(pts[:,1], pts[:,0], c=relax>0.01)

    plt.axis('equal')
    plt.axis('off')
    plt.title(title)
    if image is not None:
        plt.subplot(1,2,2)
        plt.imshow(image)
    plt.show()
    plt.pause(10)



def plot_cov(cov0, Isel, Jsel, nsel, image = None, figname = 'Fig', covType = 'pixel'):
    cov_index = np.zeros((cov0.shape[0], 5), dtype=int)
    plt.figure(figname)
    col = ['r', 'b', 'g']
    if image is None:
        ns = 3
    else:
        ns = 4
    for j in range(cov0.shape[0]):
        cov_index[j, 4] = int(np.floor(cov0[j] / nsel))
        k2 = cov0[j] % nsel
        isel = Isel[k2]
        jsel = Jsel[k2]
        cov_index[j, 0] = int(np.floor(isel / 28))
        cov_index[j, 1] = isel % 28
        cov_index[j, 2] = int(np.floor(jsel / 28))
        cov_index[j, 3] = jsel % 28
        plt.subplot(1, ns, cov_index[j, 4]+1)
        plt.plot([cov_index[j, 1], cov_index[j, 3]] , [27 - cov_index[j, 0], 27 - cov_index[j, 2]], col[cov_index[j, 4]])
    if image is not None:
        plt.subplot(1,4,4)
        plt.imshow(image)
        #print(cov_index[j, :])
    plt.subplot(1,ns,1)
    plt.plot([-1,-1, 28, 28, -1], [-1, 28, 28, -1, -1], 'k')
    plt.axis('equal')
    plt.axis('off')
    plt.subplot(1,ns,2)
    plt.plot([-1,-1, 28, 28, -1], [-1, 28, 28, -1, -1], 'k')
    plt.axis('equal')
    plt.axis('off')
    plt.subplot(1,ns,3)
    plt.plot([-1,-1, 28, 28, -1], [-1, 28, 28, -1, -1], 'k')
    plt.axis('equal')
    plt.axis('off')
    plt.show()
    plt.pause(10)
def covering_splits(Y, Z, splits, nseeds = 1, Ytest = None, Ztest = None, r=20, type_result= 'HResidual', method = 'cmim', relax = False):
    if Ytest is not None and Ztest is not None:
        testData = True
    else:
        testData = False
    cls = np.unique(Y)
    nc = cls.shape[0]
    n = Z.shape[1]
#     p1 = np.maximum(Z.sum(axis=0), 1e-10)
#     condp = np.zeros((nc, n))
#     for k,c in enumerate(cls):
#         Z0 = Z[Y == c, :]
#         condp[k, :] = Z0.sum(axis=0) / p1
# #        condp[k, :] = Z0.mean(axis=0)
#
#     VarSelect = np.logical_and(condp.max(axis=0) > 0.1, condp.min(axis=0) < 0.9)
#     VarSelect = np.nonzero(VarSelect)[0]
#     Z_ = Z[:, VarSelect]
#     condp_ = condp[:, VarSelect]
#     d = VarSelect.shape[0]

    if splits is None:
        splits = []
        for k in range(nc):
            splits.append(np.array([k]))

    coverings = []
    log_reg = []
    for k in range(len(splits)):
        print(f'split {splits[k]}')
        clk = cls[splits[k]]
        J0 = np.isin(Y, clk)
        J1 = np.logical_not(J0)
        if testData:
            JT0 = np.isin(Ytest, clk)
            JT1 = np.logical_not(JT0)
        cl0 = np.isin(cls, clk)
        cl1 = np.logical_not(cl0)
        # w = p + condp[JC, :].max(axis=0) /(1e-10 + condp[cl, :])
        #w = np.log((1 + p) / (1 + p - condp_[cl1, :].max(axis=0)))
        # w = p + condp[cl1, :].max(axis=0) / (p + condp[k, :])
        # w0 = np.minimum(1 - 1e-10, w[VarSelect])
        # w0 = w0 / (1 - w0)

        #cov = covering_run(Z_[J0, :], w0, timeLimit=1000, p=p, minSize=minSize, alphaMax=0.98)
        cov = learn_covering(J0, Z, r, nseeds=nseeds, method= method, type_result=type_result, relax=relax, balanced=True)

        # res = 'Covering: '
        # for j in range(cov['J'].shape[0]):
        #         res += f" {cov['J'][j]}"
        #         if j % 10 == 9:
        #             res += '\n'
        # print(res)

        # counts = Z[:, cov['J']].sum(axis=1)
        # print("Training:")
        # #        for c in range(10):
        # #    print(f'class {c}: {(counts[x1Tr==c]>=minSize).mean():.2f} {counts[x1Tr==c].mean():.2f}')
        # print(f'class {0}: {(counts[J0] >= minSize).mean():.2f} {counts[J0].mean():.2f}')
        # print(f'class {1}: {(counts[J1] >= minSize).mean():.2f} {counts[J1].mean():.2f}')
        #
        # if testData:
        #     print("Test:")
        #     counts = Ztest[:, res2].sum(axis=1)
        #     # for c in range(10):
        #     #     print(f'class {c}: {(counts[x1Te==c] >= minSize).mean():.2f} {counts[x1Te==c].mean():.2f}')
        #     print(f'class {0}: {(counts[JT0] >= minSize).mean():.2f} {counts[JT0].mean():.2f}')
        #     print(f'class {1}: {(counts[JT1] >= minSize).mean():.2f} {counts[JT1].mean():.2f}')
        #     # if cov.Status == 2:
        #     #     print('Converged')

        coverings.append(cov)
        Y_ = np.isin(Y, clk)
        X_ = Z[:, coverings[k]['J']]
        # dtrain = xgb.DMatrix(X_, label=Y_)
        # param = {'max_depth': 2, 'eta': 1, 'objective': 'binary:logistic'}
        # evallist = [(dtrain, 'train')]#, (dtest, 'eval')]
        bst = xgb.XGBClassifier(n_estimators=100)
        bst.fit(X_, Y_)
        # lr = LogisticRegression()
        # lr.fit(X_, Y_)
        # log_reg.append(lr)

        # print("Training Logistic Regression:")
        # Ypred = lr.predict_proba(X_)
        # #        for c in range(10):
        # #    print(f'class {c}: {(counts[x1Tr==c]>=minSize).mean():.2f} {counts[x1Tr==c].mean():.2f}')
        # print(f'class {0}: {(Ypred[J0,1] >= 0.5).mean():.2f}')
        # print(f'class {1}: {(Ypred[J1,0] >= 0.5).mean():.2f}')
        #bst.evals_result()

        if testData:
            print("Test LR:")
            X_ = Ztest[:, coverings[k]['J']]
            # Ypred = lr.predict_proba(X_)
            dYpred = bst.predict_proba(X_)
            # for c in range(10):
            #     print(f'class {c}: {(counts[x1Te==c] >= minSize).mean():.2f} {counts[x1Te==c].mean():.2f}')
            # print(f'class {0}: {(Ypred[JT0,1] >= 0.5).mean():.2f}')
            # print(f'class {1}: {(Ypred[JT1,0] >= 0.5).mean():.2f}')
            print(f'XGB class {0}: {(dYpred[JT0, 1] >= 0.5).mean():.2f}')
            print(f'XGB class {1}: {(dYpred[JT1, 0] >= 0.5).mean():.2f}')
        # if cov.Status == 2:
        #     print('Converged')



    return coverings, log_reg


N = 50000
NTe = 10000
random_split = False

images = mnist.train_images()
labels = mnist.train_labels()
imTest = mnist.test_images()
labTest = mnist.test_labels()
ind = np.indices(images[0,:,:].shape)
if covType == 'pixel':
    d = images.shape[1] * images.shape[2]
else:
    dst = np.sqrt(((ind[:,:,:, None, None] - ind[:,None,None,:,:])**2).sum(axis=0))
    D = ind.shape[1]*ind.shape[2]
    dst = np.reshape(dst, (D,D))
    select = np.logical_and(dst < 3,  dst > 0)
    print(select.sum())
    Isel, Jsel = np.nonzero(select)
    nsel = select.sum()
    d = 3*select.sum()
cls = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
x0Tr = np.zeros((len(cls) * N, d))
x1Tr = np.zeros(len(cls) * N, dtype=int)
k0 = np.zeros(10, dtype=int)
mean_img = np.zeros((10, 28, 28))
kk = 0
for k in range(len(images)):
    if labels[k] in cls and k0[labels[k]] < N:
        #im = feature.canny(images[k, :, :])
        im = np.ravel(images[k,:,:]) > 128
        # if k==240:
        #     I, J = np.nonzero(images[k,:,:] > 128)
        #     plt.figure(10)
        #     plt.subplot(121)
        #     plt.scatter(J, 28 - I)
        #     plt.axis('equal')
        #     plt.subplot(122)
        #     plt.imshow(images[k, :, :])
        #     plt.show()
        if covType == 'pixel':
            x0Tr[kk, :] = im
        else:
            im3 = np.logical_and(im[:, None] == 0, im[None, :] == 0)[select]
            im2 = np.logical_and(im[:, None] == 0, im[None, :] == 1)[select]
            im1 = np.logical_and(im[:, None] == 1, im[None, :] == 1)[select]
            x0Tr[kk, :d] = np.concatenate((im1, im2, im3), axis=0)
        #x0Tr[kk, :d] = np.ravel(im)
        #x0Tr[kk, d:] = 1 - np.ravel(images[k, :,:]) / 255.
        x1Tr[kk] = cls.index(labels[k])
        mean_img[x1Tr[kk], :, :] += images[k, :,:] > 128
        k0[labels[k]] += 1
        kk += 1
if kk < len(cls) * N:
    x0Tr = x0Tr[0:kk, :]
    x1Tr = x1Tr[0:kk]
for k in range(10):
    mean_img[k, :, :] /= k0[k]

x0Te = np.zeros((len(cls) * NTe, d))
x1Te = np.zeros(len(cls) * NTe, dtype=int)
k0 = np.zeros(10, dtype=int)
kk = 0
for k in range(len(imTest)):
    if labTest[k] in cls and k0[labTest[k]] < NTe:
        # im = feature.canny(imTest[k, :, :])
        # x0Te[kk, :d] = np.ravel(im)
        im = np.ravel(imTest[k,:,:]) > 128
        if covType == 'pixel':
            x0Te[kk, :] = im
        else:
            im3 = np.logical_and(im[:, None] == 0, im[None, :] == 0)[select]
            im2 = np.logical_and(im[:, None] == 0, im[None, :] == 1)[select]
            im1= np.logical_and(im[:, None] == 1, im[None, :] == 1)[select]
            x0Te[kk, :d] = np.concatenate((im1, im2, im3), axis=0)
        # x0Te[kk, d:] = 1-x0Te[kk,:d]
        x1Te[kk] = cls.index(labTest[k])
        k0[labTest[k]] += 1
        kk += 1
if kk < len(cls) * NTe:
    x0Te = x0Te[0:kk, :]
    x1Te = x1Te[0:kk]

p = 0.001
minSize = 10
S = images[0,:,:].shape


Z = x0Tr > 0.5
ZTest = x0Te > 0.5

if relax:
    rx = '_rx'
else:
    rx = ''
if Global:
    cov0 = learn_covering(x1Tr, Z, r=r, nseeds=nseeds, method='cmim', type_result='HResidual', relax = relax, maxselect=2000)
    figname = f'Global'
    # plt.figure(figname)
    if covType == 'pixel':
        plot_cov_pixels(cov0['J'], image = mean_img.mean(axis=0), figname=figname, title=figname, relax = cov0['x'])
    else:
        plot_cov(cov0['J'], Isel, Jsel, nsel, image = mean_img.mean(axis=0), figname=figname)
    # plt.title(figname)
    plt.savefig(f"Plots/MNIST_all_{r}{rx}.png")

    X_ = Z[:, cov0['J']]
    print(f'Running XGBoost with {X_.shape[1]} variables')
    bst = xgb.XGBClassifier(n_estimators=100)
    bst.fit(X_, x1Tr)
    Ypred = bst.predict(X_)
    print(f'XGB good classification training: {np.equal(Ypred, x1Tr).mean():.2f}')

    X_ = ZTest[:, cov0['J']]
    Ypred = bst.predict(X_)
    print(f'XBG good classification test: {np.equal(Ypred, x1Te).mean():.2f}')

    #plt.ion()



splits = []
if random_split:
    nsplit = 20
else:
    nsplit = len(one_vs_all)
if random_split:
    for k in range(nsplit):
        splits.append(np.random.choice(10, 5, replace=False))
else:
    splits = one_vs_all

for sp in splits:
    cov, log_reg = covering_splits(x1Tr, Z, [sp], Ytest = x1Te, Ztest=ZTest, r=r, nseeds=nseeds, method = 'cmim', type_result='HResidual',
                                   relax=relax)
    figname = f'Split  {sp}'
    #plt.figure(figname)
    if covType == 'pixel':
        plot_cov_pixels(cov[0]['J'], image=mean_img[sp[0], :, :], figname=figname, title=figname, relax=cov[0]['x'])
    else:
        plot_cov(cov[0]['J'], Isel, Jsel, nsel, image=mean_img[sp[0], :, :], figname=figname)
    plt.savefig(f"Plots/MNIST_{sp}_{r}{rx}.png")

    #plt.title(figname)
# Ypred = []
# counts = np.zeros((10, ZTest.shape[0]))
# for k,cl1 in enumerate(splits):
#     X = ZTest[:, coverings[k]].sum(axis=1)
#     Ypred = log_reg[k].predict_proba(X[:, None])
#     counts += Ypred[None, :, 0]
#     counts[splits[k], :] += Ypred[None, :, 1] - Ypred[None, :,0]
#
# bestclass = counts.argmax(axis=0)
# for c in range(10):
#     print(f'class {c}: {(bestclass[x1Te == c] == c).mean():.2f}')
#
    #plt.show()

