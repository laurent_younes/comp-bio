import numpy as np

# returns quantile transforms of each row of z0_
# If positive is True, negative values of z0_ are treated as zeros
# returns z1, z2 between 0 and 1 where:
# z1[k, l] is the rank of z0[k,l] in z0[k,.] divided by the number of columns
# z2[k,l] is zero if z0[k,l]=0 and the quantile transform of the of z0[k,.] in which all zeros entries are groupes as one otherwise.

def computeQuantiles(z0_, positive = True):
    if positive:
        z0  = np.maximum(z0_, 0)
    else:
        z0 = z0_
    nIN = z0.shape[1]
    z1 = np.zeros(z0.shape)
    z2 = np.zeros(z0.shape)
    for kk in range(z0.shape[0]):
        I0 = np.argsort(z0[kk, :])
        zz = z0[kk, :]
        u0, u1, u2, u3 = np.unique(zz, return_index=True, return_inverse=True, return_counts=True)
        rk0 = u1[u2]
        rk = np.zeros(z0.shape[1])
        rk[I0] = rk0
        rk = np.cumsum(u3)[u2]
        #rk = rk[I0]
        I = z0[kk, :] > 1e-10
        z1[kk, I] = rk[I] / (nIN)
        nI = (z0[kk, :] < 1e-10).sum()
        z2[kk, I] = (rk[I] - nI) / (nIN - nI)

        u = z2[kk, :].min()
        if u < 0:
            print('???')

    res = {'With_zeros': z1, 'Without_zeros':z2}
    return res