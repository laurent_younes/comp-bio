import numpy as np
#from numpy.char import lstrip
import pandas as pd
from covering import covering_run, cond_entropy, quadratic_covering, quadratic_covering_proximal, quadratic_covering_greedy
import xgboost as xgb
from covering import cmim2
from scipy.sparse.csgraph import dijkstra


fileIn = 'Data/rawCountTelley.csv'
counts = pd.read_csv(fileIn, index_col=0).T
birthAndAge = counts.index.to_numpy(dtype=str)
birth = counts.index.to_numpy(dtype=str)
age = counts.index.to_numpy(dtype=str)
for i in range(birthAndAge.shape[0]):
    birthAndAge[i] = birthAndAge[i].split('_')[0]
    birth[i] = birthAndAge[i].split('.')[0]
    age[i] = birthAndAge[i].split('.')[1]

# birthAndAgeCls = np.unique(birthAndAge)
# birthCls = np.unique(birth)
# ageCls = np.unique(age)

labels = birthAndAge
cls = np.unique(labels)
genes0 = counts.columns
counts = counts.to_numpy(dtype=int)
testData = True
globalPanelSize = 100
panelSize = 50
tr = 'HResidual'
method = 'cmim'
#method = 'quadratic'

minSize = 1
minCount = 1
p = 1e-4
nc = cls.shape[0]
d = counts.shape[1]
if testData:
    repeats = 10
    combinedRate = np.zeros(repeats)
else:
    repeats = 1

cls, Y0 = np.unique(labels, return_inverse=True)
for nrep in range(repeats):
    print(f'\n\n Repeat {nrep+1}')
    if testData:
        inTest = np.zeros(labels.shape[0], dtype=bool)
        for l in cls:
            J = np.nonzero(labels == l)[0]
            J0 = np.random.choice(J.shape[0], J.shape[0]//5)
            inTest[J[J0]] = True
        inTrain = np.invert(inTest)
        Y = Y0[inTrain]
        X = counts[inTrain, :]
        Ytest = Y0[inTest]
        Xtest = counts[inTest, :]
    else:
        Y = Y0
        X = counts
        Ytest = None
        Xtest = None

    condp0 = np.zeros((nc, d))
    condp1 = np.zeros((nc, d))
    pc = np.zeros(nc)
    Z = X >= minCount
    # p1 = np.minimum(np.maximum(Z.mean(axis=0), 1e-10), 1-1e-10)
    # for k, c in enumerate(cls):
    #     Z0 = Z * (Y == c)[:, None]
    #     pc[k] = (Y == c).mean()
    #     Zm = Z0.mean(axis = 0)
    #     condp1[k, :] = Zm / p1
    #     condp0[k, :] = (pc[k]-Zm) / (1-p1)

    # H = - (1-p1) * (condp0*np.log(np.maximum(condp0, 1e-10))).sum(axis=0) \
    #     - p1*(condp1*np.log(np.maximum(condp1, 1e-10))).sum(axis=0)
    #
    # selectSize = min(20000, H.shape[0]-1)
    # VarSelect = np.argpartition(H, selectSize)[:selectSize]
    # nselect = VarSelect.shape[0]
    # votes = np.zeros(nselect, dtype=int)
    # selections = np.zeros(nselect)
    # Z0 = Z[:, VarSelect]
    # genes = genes0[VarSelect]
    Z0 = Z
    genes = genes0
    if method == 'cmim':
        entropy0_, entropy_, selection_ = cond_entropy(Y, Z0, type_result=tr, selectThreshold = 0.99, maxSelect=200,
                                                            balanced=True)
        J1 = cmim2(entropy_, entropy0_, globalPanelSize, timeLimit=300)
        J10_ = selection_[J1]
        start = np.zeros(Z0.shape[1])
        start[J10_] = 1
    else:
        start = None

    entropy0, entropy, selection = cond_entropy(Y, Z0, type_result=tr, selectThreshold = 0.99, maxSelect=1000, balanced=True)

    if method == 'cmim':
        J1  = cmim2(entropy, entropy0, globalPanelSize, timeLimit=600, start=start[selection])
        J10 = selection[J1]
    else:
        _, x_ = quadratic_covering_proximal(entropy, globalPanelSize, type_result=tr, weight=0.)
        J1, x_ = quadratic_covering_proximal(entropy, globalPanelSize, type_result=tr, weight=10., init=x_)
        x = np.zeros(Z0.shape[1])
        #J0 = selection[J]
        J10 = selection[J1]
        x[selection] = x_
        J10 = J10[np.argsort(x[J10])][::-1]

    s=''
    for l, g in enumerate(J10):
        s += genes[g] + f' '
        if (l + 1) % 10 == 0 and (l + 1) != len(J10):
            s += '\n'
    print(s)

    if testData:
        bst = xgb.XGBClassifier(n_estimators=50)
        X_ = Z0[:, J10]
        # _, YYtest = np.unique(Ytest, return_inverse=True)
        bst.fit(X_, Y)
        Ypred = bst.predict(X_)
        print(f'XGB good classification training (LP): {np.equal(Ypred, Y).mean():.2f}')
        # Z0Test = Xtest[:, VarSelect] >= minCount
        Z0Test = Xtest >= minCount
        X_ = Z0Test[:, J10]
        Ypred = bst.predict(X_)
        print(f'XGB good classification test (LP): {np.equal(Ypred, Ytest).mean():.2f}')

        splitTests = np.zeros((Ytest.shape[0], cls.shape[0]), dtype=int)


    combinedSelection = np.zeros(Z0.shape[1], dtype=int)
    for k in range(cls.shape[0]):
        print(f'\nsplit {cls[k]}')
        Yk = Y==k

        if method == 'cmim':
            entropy0_, entropy_, selection_ = cond_entropy(Yk, Z0, type_result=tr, selectThreshold=0.99,
                                                               maxSelect=200,
                                                               balanced=True)
            J1 = cmim2(entropy_, entropy0_, globalPanelSize, timeLimit=300)
            J10_ = selection_[J1]
            start = np.zeros(Z0.shape[1])
            start[J10_] = 1
        else:
            start = None

        entropy0, entropy, selection = cond_entropy((Y==k).astype('int64'), Z0, type_result=tr,
                                                        selectThreshold=0.975, maxSelect=1000)
        #init = np.random.random(x.shape)
        #init *= panelSize / init.sum()
        init = np.zeros(entropy.shape[0])
        ones = np.random.choice(entropy.shape[0], panelSize, replace=False)
        init[ones] = 1
        if method == 'cmim':
            J = cmim2(entropy, entropy0, panelSize, timeLimit=1000, start=start[selection])
            J0 = selection[J]
        else:
            _, x_ = quadratic_covering_proximal(entropy, panelSize, type_result=tr, weight=0, init=init)
            J, x_ = quadratic_covering_proximal(entropy, panelSize, type_result=tr, weight=10, init=x_)
            x = np.zeros(Z0.shape[1])
            J = selection[J]
            x[selection] = x_

            combinedSelection[J] = 1
            J0 = J[np.argsort(x[J])][::-1]

        s = ''
        for l,g in enumerate(J0):
            s += genes[g] + f' '
            if (l + 1) % 10 == 0 and (l+1) != len(J0):
                s += '\n'
        print(s)
        if testData:
            bst = xgb.XGBClassifier(n_estimators=50)
            X_ = Z0[:, J]
            bst.fit(X_, Yk)
            Ypred = bst.predict(X_)
            nk = (Yk).sum()
            nnk = (Y!=k).sum()
            print(f'XGB good classification training: {0.5 * ((Ypred*(Yk)).sum()/nk + ((1-Ypred)*(Y !=k)).sum()/nnk):.2f}')
            X_ = Z0Test[:, J]
            Ypred = bst.predict(X_)
            splitTests[:, k] = Ypred
            nk = (Ytest==k).sum()
            nnk = (Ytest!=k).sum()
            print(f'XGB good classification test: {0.5 * ((Ypred*(Ytest==k)).sum()/nk + ((1-Ypred)*(Ytest !=k)).sum()/nnk):.2f}')

    if testData:
        correct = 0
        correct_ambiguous = 0
        n_ones = splitTests.sum(axis=1)
        for j in range(Ytest.shape[0]):
            if splitTests[j, Ytest[j]] == 1:
                correct_ambiguous += 1
                if n_ones[j] < 2:
                    correct += 1
        print(f'1 vs all classification: correct {correct/Ytest.shape[0]:.2f}')
        print(f'1 vs all classification: correct or ambiguous {correct_ambiguous/Ytest.shape[0]:.2f}')

        J = np.nonzero(combinedSelection)[0]
        print(f'Combined selection: {J.shape[0]} genes')
        bst = xgb.XGBClassifier(n_estimators=50)
        X_ = Z0[:, J]
        bst.fit(X_, Y)
        Ypred = bst.predict(X_)
        print(f'XGB combined good classification training: {np.equal(Ypred, Y).mean():.2f}')
        # Z0Test = Xtest[:, VarSelect] >= minCount
        Z0Test = Xtest >= minCount
        X_ = Z0Test[:, J]
        Ypred = bst.predict(X_)
        combinedRate[nrep] = np.equal(Ypred, Ytest).mean()
        print(f'XGB combined good classification test: {combinedRate[nrep]:.2f}')

print(f'\n\n Average classification rate: {combinedRate.mean():.2f}')
# QJ = VarSelect[J]
    # print(genes[QJ])
    # clk = cls[k]
    # J0 = Y == cls[k]
    # J1 = np.logical_not(J0)
    # if testData:
    #     JT0 = np.isin(Ytest, clk)
    #     JT1 = np.logical_not(JT0)
    # cl0 = np.isin(cls, clk)
    # cl1 = cls != clk
    # #Z0 = counts[J0, :]
    # #w = p + condp[cl1, :].max(axis=0) / (1e-10 + condp[k, :])
    # #w = p + condp[cl1, :].max(axis=0) / (1e-10 + p1)
    # w = p + condp[cl1, :].max(axis=0) / (p + condp[k,:])
    # #w = np.log((1 + p) / (1 + p - condp[cl1, :].max(axis=0)))
    # Zk = Z0[k, :]
    # w0 = np.minimum(1-1e-10, w[VarSelect])
    # w0 = w0 / (1-w0)
    # #w0 = w[VarSelect]
    #
    #
    # cov = covering_run(Z0[J0, :], w0, timeLimit=1000, p=p, minSize=minSize, alphaMax=0.98)
    #
    # res = 'Covering: '
    # res2 = []
    # j = 0
    # for i_ in range(VarSelect.shape[0]):
    #     i = VarSelect[i_]
    #     if cov.x[i_] > 0.5:
    #         res += f' {genes[i]}'
    #         res2 += [i]
    #         j += 1
    #         if j % 10 == 9:
    #             res += '\n'
    # print(res)
    # print(w[res2])
    # print(w[res2].sum())
    #
    # cts = Z[:, res2].sum(axis=1)
    # print("Training:")
    # for c in cls:
    #     print(f'class {c}: {(cts[Y==c]>=minSize).mean():.2f} {cts[Y==c].mean():.2f}')
    # # print(f'class {0}: {(cts[J0] >= minSize).mean():.2f} {cts[J0].mean():.2f}')
    # # print(f'class {1}: {(cts[J1] >= minSize).mean():.2f} {cts[J1].mean():.2f}')
    #
    # if testData:
    #     cts = (Xtest[:, res2] >= minCount).sum(axis=1)
    #     # Jt0 = Ytest == cls[k]
    #     # Jt1 = np.logical_not(Jt0)
    #     print("Test:")
    #     for c in cls:
    #         print(f'class {c}: {(cts[Ytest == c] >= minSize).mean():.2f} {cts[Ytest == c].mean():.2f}')
    #     # #        for c in range(10):
    #     # #    print(f'class {c}: {(counts[x1Tr==c]>=minSize).mean():.2f} {counts[x1Tr==c].mean():.2f}')
    #     # print(f'class {0}: {(cts[Jt0] >= minSize).mean():.2f} {cts[Jt0].mean():.2f}')
    #     # print(f'class {1}: {(cts[Jt1] >= minSize).mean():.2f} {cts[Jt1].mean():.2f}')
