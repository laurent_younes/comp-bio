import numpy as np
import pandas as pd
from sklearn.manifold import SpectralEmbedding, LocallyLinearEmbedding, TSNE
from sklearn.cluster import KMeans
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def analyze(df_div, df_sig, df_pheno, className, useBest=True, algorithm="spectral"):
    df0 = pd.merge(df_div,df_pheno[className], left_index=True, right_index=True)
    divg = df0.to_numpy()[:,:-1].astype(float)
    sig = df_sig.to_numpy(dtype=float) > 0.5
    classes = np.unique(df0[className].dropna())
    nc = classes.size

    if useBest:
        allSig = np.dot(divg,sig.T)
        val = allSig.sum(axis=0)
        bestSig = np.argmax(val)
        signat = divg[:,sig[bestSig,:]]
    else:
        allSig = sig.max(axis=0)
        signat = divg[:, allSig]

    if algorithm == "spectral":
        inter = np.logical_and(signat[:, np.newaxis,:], signat[np.newaxis, :, :]).sum(axis=2)
        union = np.logical_or(signat[:, np.newaxis,:], signat[np.newaxis, :, :]).sum(axis=2)
        aff0 = inter / np.maximum(union, 1)
        c = 10
        aff = np.exp((aff0-1)/c)
        embedding = SpectralEmbedding(n_components=max(nc,3), affinity='precomputed')
        s2 = embedding.fit_transform(aff)
    elif algorithm == "lle":
        embedding = LocallyLinearEmbedding(n_components=max(nc,3))
        s2 = embedding.fit_transform(signat)
    elif algorithm == "tsne":
        embedding = TSNE(n_components=3, metric='precomputed')
        inter = np.logical_and(signat[:, np.newaxis,:], signat[np.newaxis, :, :]).sum(axis=2)
        union = np.logical_or(signat[:, np.newaxis,:], signat[np.newaxis, :, :]).sum(axis=2)
        aff0 = inter / np.maximum(union, 1)
        s2 = embedding.fit_transform(1-aff0)
    else:
        print("Unknown algorithm")
        return

    v0 = 0
    v1 = 1
    v2 = 2
    colors = ['r', 'g', 'b', 'm', 'k']

    status = []
    for cl in classes:
        status.append(df0[className] == cl)
    fig = plt.figure(1)
    ax = fig.add_subplot(111, projection='3d')
    for k,st in enumerate(status):
        ax.scatter(s2[st, v0], s2[st, v1], s2[st,v2], label = classes[k], color=colors[k], marker='o')
    ax.legend()
    plt.title(className)
    plt.axis('off')

    kmeans = KMeans(n_clusters=nc, random_state=0).fit(s2)
    cls = kmeans.predict(s2)
    status = []
    for cl in range(nc):
        status.append(cls == cl)
    fig = plt.figure(2)
    ax = fig.add_subplot(111, projection='3d')
    #ax.plot(s2[status, v0], s2[status, v1], s2[status,v2], 'g*')
    for k,st in enumerate(status):
        ax.scatter(s2[st, v0], s2[st, v1], s2[st,v2], label = k, color=colors[k], marker='o')
    ax.legend()
    plt.title(className+ " Clusters")
    plt.axis('off')




def run(tissue="breast"):
    if tissue == "breast":
        df_div = pd.read_csv('DATAandResult/BreastGeneMotif.csv')
        df_sig = pd.read_csv('DATAandResult/BreastGeneMotifSig.csv')
        df_pheno = pd.read_csv('DATAandResult/TCGA_Pheno2.csv', index_col=1)
        #classColumn = 'PAM50_mRNA_nature2012'
        classColumn = 'ER_Status_nature2012'
    else:
        print("Unknown tissue type")
        return

    df_div = df_div.T
    analyze(df_div, df_sig, df_pheno, classColumn, useBest=True, algorithm="tsne")

if __name__=="__main__":
    plt.ion()
    #useBest = False
    #classColumn = 'PAM50_mRNA_nature2012'
    #classColumn = 'ER_Status_nature2012'
    run("breast")
    print('All done!')
    plt.ioff()
    plt.show()
