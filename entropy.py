import numba
import numpy as np
from functools import partial
from itertools import combinations
from scipy.optimize import minimize
from scipy.stats import rv_discrete
from scipy.stats.contingency import crosstab
from scipy.spatial.distance import pdist
from scipy.special import digamma
from numba import jit, prange, int64, int16
from numba.typed import List
import tqdm


MechPred = "/Users/younes/Dropbox (MechPred)/MechPred"


def distEntropy(X, withCI=True):
    y = pdist(X, lambda u,v: np.fabs(u-v).sum())
    #h = np.histogram(y, bins=10, density=True)
    result = {'entropy':entropy(y)}
    return result

def entropy(X, with_correction=True, balanced = False):
    val,counts = np.unique(X, return_counts=True, axis=0)
    if balanced:
        return np.log(val.shape[0])
    prob = counts/X.shape[0]
    H__ = - (prob*np.log2(prob)).sum()
    H_ = np.log2(np.e)*(prob *(np.log(X.shape[0]) - digamma(counts) - (1 - 2*(counts%2))/(counts*(counts+1)))).sum()
    #H = - (prob*np.log2(prob)).sum() + (2**X.shape[1] - 1)/(2*X.shape[0])
    if with_correction:
        return H_
    else:
        return H__

def entropy_CI(X, subsets, nsim=1000, nsample = None, epsilon=0., maxSize=4):
    nsub = len(subsets)
    psub = []
    pval = []
    n = X.shape[0]
    if nsample is None:
        nsample = n
    ent0 = 0
    for sub in subsets:
        ent0 += entropy(X[:,sub], with_correction=False)

    for sub in subsets:
        ns = len(sub)
        sh = (2,)*ns
        pr = np.zeros(2**ns)
        val, counts = np.unique(X[:, sub], return_counts=True, axis=0)
        for k, v in enumerate(val):
            pr[np.ravel_multi_index(v,sh)] = counts[k]/n
        #pr = (1-epsilon)*pr.reshape(sh) + epsilon/len(pr)
        pr = (1-epsilon)*pr + epsilon/len(pr)
        x_ = [[]]
        for i in range(ns):
            x__ = []
            for s in x_:
                x__.append(s + [0])
                x__.append(s + [1])
            x_ = x__
        psub.append(rv_discrete(values=(np.arange(len(x_)), pr)))
        pval.append(np.array(x_))

    Y = np.zeros((nsample, X.shape[1]), dtype= int)
    ent = np.zeros(nsim)
    for k in range(nsim):
        for j,sub in enumerate(subsets):
            ent[k] += entropy(pval[j][psub[j].rvs(size=nsample)])
            #Y[:,sub] = pval[j][psub[j].rvs(size=nsample)]
        #ent[k] = entropyUB(Y, maxSize=maxSize, withCI=False)['entropy']

    #CI = [np.quantile(ent, 0.05), np.quantile(ent,0.95)]

    return ent,ent0

def entropyTuple(X, size=3):
    N = X.shape[1]
    ub = 0
    selected = list(combinations(range(N), size))
    for s in selected:
        ub += entropy(X[:,s])
    ub /= len(selected)*size/N
    return ub


def entropyUB(X, maxSize=4, withCI = False):
    selected = []
    for i in range(X.shape[1]):
        selected.append([i])
    selectedValues = list(map(lambda I: entropy(X[:,I]), selected))
    candidates = []
    for i in range(X.shape[1]):
        for j in range(i+1, X.shape[1]):
            candidates.append([[i],[j]])
    candidateValues = list(map(lambda I: -entropy(X[:,I[0]+I[1]]), candidates))
    for k,I in enumerate(candidates):
        candidateValues[k] += selectedValues[selected.index(I[0])] + selectedValues[selected.index(I[1])]

    while len(candidates) > 0:
        best = np.argmax(candidateValues)
        J = candidates[best]
        I1 = J[0]
        I2 = J[1]
        i1 = selected.index(I1)
        h1 = selectedValues[i1]
        selectedValues.pop(i1)
        selected.remove(I1)
        i2 = selected.index(I2)
        h2 = selectedValues[i2]
        selectedValues.pop(i2)
        selected.remove(I2)
        selected.append(I1+I2)
        bestH = -candidateValues[best]+h1+h2
        selectedValues.append(bestH)
        newCand = candidates.copy()
        newCandVal = candidateValues.copy()
        for k in range(len(candidates)-1, -1, -1):
            JJ = candidates[k]
            if JJ[0] == I1 or JJ[0] == I2 or JJ[1] == I1 or JJ[1] == I2:
                newCand.pop(k)
                newCandVal.pop(k)
        candidates = newCand
        candidateValues = newCandVal

        J = I1+I2
        for i in range(len(selected)-1):
            if len(selected[i]) + len(J) <= maxSize:
                J2 = [selected[i], J]
                H = entropy(X[:,J2[0] +J2[1]])
                if (selectedValues[i]+bestH - H)>0:
                    candidates.append(J2)
                    candidateValues.append(selectedValues[i]+bestH - H)
    result = {'entropy':np.array(selectedValues).sum(), 'subsets':selected, 'subset_entropies':selectedValues}
    if withCI:
        result['mc'], result['ent0'] = entropy_CI(X, selected, maxSize=maxSize) #, nsample=30)
    return result






def probExact(par0, par1):
    d = par0.shape[0]
    x = np.ones((2,1))
    x[1,0] = -1
    n0 = 2
    for k in range(1,d):
        _x = np.zeros((2*n0,k+1))
        _x[:n0,:k] = x
        _x[n0:,:k] = x
        _x[:n0,k] = 1
        _x[n0:,k] = -1
        n0 *= 2
        x = _x

    E = np.dot(x,par0)
    for k in range(d):
        E += x[:,k] * (x[:,(k+1):]*(par1[k,(k+1):])[np.newaxis,:]).sum(axis=1)

    P = E-E.max()

    P = np.exp(P)
    P /= P.sum()

    return P, E, x


def logLikelihood(par, X, U):
    d = X.shape[1]
    tr1 = np.triu_indices(d, 1)
    if U is None:
        N = X.shape[1]
        H0 = X.sum(axis=0)/N
        H1 = (X[:, :, np.newaxis]*X[:, np.newaxis, :]).sum(axis=0)/N
        H1 = H1[tr1]
        U = np.concatenate((H0,H1))

    par0 = par[:d]
    par1 = np.zeros((d,d))
    par1[np.triu_indices(d,1)] = par[d:]
    P, E, x = probExact(par0, par1)
    pow2 = 2**(np.arange(d))
    IX = np.dot(X<0, pow2).astype(int)
    L = np.log(P[IX]).sum()

    g0 = (x * P[:,np.newaxis]).sum(axis=0)
    g1 = (x[:,:,np.newaxis]*x[:,np.newaxis,:]*P[:, np.newaxis,np.newaxis]).sum(axis=0)
    g1 = g1[np.triu_indices(d,1)]
    #print(-L)
    return -L, -U+np.concatenate((g0, g1))

def entropyExact(par0, par1):
    P, E, x = probExact(par0, par1)
    I= P > 1e-100

    H = - (P[I]*np.log(P[I])).sum()

    p = (1+(x * P[:,np.newaxis]).sum(axis=0))/2
    I= p > 1e-100
    H0 = - (p[I]*np.log(p[I]) + (1-p[I])*np.log(1-p[I])).sum()
    l2 = np.log(2)
    return H/l2, H0/l2




def entropyMRF(par0, par1, n = 10, ns=10000, exact=False, withCI=True):
    d = par0.shape[0]
    if exact == True or d<21:
        return entropyExact(par0,par1)
    t = np.linspace(0,1,num=n+1)
    T = len(t)
    x = np.ones(d)
    tr1 = np.triu_indices(d,1)
    Emean = np.zeros(T)
    Emean0 = np.zeros((T,d))
    for k, _t in enumerate(t):
        for it in range(ns):
            uni = np.random.uniform(0,1,d)
            for i in range(d):
                E = 2 * (par0[i] + _t*((par1[i, (i + 1):] * x[(i + 1):]).sum() + (par1[:i, i] * x[:i]).sum()))
                u0 = 1/(1+np.exp(-E))
                x[i] = 2*(uni[i] < u0) - 1
        #for k in range(T):
            E = (par1[tr1]*(x[np.newaxis,:]*x[:,np.newaxis])[tr1]).sum()
            Emean[k] += (E-Emean[k])/(it+1)
            Emean0[k, :] += (x -Emean0[k,:])/(it+1)


    p0 = (Emean0[0,:]+1)/2 #np.exp(par0)/(np.exp(par0) + np.exp(-par0))
    Entropy0 = - (p0*np.log(p0)).sum() - ((1-p0)*np.log(1-p0)).sum()
    Entropy = - (par0*Emean0[-1,:]).sum() - Emean[-1] + (np.log(np.exp(-par0)+np.exp(par0))).sum()
    for k in range(T-1):
        Entropy += 0.5*(Emean[k+1] + Emean[k])*(t[k+1]-t[k])

    l2 = np.log(2)
    return Entropy/l2, Entropy0/l2


def trainMRF(X, pr = 0.5, npr = 10, n_iter=100000, sizeLimit = -1, replacement=False):
    N = X.shape[0]
    if sizeLimit>0:
        if not replacement and N > sizeLimit:
            I = np.random.permutation(N)
            X = X[I[:sizeLimit], :]
            N = sizeLimit
        else:
            I = np.random.randint(0,N,sizeLimit)
            X = X[I,:]
            N = sizeLimit

    d = X.shape[1]
    if d<21:
        return trainMRF_exact(X, pr, npr, n_iter)

    prior = 2*pr - 1

    H0 = (X.sum(axis=0) + npr*prior)/(N+npr)
    tr1 = np.triu_indices(d,1)
    H1 = ((X[:, :, np.newaxis]*X[:, np.newaxis, :]).sum(axis=0)+npr*prior**2)/(N+npr)


    par0 = np.zeros(d)
    par1 = np.zeros((d,d))

    step = .01
    a = 0.001
    it = 0
    x = np.ones(d, dtype=int)
    hx0 = np.ones(d)
    hx1 = np.ones((d,d))
    hx0m = np.ones(d)
    hx1m = np.ones((d,d))

    for it in range(n_iter):
        uni = np.random.uniform(0,1,d)
        for i in range(d):
            u0 = np.exp(2*(par0[i] + (par1[i,(i+1):]*x[(i+1):]).sum()+ (par1[:i,i]*x[:i]).sum()))
            if uni[i] < u0/(1+u0):
                x[i] = 1
                hx0[i] = 1
                hx1[i, i:] = x[i:]
                hx1[:i, i] = x[:i]
            else:
                hx0[i] = -1
                hx1[i, i:] = -x[i:]
                hx1[:i, i] = -x[:i]
                x[i] = -1

        s = (step/(1+a*it))
        par0 += s * (H0-hx0)
        hx0m += (hx0-hx0m)/(it+1)
        hx1m += (hx1-hx1m)/(it+1)
#        par1[tr1] += s * (H1[tr1] - hx1[tr1])
        par1 += s * (H1 - hx1)
        #print('par1:', par1[tr1])
        #print('x:', x)
        #print('hx1:', hx1[tr1])
        # prs = "it: {0:d} par0 ".format(it)
        # for u in par0:
        #     prs += "{0:2f} ".format(u)
        # print(prs)
        # print(par1)

    print('H0:', ((H0-hx0m)**2).sum(), 'H1: ', ((H1[tr1]-hx1m[tr1])**2).sum())
    # print(hx1m)

    return par0, par1

def entropyMRF_fit(X, maxVar = 16, n_sde=5000, n_sim=10000, sizeLimit=-1):
    p = X.mean(axis=0)
    s = p * (1 - p)
    if X.shape[1] > maxVar:
        sth = max(np.sort(s)[-maxVar - 1], 0.001)
    else:
        sth = 0.001
    p1 = np.minimum(np.maximum(p[s < (sth + 1e-10)], 1e-10), 1 - 1e-10)
    H1 = -(p1 * np.log2(p1)).sum() - ((1 - p1) * np.log2(1 - p1)).sum()
    keep = (s > sth)
    # if keep.sum() < X.shape[1]:
    #     print('Removing {0:d} variable(s), keeping {1:d}'.format(inp.X.shape[1] - keep.sum(), keep.sum()))
    X_ = 2 * X[:, keep] - 1
    par = trainMRF(X_, npr=1, n_iter=n_sde, sizeLimit=sizeLimit)
    H_, H0 = entropyMRF(par[0], par[1], n=n_sim)
    result = {'entropy':H_+H1}
    return result

def trainMRF_exact(X, pr = 0.5, npr = 1, n_iter=100000):
    N = X.shape[0]
    d = X.shape[1]

    prior = 2*pr - 1

    H0 = (X.sum(axis=0) + npr*prior)/(N+npr)
    tr1 = np.triu_indices(d,1)
    H1 = ((X[:, :, np.newaxis]*X[:, np.newaxis, :]).sum(axis=0)+npr*prior**2)/(N+npr)
    H1 = H1[tr1]

    H = np.concatenate((H0,H1))

    par0 = np.zeros(d)
    par1 = np.zeros((d,d))
    par = np.concatenate((par0, par1[tr1]))

    res = minimize(logLikelihood, par, args=(X,H),method='BFGS', jac=True, tol=1e-5, options={'gtol':1e-4})
    #print('BFGS:', res.fun)
    par = res.x
    par0 = par[:d]
    par1[tr1] = par[d:]


    # step = .1
    #
    # for it in range(n_iter):
    #     L, g = logLikelihood(par, X, H)
    #     g0 = g[:d]
    #     g1 = g[d:]
    #     par0 -= step * g0
    #     par1[tr1] -= step * g1
    #     par = np.concatenate((par0, par1[tr1]))
    #     #print('par1:', par1[tr1])
    #     #print('x:', x)
    #     #print('hx1:', hx1[tr1])
    #     # prs = "it: {0:d} par0 ".format(it)
    #     # for u in par0:
    #     #     prs += "{0:2f} ".format(u)
    #     # print(prs)
    #     # print(par1)
    #
    #     e0 =  np.fabs(g0).max()
    #     e1 =  np.fabs(g1).max()
    #
    #     print('L=', L, 'H0:', e0, 'H1: ', e1)
    #     if e0 <.0001 and e1 <.0001:
    #         break
    # # print(hx1m)

    return par0, par1



## Compute a matrix cotaining the conditional entropy of Y gievn each pair of elements in X
## If MI = True, computes mutual information instead
@jit(nopython=True, parallel= True)
def trivar_counts(Y,X):
    print("computing counts")
    cls = np.unique(Y)
    nc = cls.size
    G = X.shape[1]
    N = X.shape[0]
    bigArray = np.zeros((nc, 2, 2, G, G), dtype="uint16")
    medArray = np.zeros((nc, 2, G), dtype="uint16")
    piy = np.zeros(nc)
    for k in prange(nc):
        J = np.nonzero(Y == k)[0]
        XJ = X[J, :]
        piy[k] = J.shape[0] / N
        for i1 in range(2):
            XJ1 = np.zeros(XJ.shape)
            XJ1[:, :] = XJ == i1
            for i2 in range(2):
                #print(f"{c} {i1} {i2}")
                XJ2 = np.zeros(XJ.shape)
                #bigArray[k, i1, i2, :, :] = np.logical_and(X[J, :, None] == i1, X[J, None, :] == i2).sum(axis=0)
                XJ2[:, :] = XJ == i2
                bigArray[k, i1, i2, :, :] = np.dot(XJ1.T, XJ2).astype("uint16")
            medArray[k, i1, :] = np.sum(XJ1, 0)
    return bigArray, medArray

@jit(nopython=True, parallel = True, debug=False)
def cond_entropy_try(Y, X, weights = None):
    cls = np.unique(Y)
    nc = cls.size
    n = X.shape[0]
    d = X.shape[1]
    print('d=',d)
    #atype = float
    atype0 = numba.int8
    atype = numba.float32
    atype2 = float
    if weights is None:
        weights = np.ones(n).astype(numba.double)
    #else:
        #weights = weights.astype(atype)
    wsum = weights.sum()
    print(weights.shape)
    HXX = np.zeros((d, d), dtype=atype2)
    HYXX = np.zeros((d, d), dtype=atype2)
    HYX = np.zeros(d, dtype=atype2)
    HX = np.zeros(d, dtype=atype2)


    print('first loop')
    for js in range(2):
        xs = X == js
        for iy in range(nc):
            # ymat = (Y == iy).astype(atype)
            # ymat *= weights
            cys = np.zeros((d, nc), dtype=atype2)
            for i1 in range(d):
                for k in range(n):
                    cys[i1, int(Y[k])] += weights[k] * xs[k, i1]
                # v1 = np.ascontiguousarray(xs[:, i1])
                # cys[i1] = (ymat*v1).sum()
        for yc in range(nc):
            HYX += (np.log(cys[:, yc]+1e-10) * cys[:, yc])
        cx = cys.sum(axis=1)
        HX += (np.log(cx + 1e-10) * cx)

    print('second loop')
    for js in range(2):
        xs = (X==js).astype(atype)
        #xs = np.array(X == js)
        #xs = X.T==js
        for jt in range(2):
            #xt = X==jt
            xt = (X == jt).astype(atype)
            # xt = np.array(X == jt, dtype=int64)
            #cst = np.dot(xs.T, xt)
            cst = np.zeros((d,d, nc), dtype=atype2)
            u = 0
            i1 = 0
            # for i1 in prange(d):
            #      u+=1
            # print('u: ', u)
            i1 = 0
            c_ = np.zeros(d)
            dd = d//20
            print(js, jt)
            for i1 in prange(d):
                cs = np.zeros((d, nc), dtype= atype2)
                for i2 in range(d):
                    for k in range(n):
                        cs[i2, int(Y[k])] += xs[k, i1] * xt[k, i2] * weights[k]
                cst[i1, :, :] = cs
            for yc in range(nc):
                HYXX += (np.log(cst[:, :, yc]+1e-10) * cst[:, :, yc])
            csts = cst.sum(axis=2)
            HXX += (np.log(csts+1e-10) * csts)
            # print('HXX', HXX.max(), HXX.min())

    print(HYXX[1,1], HYX[1])
    print(HXX[1,1], HX[1])
    HX = -HX /wsum + np.log(wsum)
    HXX = -HXX /wsum + np.log(wsum)
    HYXX = -HYXX /wsum + np.log(wsum)
    HYX = -HYX /wsum + np.log(wsum)
    return HYX, HX, HYXX, HXX

@jit(nopython=True, parallel= True, debug=False)
def cond_entropy_old(Y, X, type_result = 'H', counts=None, return_counts = False, focus= -1):
    cls = np.unique(Y)
    nc = cls.size
    G = X.shape[1]
    N = X.shape[0]
    if counts is not None:
        bigArray_, medArray_ = counts
    else:
        bigArray_, medArray_ = trivar_counts(Y,X)

    print("computed arrays")

    piy_ = np.zeros(nc)
    for k in range(nc):
        piy_[k] = np.mean(Y==k)
    if focus >= 0:
        bigArray = np.zeros((2, 2, 2, bigArray_.shape[3], bigArray_.shape[4]), dtype="uint16")
        bigArray[1,:,:,:,:] = bigArray_[focus, ...]
        bigArray[0,:,:,:,: ] = bigArray_.sum(axis=0) - bigArray_[focus, ...]
        medArray = np.zeros((2, 2, medArray_.shape[2]), dtype="uint16")
        medArray[1,:,:] = medArray_[focus, ...]
        medArray[0, :,:] = medArray_.sum(axis=0) - medArray_[focus, ...]
        piy = np.zeros(2)
        piy[1] = piy_[focus]
        piy[0] = 1 - piy[1]
        HY = - np.sum(np.log(piy + 1e-10) * piy)
    else:
        bigArray = bigArray_
        medArray = medArray_
        piy = piy_
        HY = - np.sum(np.log(piy_ + 1e-10) * piy_)

    print("Computed focus")
    bigArray2 = np.zeros((bigArray.shape[1], bigArray.shape[2], bigArray.shape[3], bigArray.shape[4]))
    for k in range(bigArray.shape[0]):
        bigArray2 += bigArray[k,:,:,:,:]
    medArray2 = np.zeros((medArray.shape[1], medArray.shape[2]))
    for k in range(medArray.shape[0]):
        medArray2 += medArray[k,:,:]

    if type_result == 'MI':
        entropy = np.zeros((G, G), dtype = 'float32')
        for i1 in range(2):
            for i2 in range(2):
                for k in range(nc):
                    entropy += np.log(bigArray[k, i1, i2, :, :]/N+1e-10) * bigArray[k, i1, i2, :, :]/N
                entropy -= np.log(bigArray2[i1, i2, :, :]/N + 1e-10) * bigArray2[i1, i2, :, :]/N
        entropy += HY
    elif type_result == 'H':
        entropy = np.zeros((G, G), dtype = 'float32')
        for i1 in range(2):
            for i2 in range(2):
                for k in range(nc):
                    entropy -= np.log(bigArray[k, i1, i2, :, :]/N+1e-10) * bigArray[k, i1, i2, :, :]/N
                entropy += np.log(bigArray2[i1, i2, :, :]/N + 1e-10) * bigArray2[i1, i2, :, :]/N
    elif type_result == 'HResidual':
        entropy0 = np.zeros(G, dtype='float32')
        for i1 in range(2):
            for k in range(nc):
                entropy0 -= np.log(medArray[k, i1, :] / N + 1e-10) * medArray[k, i1, :] / N
            entropy0 += np.log(medArray2[i1, :]/N + 1e-10) * medArray2[i1, :]/N

        entropy = np.zeros((G, G), dtype = 'float32')
        for k1 in prange(G):
            for k2 in range(G):
                entropy[k1,k2] = 0.5*(entropy0[k1]+entropy0[k2])
        # entropy = (np.expand_dims(entropy0, axis=1) + np.expand_dims(entropy0, axis=0))/2
        for i1 in range(2):
            for i2 in range(2):
                for k in range(nc):
                    for k1 in prange(G):
                        for k2 in range(G):
                            entropy[k1, k2] += np.log(bigArray[k, i1, i2, k1, k2]/N+1e-10) * bigArray[k, i1, i2, k1, k2]/N
                for k1 in prange(G):
                    for k2 in range(G):
                        entropy[k1, k2] -= np.log(bigArray2[i1, i2, k1, k2]/N + 1e-10) * bigArray2[i1, i2, k1, k2]/N

    else:
        print('Unknown entropy type')
        return np.zeros((G,G), dtype='float32'), None


    for k in range(G):
        entropy[k,k] = 0

    print("computed entropy")

    if return_counts:
        return entropy, (bigArray, medArray)
    else:
        return entropy, None


@jit(nopython=True, parallel = True, debug=False)
def __counts__(JY, J1, J2, w):
    nc = w.shape[0]
    counts = np.zeros((nc, 2, 2))
    for jy in range(nc):
        for j1 in range(2):
            for j2 in range(2):
                #counts[jy, j1, j2] = (JY[jy] * J1[j1] * J2[j2]).sum() * w[jy]
                counts[jy, j1, j2] = (JY[jy] & J1[j1] & J2[j2]).sum() * w[jy]
    return counts


@jit(nopython=True, parallel = True, debug=False)
def cond_entropy(Y, X, type_result = 'H', selectThreshold = -1., maxSelect=-1, balanced = False):
    G = X.shape[1]
    N = X.shape[0]
    print(f'Number of variables: {G}')


    cls = np.unique(Y)
    nc = cls.size
    piy = np.zeros(nc)

    if balanced:
        w = np.zeros(nc)
        k = N % nc
        for i in range(k):
            piy[i] = (N // nc + 1)
            w[i] = piy[i] / np.sum(Y==i)
        for i in range(k, nc):
            piy[i] = N//nc
            w[i] = piy[i] / np.sum(Y == i)
    else:
        w = np.ones(nc)
        for k in range(nc):
            piy[k] = np.sum(Y==k)

    wgt = np.zeros(N)
    for k in range(N):
        wgt[k] = w[Y[k]]


    HY = - (np.log(piy/N) * piy).sum() / N
    # HYX, HYXX, HXX = cond_entropy_try(Y, X)

    entropy0 = np.zeros(G)
    for k1 in prange(G):
        ## Computing H(Y|X[k1]) = H(Y, X[k1]) - H(X[k1])
        counts = np.zeros((nc, 2))
        for ll in range(N):
            counts[int(Y[ll]), int(X[ll, k1])] += w[int(Y[ll])]
        entropy0[k1] -= (np.log(counts/N + 1e-10) * counts).sum()/N
        counts2 = counts.sum(axis=0)
        entropy0[k1] += (np.log(counts2/N + 1e-10) * counts2).sum()/N

    if selectThreshold > 0:
        if maxSelect > 0:
            if maxSelect > G:
                thr = (entropy0/HY).max()
            else:
                thr = min(selectThreshold, np.quantile(entropy0/HY, maxSelect/G))
        else:
            thr = selectThreshold

        selection = np.nonzero(entropy0 < thr*HY)[0]
        if type_result == 'Univariate':
            return entropy0[selection], None, selection

        print(f'Preselected {len(selection)} variables')
        # print((entropy0/HY).min())
        entropy0, entropy, _ = cond_entropy(Y, X[:, selection], type_result = type_result, selectThreshold = -1,
                                                balanced=balanced)
        return entropy0, entropy, selection


    if type_result == 'Univariate':
        return entropy0, None, np.arange(N)

    HYX, HX, HYXX, HXX = cond_entropy_try(Y, X, weights=wgt)
    if type_result == 'MI':
        entropy = np.zeros((G, G))
        ### Computing I(Y, X(k1), X(k2))
        for k1 in range(G):
            for k2 in prange(k1+1, G):
                counts = np.zeros((nc, 2, 2))
                for ll in range(N):
                    counts[int(Y[ll]), int(X[ll, k1]), int(X[ll, k2])] += w[int(Y[ll])]
                counts /= N
                entropy[k1, k2] += (np.log(counts + 1e-10) * counts).sum()
                counts2 = counts.sum(axis=0)
                entropy[k1, k2] -= (np.log(counts2 + 1e-10) * counts2).sum()
        entropy += HY
        for k in range(G):
            entropy[k, k] = 0
            for k2 in range(k):
                entropy[k, k2] = entropy[k2, k]
    elif type_result == 'H':
        ## Computing H(Y|X(k1), X(k2))
        entropy = np.zeros((G, G))
        for k1 in range(G):
            for k2 in prange(k1+1, G):
                counts = np.zeros((nc, 2, 2))
                for ll in range(N):
                    counts[int(Y[ll]), int(X[ll, k1]), int(X[ll, k2])] += w[int(Y[ll])]
                counts /= N
                entropy[k1,k2] -= (np.log(counts+1e-10) * counts).sum()
                counts2 = counts.sum(axis=0)
                entropy[k1,k2] += (np.log(counts2+1e-10) * counts2).sum()
        for k in range(G):
            entropy[k, k] = 0
            for k2 in range(k):
                entropy[k, k2] = entropy[k2, k]
    elif type_result in ('HResidual', 'CMI'):
        ## Computing H(Y|X(k2)) - H(Y|X(k1),X(k2))
        ### entropy[k1,k2] = H(Y|X[k2]) - H(Y|X[k1], X[k2]) = I(Y, X[k1] | X[k2])
        # print(HYXX.shape, np.expand_dims(HYX, 0).shape)
        # entropy = np.expand_dims(HYX, 0) - HYXX
        entropy = np.zeros((G, G))
        for k1 in range(G):
            entropy[k1, :] = HYX - HYXX[k1, :] + HXX[k1, :] - HX
        # k10 = 1
        # for k1 in prange(G):
        #     #if k1 > k10 * G / 10: #(100 * k1 / G) % 10 < 100/G:
        #      #   print(f'Conditional entropy: {int(100 * k1 / G)}%')
        #       #  k10 += 1
        #     for k2 in range(k1+1,G):
        #             counts = np.zeros((nc, 2, 2))
        #             for ll in range(N):
        #                 counts[int(Y[ll]), int(X[ll, k1]), int(X[ll, k2])] += w[int(Y[ll])]
        #             entropy[k1,k2] = (np.log(counts/N + 1e-10) * counts).sum()/N
        #
        #             counts2 = counts.sum(axis=0)
        #             entropy[k1,k2] -= (np.log(counts2/N+1e-10) * counts2).sum()/N
        # for k1 in range(G):
        #     for k2 in range(k1+1, G):
        #         entropy[k2, k1] = entropy[k1,k2] + entropy0[k1]
        #         entropy[k1,k2] += entropy0[k2]

        ## Computes MI(Y,X)
        entropy0 = HY - entropy0
    else:
        print(f'Unknown entropy type: {type_result}')
        return np.zeros(G), np.zeros((G,G)), None

    print('maxdiag', np.diag(entropy).max(), np.diag(entropy).min(), entropy.min())

    return entropy0, entropy, None
