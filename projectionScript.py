import  numpy as np
from projection import project as project, project_admm

N = 10000
r = 1
y = .1*np.random.normal(0, 1, N)
ny = np.sqrt((y**2).sum())
x0, z, ll = project_admm(y, r, maxiter=100000, alpha=.01*ny)
x = project(y, r)
print(x.sum(), (x>0).sum())
print(np.fabs(x-x0).max())