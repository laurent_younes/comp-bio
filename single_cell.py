import numpy as np
import pandas as pd
from covering import covering, getCoveringVariables
import gurobipy as grb


def grbCallback(model, where):
    if where == grb.Callback.SIMPLEX:
        print('Hello')



df = pd.read_csv('~/OneDrive - Johns Hopkins University/RESEARCH/Projects/Censoring/SY Data/SY1_matrix.csv',sep=' ')
X = df.to_numpy()
Z = X>0
J = np.logical_and(np.mean(Z, axis=1) > 0.1, np.mean(Z, axis=1) < 0.5)
print(J.sum())
m = X.mean(axis=1) + 1e-10
s = X.std(axis=1)
J = np.logical_and(J,s/m>2)
print(J.sum())
Z = Z[J, :]

# I = np.random.choice(Z.shape[1], 500, replace=False)
# Z = Z[:,I]
depth = [3,5]
alpha = []
for m in depth:
    covered = (Z.sum(axis=1) >= m)
    cv = covered.mean()
    alpha.append(max(0.05, 1-cv))
    print(f'Fraction covered size {m}: {cv:.2f}')
c = covering(Z, weights=1., minSize=depth, alpha=alpha, timeLimit=200)
genes = getCoveringVariables(c, Z.shape[1], nlevels=len(depth))
for m in genes:
    print(m)