import numpy as np
import gurobipy as grb
from covering import covering
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt


def twoLevelCovering(Z, J=5, S = None, minSize=1, alpha=0.05, maxOverlap = 1,  lbd = 1., w = None, output=None, callBack = None,
             poolSolutions=None, poolSearchMode=None, poolGap = None, timeLimit=None):
    N = Z.shape[0]
    d = Z.shape[1]
    if S is None:
        S = 2**(J-1)
    if w is None:
        w = 1 - 0.01 * np.mean(Z, axis=0)
    elif np.isscalar(w):
        w = w * np.ones(d)
    cov = grb.Model()
    if output is not None:
        cov.Params.OutputFlag=output
    if poolSolutions is not None:
        cov.Params.PoolSolutions = poolSolutions
    if poolSearchMode is not None:
        cov.Params.PoolSearchMode = poolSearchMode
    if poolGap is not None:
        cov.Params.PoolGap = poolGap
    if timeLimit is not None:
        cov.Params.TimeLimit = timeLimit
    x = cov.addVars(d, S, vtype=grb.GRB.BINARY)
    y = cov.addVars(N, vtype=grb.GRB.BINARY)
    h = cov.addVars(N, S, vtype=grb.GRB.BINARY)
    #J = cov.addVar(vtype=grb.GRB.CONTINUOUS)
    H = cov.addVar(vtype=grb.GRB.INTEGER)
    expr = grb.LinExpr()
    for k in range(N):
        expr += y[k]
    cov.addLConstr(expr, sense=grb.GRB.GREATER_EQUAL, rhs=N*(1-alpha), name='Coverage')
    for j in range(d):
        expr = grb.LinExpr()
        for s in range(S):
            expr += x[j,s]
        cov.addLConstr(expr, sense=grb.GRB.LESS_EQUAL, rhs=maxOverlap, name= 'MaxIn'+str(j))
    for k in range(N):
        expr = grb.LinExpr()
        for j in range(d):
            for s in range(S):
                expr += Z[k, j] * x[j,s]
        expr -= minSize * y[k]
        cov.addLConstr(expr, sense=grb.GRB.GREATER_EQUAL, rhs=0, name= 'Covered'+str(k))
    for s in range(S):
        expr = grb.LinExpr()
        for j in range(d):
            expr += x[j,s]
        cov.addLConstr(expr, sense=grb.GRB.LESS_EQUAL, rhs=J, name='MaxSize' + str(s))
    # for j in range(d):
    #     #expr = grb.LinExpr()
    #     expr = (Z[:,j].sum()-2) * x[j]
    #     cov.addLConstr(expr, sense=grb.GRB.GREATER_EQUAL, rhs=0)
    for k in range(N):
        for s in range(S):
            expr = grb.LinExpr()
            for j in range(d):
                expr += Z[k, j] * x[j,s]
            expr -= h[k,s]
            cov.addLConstr(expr, sense=grb.GRB.GREATER_EQUAL, rhs=0, name='InSet' + str(k) + '_' + str(s))
    for s in range(S):
        expr = grb.LinExpr()
        for k in range(N):
            expr += h[k,s]
        expr -= H
        cov.addLConstr(expr, sense=grb.GRB.GREATER_EQUAL, rhs=0, name='Frequency' + str(s))

    expr = grb.LinExpr()
    #expr += J
    expr += H
    cov.setObjective(expr, grb.GRB.MAXIMIZE)

    if callBack is None:
        cov.optimize()
    else:
        cov.optimize(callBack)
    return cov



# def getCovering(ttype, stype, minSize = 2, alphaMax = 0.0, callback = None, force_computation= (), result='core'):
#     inp = InputData(ttype=ttype, force_computation=force_computation)
#     Z = inp.df_data[stype].to_numpy().T
#     #if stype == 'pair':
#     #    Z = Z[2:,:]
#     twoSub = (Z.mean(axis=0) >= .02)
#     Z = Z[:,twoSub]
#     covered = (Z.sum(axis=1) >= minSize)
#     err = covered.mean()
#     print('Fraction covered:', err)
#     cont_ = True
#     #Z = Z[covered,:]
#     n0 = int(0.9*Z.shape[0])
#
#     if result in ('core', 'all'):
#         w=1
#     else:
#         w=None
#
#     cov = covering(Z, alpha=max(alphaMax, 1-err), output=1, minSize=minSize, callBack=callback, poolSearchMode=2,
#                    poolSolutions=1000, poolGap=1e-10, w=w)
#     #cov = covering(Z, alpha=max(alphaMax, 1-err), w=np.ones(Z.shape[1]), output=1, minSize=minSize)
#     print(cov.ObjVal)
#
#     if result in ('core', 'all'):
#         counts = np.zeros(Z.shape[1], dtype=int)
#         cov.Params.LogToConsole = 0
#         for i in range(cov.SolCount):
#             cov.Params.SolutionNumber = i
#             counts += np.rint(cov.Xn[:Z.shape[1]]).astype(int)
#         if result == 'core':
#             J = np.nonzero(twoSub)[0][counts==cov.SolCount]
#         else:
#             J = np.nonzero(twoSub)[0][counts > 0]
#     else:
#         covx = np.rint(np.array(cov.x[:Z.shape[1]])).astype(bool)
#         J = np.nonzero(twoSub)[0][covx]
#     print(inp.df_data[stype].index[J])
#     return J
#
# def getCovering2(ttype, stype, J=5, S=10):
#     inp = InputData(ttype=ttype)
#     Z = inp.df_data[stype].to_numpy().T
#     twoSub = (Z.mean(axis=0) >= .02)
#     Z = Z[:,twoSub]
#     minSize = 1
#     covered = (Z.sum(axis=1) >= minSize)
#     err = covered.mean()
#     print('Fraction covered:', err)
#     cont_ = True
#     #Z = Z[covered,:]
#     n0 = int(0.9*Z.shape[0])
#     alphaMax = 0.00
#
#     cov0 = covering(Z, w=1, alpha=max(alphaMax, 1-err), output=1, minSize=minSize)
#     cov = twoLevelCovering(Z, J=J, S=S, w=1, alpha=max(alphaMax, 1-err), output=1, minSize=minSize, maxOverlap = 1, timeLimit=300)
#     #cov = covering(Z, alpha=max(alphaMax, 1-err), w=np.ones(Z.shape[1]), output=1, minSize=minSize)
#     return cov, cov0, inp

