import numpy as np
from scipy.spatial.distance import pdist, squareform
from scipy import linalg as LA
from numba import jit, prange
from sklearn import svm as sksvm
from sklearn import mixture as mx
import matplotlib
#matplotlib.use("TKAgg")
import matplotlib.pyplot as plt
import pandas as pd


@jit(nopython=True, parallel=True)
def gKernel(X,Y, scale = 1.):
    dist = np.zeros((X.shape[0], Y.shape[0]))
    for i in prange(X.shape[0]):
        for j in range(Y.shape[0]):
            dist[i,j] = ((X[i, :] - Y[j, :])**2).sum()
    return np.exp(-dist/(2*scale**2))

def cKernel(X,Y, scale = 1.):
    dist = ((X[:, np.newaxis, :] - Y[np.newaxis, :, :])**2).sum(axis=2)
    return 1/(1+dist)

def mKernel(X,Y):
    dist = np.sqrt(((X[:, np.newaxis, :] - Y[np.newaxis, :, :])**2).sum(axis=2))
    return (1+dist + (dist**2)/3) * np.exp(-dist)

def q2Kernel(X,Y):
    dist = np.sqrt(((X[:, np.newaxis, :] - Y[np.newaxis, :, :])**2).sum(axis=2))
    #dist = squareform(pdist(X, Y))
    C = (X[:, np.newaxis, :] * Y[np.newaxis, :, :]).sum(axis=2)
    return (1+C+C**2+C**3+C**4)*np.exp(-dist)

def lKernel(X,Y):
    return (X[:, np.newaxis, :]*Y[np.newaxis, :,:]).sum(axis=2)

class SVM_Primal:
    def __init__(self):
        self.b = None
        self.beta0 = None
        self.SV = None

class SVM_Dual:
    def __init__(self):
        self.beta0 = None
        self.X = None
        self.alpha = None
        self.alphaY = None
        self.kernel = lKernel
        self.scale = 1.
        self.SV = None

class SVM_Base:
    def __init__(self):
        self.b = None
        self.beta0 = None
        self.X = None
        self.alpha = None
        self.alphaY = None
        self.kernel = lKernel
        self.scale = 1.
        self.SV = None


def proju(x, u0, gamma):
    nu = np.sqrt((u0**2).sum())
    u = u0 / nu
    h = x - (u*x).sum()*u
    while (h<-1e-10).sum() > 0 or (h>(gamma+1e-10)).sum() > 0:
        h = np.minimum(np.maximum(h,0), gamma)
        h = h - (u*h).sum()*u

    h = np.minimum(np.maximum(h, 0), gamma)

    return h


class SVM(SVM_Base):
    def __init__(self, option = 'dual', kernel=gKernel, rho=0.001, C=1., tol=0.001, algorithm='ADMM',
                 balanced = True, kernel_scale_method = 'auto', zero_one=False):
        super().__init__()
        self.kernel = kernel
        self.kernel_scale_method = kernel_scale_method
        self.kscale = 1.
        self.rho = rho
        self.gamma = C
        self.tol = tol
        self.option = option
        self.algorithm = algorithm
        self.balanced = balanced
        self.zero_one = zero_one
        if self.algorithm == 'proximal':
            self.option = 'dual'

    def fit(self, X, Y, sample_weight=None):
        if self.kernel_scale_method == 'auto':
            self.kscale = np.sqrt(X.shape[1]) * np.std(X)
        elif self.kernel_scale_method == 'binary':
            self.kscale = np.sqrt(X.sum(axis=1).mean())
        else:
            self.kscale = self.kernel_scale_method

        if self.zero_one:
            YY = 2*Y-1
        else:
            YY = Y
        if self.algorithm == 'ADMM':
            if self.option == 'primal':
                self.fit_ADMM_primal(X, YY, sample_weight=sample_weight)
            else:
                self.fit_ADMM_dual(X, YY, sample_weight=sample_weight)
        else:
            self.fit_proximal_dual(X, YY, sample_weight=sample_weight)

    def fit_ADMM_dual(self, X, Y, sample_weight=None):
        N = X.shape[0]
        K = self.kernel(X.astype(float), X.astype(float), scale=self.kscale)
        YK = np.outer(Y, Y) * K
        YKI = LA.inv(self.rho * YK + np.eye(N))
        alpha = np.zeros(N)
        z = np.zeros(N)
        u = np.zeros(N)
        ones = self.rho * np.ones(N)
        Ay = np.dot(YKI, Y)
        sy = (Y * Ay).sum()
        converged = False
        if sample_weight is not None:
            gamma_ = self.gamma*sample_weight
        else:
            gamma_ = self.gamma * np.ones(N)
            if self.balanced:
                n0 = (Y > 0).sum()
                gamma_[Y > 0] *= N / n0
                gamma_[Y < 0] *= N / (N - n0)

        for k in range(10000):
            old_alpha = np.copy(alpha)
            c = (Ay * (ones + z - u)).sum() / sy
            alpha = np.dot(YKI, ones + z - u - c * Y)
            z = np.maximum(0, np.minimum(alpha + u, gamma_))
            r = alpha - z
            u += r
            if np.fabs(alpha - old_alpha).max() < self.tol:
                converged = True
                break
        print(converged, (alpha * np.dot(YK, alpha)).sum() / 2 - alpha.sum(), (alpha > gamma_ + 1e-10).sum())
        SV = np.argmin(np.fabs(alpha - gamma_ / 2))
        beta0 = Y[SV] * (1 - (YK[SV, :] * alpha).sum())

        self.beta0 = beta0
        self.alpha = alpha
        self.alphaY = alpha * Y
        self.X = X
        self.SV = np.fabs(alpha - self.gamma / 2) < self.gamma / 2 - 1e-5
        if self.kernel is lKernel:
            self.b = np.dot(alpha * Y, X)

    def fit_ADMM_primal(self, X, Y, sample_weight=None):
        N = X.shape[0]
        d = X.shape[1]
        A = np.concatenate((Y[:, np.newaxis], Y[:, np.newaxis] * X), axis=1)
        D = np.eye(d + 1)
        D[0, 0] = 0
        IA = LA.inv(self.rho * D + np.dot(A.T, A))
        beta = np.zeros(d + 1)
        u = np.zeros(N)
        z = np.zeros(N)
        if sample_weight is not None:
            gamma_ = self.gamma*sample_weight
        else:
            gamma_ = self.gamma * np.ones(N)
            if self.balanced:
                n0 = (Y > 0).sum()
                gamma_[Y > 0] *= N / n0
                gamma_[Y < 0] *= N / (N - n0)
        cost = (beta[1:] ** 2).sum() / 2 + (gamma_ * np.maximum(0, 1 - Y * (beta[0] + np.dot(X, beta[1:])))).sum()
        converged = False
        J0 = None
        for k in range(10000):
            v = np.dot(A, beta)
            vu = v + u
            J1 = vu > 1
            J2 = vu < 1 - self.rho * gamma_
            J0 = np.logical_and(~J1, ~J2)
            z[J0] = 1
            z[J1] = vu[J1]
            z[J2] = vu[J2] + self.rho * gamma_[J2]
            r = v - z
            u += r
            old_beta = np.copy(beta)
            beta = np.dot(IA, np.dot(A.T, z - u))
            old_cost = cost
            cost = (beta[1:] ** 2).sum() / 2 + (gamma_ * np.maximum(0, 1 - Y * (beta[0] + np.dot(X, beta[1:])))).sum()
            if np.fabs(beta - old_beta).max() < self.tol and np.fabs(cost - old_cost) < self.tol:
                converged = True
                break
        print(converged, cost / self.gamma)
        self.beta0 = beta[0]
        self.b = beta[1:]
        self.SV = J0

    def fit_proximal_dual(self, X, Y, sample_weight=None):
        N = X.shape[0]
        K = self.kernel(X.astype(float), X.astype(float), scale=self.kscale)
        #K = np.ones((N,N))
        #rho *= LA.eigh(K)[0].min()
        YK = np.outer(Y,Y) * K
        alpha = np.zeros(N)
        u = Y/np.sqrt(N)
        converged = False
        if sample_weight is not None:
            gamma_ = self.gamma*sample_weight
        else:
            gamma_ = self.gamma * np.ones(N)
            if self.balanced:
                n0 = (Y > 0).sum()
                gamma_[Y > 0] *= N / n0
                gamma_[Y < 0] *= N / (N - n0)
        cost = (alpha * np.dot(YK, alpha)).sum() / 2 - alpha.sum()
        for k in range(10000):
            old_alpha = np.copy(alpha)
            old_cost = cost
            grad = Y*np.dot(K, Y*alpha) - 1
            alpha = proju(alpha - self.rho*grad, u, gamma_)
            cost = (alpha * np.dot(YK, alpha)).sum() / 2 - alpha.sum()
            if np.fabs(alpha-old_alpha).max() < self.tol and np.fabs(cost-old_cost)<self.tol:
                converged = True
                break
        print(converged, (alpha * np.dot(YK, alpha)).sum() / 2 - alpha.sum(), (alpha > self.gamma+1e-10).sum())
        SV = np.argmin(np.fabs(alpha - gamma_/2))
        beta0 = Y[SV] *(1 - (YK[SV,:]*alpha).sum())

        self.beta0 = beta0
        self.alpha = alpha
        self.alphaY = alpha * Y
        self.X = X
        self.SV = np.fabs(alpha - gamma_/2) < gamma_/2 - 1e-5

    def fit_predict(self, X, Y):
        self.fit(X, Y)
        return self.predict(X)

    def predict(self, X):
        if self.algorithm=='primal':
            YY = self.predict_primal(X)
        else:
            YY = self.predict_dual(X)
        if self.zero_one:
            return (YY > 0).astype(int)
        else:
            return YY

    def predict_primal(self, X):
        Y = self.beta0 + np.dot(X, self.b)
        return Y

    def predict_dual(self, X):
        Y = self.beta0 + np.dot(self.kernel(X, self.X, scale=self.kscale), self.alphaY)
        return Y



def SVM_ADMM_OLD(X, Y, kernel=gKernel, rho=0.001, gamma=1., tol=0.001):
    N = X.shape[0]
    K = kernel(X, X)
    YK = np.outer(Y,Y) * K
    YKI = LA.inv(rho * YK + np.eye(N))
    alpha = np.zeros(N)
    z = np.zeros(N)
    u = np.zeros(N)
    ones = rho*np.ones(N)
    yn = Y/np.sqrt(N)
    converged = False
    for k in range(10000):
        old_alpha = np.copy(alpha)
        alpha = np.dot(YKI, ones + z - u)
        z = proju(alpha + u,  yn, gamma)
        r = alpha - z
        u += r
        if np.fabs(alpha-old_alpha).max() < tol:
            converged = True
            break
    print(converged, (alpha * np.dot(YK, alpha)).sum() / 2 - alpha.sum(), (alpha > gamma+1e-10).sum())
    SV = np.argmin(np.fabs(alpha - gamma/2))
    beta0 = Y[SV] *(1 - (YK[SV,:]*alpha).sum())

    svm = SVM_Dual()
    svm.kernel = kernel
    svm.beta0 = beta0
    svm.alpha = alpha
    svm.alphaY = alpha * Y
    svm.X = X
    svm.SV = np.fabs(alpha - gamma/2) < gamma/2 - 1e-5

    return svm



def SVM_predict_primal(X, svm):
    Y = svm.beta0 + np.dot(X, svm.b)
    return Y



