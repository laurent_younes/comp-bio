import numpy as np
import pandas as pd
from covering import cond_entropy, quadratic_covering, quadratic_covering_proximal, quadratic_covering_greedy, cmim2
import xgboost as xgb
from sklearn.utils.class_weight import compute_sample_weight, compute_class_weight
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt


fileIn = 'Data/BR0_data.csv'
counts0 = pd.read_csv(fileIn, index_col=0)
meta = pd.read_csv('Data/BR0_meta.csv', index_col=0)
counts = counts0.copy()
_, s = np.unique(meta['ER_Status'], return_inverse=True)
counts['ER_Status'] = s
_, s = np.unique(meta['PR_Status'], return_inverse=True)
counts['PR_Status'] = s
_, s = np.unique(meta['HER2_Status'], return_inverse=True)
counts['HER2_Status'] = s
_, s = np.unique(meta['Age'], return_inverse=True)
counts['Age'] = s

dfgenes = pd.DataFrame(columns=counts.columns, dtype=int)
dfgenes.loc['sum', :] = 0
dfseeds = pd.DataFrame(columns=counts.columns, dtype=int)
dfseeds.loc['sum', :] = 0

# birthAndAgeCls = np.unique(birthAndAge)
# birthCls = np.unique(birth)
# ageCls = np.unique(age)

labels = meta['ChemoResponse'].to_numpy()
cls, Y0 = np.unique(labels, return_inverse=True)
# cls = np.unique(Y0)
genes0 = counts.columns
counts = counts.to_numpy()
testData = True
globalPanelSize = 100
nseeds = 10
panelSize = 100
tr = 'HResidual'
method = 'cmim'
classifier = RandomForestClassifier #SVC #xgb.XGBClassifier
#method = 'quadratic'

balanced = True
minSize = 1
minCount = 1
p = 1e-4
nc = cls.shape[0]
d = counts.shape[1]
geneTotal = np.zeros(d)
if testData:
    repeats = 25
    combinedRate = np.zeros(repeats)
else:
    repeats = 1

correctAvg = 0
correctAvg0 = 0
correctAvg2 = 0
correctAvg20 = 0
sensitivityAvg = 0
specificityAvg = 0
sensitivityAvg0 = 0
specificityAvg0 = 0

for nrep in range(repeats):
    print(f'\n\n Repeat {nrep+1}')
    if testData:
        inTest = np.zeros(labels.shape[0], dtype=bool)
        for l in cls:
            J = np.nonzero(Y0 == l)[0]
            J0 = np.random.choice(J.shape[0], J.shape[0]//5)
            inTest[J[J0]] = True
        inTrain = np.invert(inTest)
        Y = Y0[inTrain]
        X = counts[inTrain, :]
        Ytest = Y0[inTest]
        Xtest = counts[inTest, :]
        print(f'{Y.shape[0]} training samples; {(Y==1).sum()} positive, {(Y==0).sum()} negative')
        print(f'{Ytest.shape[0]} test samples; {(Ytest==1).sum()} positive, {(Ytest==0).sum()} negative')
    else:
        Y = Y0
        X = counts
        Ytest = None
        Xtest = None

    # sample_weight = np.zeros(Y.shape)
    # sample_weight[Y==0] = 1/np.mean(Y==0)
    # sample_weight[Y==1] = 1/np.mean(Y==1)
    # sample_weight /= sample_weight.sum()
    sample_weight = compute_sample_weight('balanced', y=Y)
    class_weight = compute_class_weight('balanced', classes=cls, y=Y)
    Z = np.zeros((X.shape))
    km = []
    for j in range(X.shape[1]):
        km.append(KMeans(n_clusters=2, n_init=1))
        Z[:, j] = km[j].fit_predict(X[:,j][:, None])
    # med = np.quantile(X, 0.5, axis=0)
    # Z = X >= med
    Z0 = Z
    genes = genes0

    if testData:
        #pred = xgb.XGBClassifier(n_estimators=50, sample_weight=sample_weight)
        pred = classifier()
        # _, YYtest = np.unique(Ytest, return_inverse=True)
        pred.fit(Z0, Y, sample_weight=sample_weight)
        Ypred = pred.predict(Z0)
        specificity = np.logical_and(Ypred == 1, Y==1).sum() / (Y==1).sum()
        sensitivity = np.logical_and(Ypred == 0, Y==0).sum() / (Y==0).sum()
        print(f'specificity: {specificity:.3f}; sensitivity: {sensitivity:.3f}')
        print(f'Good classification no selection training (LP): {np.equal(Ypred, Y).mean():.2f}')
        # Z0Test = Xtest[:, VarSelect] >= minCount
        Z0Test = np.zeros(Xtest.shape)
        for j in range(X.shape[1]):
            Z0Test[:, j] = km[j].predict(Xtest[:, j][:, None])
        #Z0Test = Xtest >= med
        Ypred = pred.predict(Z0Test)
        specificity = np.logical_and(Ypred == 1, Ytest==1).sum() / (Ytest==1).sum()
        sensitivity = np.logical_and(Ypred == 0, Ytest==0).sum() / (Ytest==0).sum()
        correct = (specificity+sensitivity)/2 #np.equal(Ypred, Ytest).mean()
        correctAvg0 += correct
        correctAvg20 += correct**2
        mn = correctAvg0/(nrep+1)
        stdev = np.sqrt(correctAvg20/(nrep+1) - mn**2)
        specificityAvg0 += specificity
        sensitivityAvg0 += sensitivity
        print(f'Good classification test no selection (LP): {correct:.3f}; Average: {mn: .3f} ({stdev:.3f})')
        print(f'specificity: {specificity:.3f} (avg: {specificityAvg0/(nrep+1):.3f}); sensitivity: {sensitivity:.3f} (avg: {sensitivityAvg0/(nrep+1):.3f})')


    if method == 'cmim':
        entropy0_, entropy_, selection_ = cond_entropy(Y, Z0, type_result=tr, selectThreshold = 0.99, maxSelect=200,
                                                        balanced=True)
        J1 = cmim2(entropy_, entropy0_, globalPanelSize, nseeds=nseeds, timeLimit=300, nodeLimit=None)
        J10_ = selection_[J1['panel']]
        start = np.zeros(Z0.shape[1])
        start[J10_] = 1
    else:
        start = None

    entropy0, entropy, selection = cond_entropy(Y, Z0, type_result=tr, selectThreshold=0.99, maxSelect=2000,
                                                       balanced=True)
    # plt.figure()
    # plt.hist(entropy0.ravel())
    # plt.title('entropy0')
    # plt.figure()
    # plt.hist(entropy.ravel())
    # plt.title('entropy')
    # plt.show()

    if method == 'cmim':
        J1 = cmim2(entropy, entropy0, globalPanelSize, nseeds=nseeds, timeLimit=1000, start = start[selection], nodeLimit=1000)
        J10 = selection[J1['panel']]
        J11 = selection[J1['seeds']]
    else:
        _, x_ = quadratic_covering_proximal(entropy, globalPanelSize, type_result=tr, weight=0.)
        J1, x_ = quadratic_covering_proximal(entropy, globalPanelSize, type_result=tr, weight=10., init=x_)
        x = np.zeros(Z0.shape[1])
        #J0 = selection[J]
        J10 = selection[J1]
        J11 = None
        x[selection] = x_
        J10 = J10[np.argsort(x[J10])][::-1]
    dfgenes.loc[nrep+1, :] = 0
    Jcount = np.zeros(J10.shape)
    for l, g in enumerate(J10):
        geneTotal[g] += 1
        Jcount[l] = geneTotal[g]
    J10 = J10[np.argsort(Jcount)][::-1]

    if J11 is not None:
        s = 'Seeds: '
        for l, g in enumerate(J11):
            dfseeds.loc[nrep + 1, genes[g]] = 1
            dfseeds.loc['sum', genes[g]] = geneTotal[g]
            s += genes[g] + f' ({geneTotal[g]}) '
            if (l + 1) % 10 == 0 and (l + 1) != len(J11):
                s += '\n'
        print(s)

    s='Panel: '
    for l, g in enumerate(J10):
        dfgenes.loc[nrep+1, genes[g]] = 1
        dfgenes.loc['sum', genes[g]] = geneTotal[g]
        s += genes[g] + f' ({geneTotal[g]}) '
        if (l + 1) % 10 == 0 and (l + 1) != len(J10):
            s += '\n'
    print(s)
    dfgenes.transpose().to_csv(f'gene_panel_counts_{globalPanelSize}_{nseeds}.csv')
    if J11 is not None:
        dfgenes.transpose().to_csv(f'gene_seeds_counts_{globalPanelSize}_{nseeds}.csv')

    if testData:
        #pred = xgb.XGBRFClassifier(n_estimators=50)
        pred = classifier()

        X_ = Z0[:, J10]
        # _, YYtest = np.unique(Ytest, return_inverse=True)
        pred.fit(X_, Y, sample_weight=sample_weight)
        Ypred = pred.predict(X_)
        specificity = np.logical_and(Ypred == 1, Y==1).sum() / (Y==1).sum()
        sensitivity = np.logical_and(Ypred == 0, Y==0).sum() / (Y==0).sum()
        print(f'good classification training (LP): {np.equal(Ypred, Y).mean():.2f} {specificity:.3f} {sensitivity:.3f}')
        # Z0Test = Xtest[:, VarSelect] >= minCount
        # Z0Test = Xtest >= med
        X_ = Z0Test[:, J10]
        Ypred = pred.predict(X_)
        #correct = np.equal(Ypred, Ytest).mean()
        specificity = np.logical_and(Ypred == 1, Ytest==1).sum() / (Ytest==1).sum()
        sensitivity = np.logical_and(Ypred == 0, Ytest==0).sum() / (Ytest==0).sum()
        correct = (specificity + sensitivity) / 2
        correctAvg += correct
        correctAvg2 += correct**2
        mn = correctAvg/(nrep+1)
        stdev = np.sqrt(correctAvg2/(nrep+1) - mn**2)
        specificityAvg += specificity
        sensitivityAvg += sensitivity
        print(f'Good classification test (LP): {correct:.3f}; Average: {mn: .3f} ({stdev/np.sqrt(nrep+1):.3f})')
        print(f'specificity: {specificity:.3f} (avg: {specificityAvg/(nrep+1):.3f}); sensitivity: {sensitivity:.3f} (avg: {sensitivityAvg/(nrep+1):.3f})')


