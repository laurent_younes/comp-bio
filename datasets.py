import numpy as np
from pathlib import Path
import pandas as pd
import h5py
from numba.scripts.generate_lower_listing import description
from sklearn.cluster import KMeans
import keras


def mnist_clusters(images, labels, imTest, labTest, N, NTe, nclusters = 10, patchsize=3, readh5 = None, saveh5=None):
    foundh5 = False
    if readh5 is not None:
        f = Path(readh5)
        if f.is_file():
            with h5py.File(readh5) as f5:
                x0Tr = f5['x0Tr']
                x1Tr = f5['x1Tr']
                x0Te = f5['x0Te']
                x1Te = f5['x1Te']
                varNames = f5['varNames']
                foundh5 = True

    if not foundh5:
        X0 = np.zeros(((28-patchsize)**2*images.shape[0], patchsize**2))
        kx = 0
        for k in range(images.shape[0]):
            for i in range(28-patchsize):
                for j in range(28-patchsize):
                    X0[kx, :] = np.ravel(images[k,i:i+patchsize,j:j+patchsize])
                    kx += 1
        kme = KMeans(n_clusters = nclusters)
        kme.fit(X0)
        d = nclusters*(28-patchsize)**2
        cls = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        x0Tr = np.zeros((len(cls) * N, d))
        x1Tr = np.zeros(len(cls) * N, dtype=int)
        k0 = np.zeros(10, dtype=int)

        X0_ = np.zeros(((28-patchsize)**2, patchsize**2))
        kk = 0
        for k in range(images.shape[0]):
            if labels[k] in cls and k0[labels[k]] < N:
                kx = 0
                for i in range(28-patchsize):
                    for j in range(28-patchsize):
                        X0_[kx, :] = np.ravel(images[k,i:i+patchsize,j:j+patchsize])
                        kx += 1
                X_ = kme.predict(X0_)
                kx = 0
                for i in range(28-patchsize):
                    for j in range(28-patchsize):
                        x0Tr[kk,nclusters*kx+X_[kx]] = 1
                        kx += 1
                x1Tr[kk] = cls.index(labels[k])
                k0[labels[k]] += 1
                kk += 1

        if kk < len(cls) * N:
            x0Tr = x0Tr[0:kk, :]
            x1Tr = x1Tr[0:kk]


        x0Te = np.zeros((len(cls) * NTe, d))
        x1Te = np.zeros(len(cls) * NTe, dtype=int)
        k0 = np.zeros(10, dtype=int)
        kk = 0
        for k in range(imTest.shape[0]):
            if labTest[k] in cls and k0[labTest[k]] < N:
                kx = 0
                for i in range(28-patchsize):
                    for j in range(28-patchsize):
                        X0_[kx, :] = np.ravel(imTest[k,i:i+patchsize,j:j+patchsize])
                        kx += 1
                X_ = kme.predict(X0_)
                kx = 0
                for i in range(28-patchsize):
                    for j in range(28-patchsize):
                        x0Te[k][nclusters*kx+X_[kx]] = 1
                        kx += 1
                x1Te[kk] = cls.index(labTest[k])
                k0[labTest[k]] += 1
                kk += 1
        if kk < len(cls) * NTe:
            x0Te = x0Te[0:kk, :]
            x1Te = x1Te[0:kk]

        varNames = []
        for i in range(x0Tr.shape[1]):
            varNames.append(f'{i}')

    if not foundh5 and saveh5 is not None:
        with h5py.File(saveh5, 'w') as f5:
            f5.create_dataset('x0Tr', data=x0Tr)
            f5.create_dataset('x1Tr', data=x1Tr)
            f5.create_dataset('x0Te', data=x0Te)
            f5.create_dataset('x1Te', data=x1Te)
            f5.create_dataset('varNames', data=varNames)

    return x0Tr, x1Tr, x0Te, x1Te, varNames
        
        
def mnist_cooccurrences(images, labels, imTest, labTest, N, NTe):
    ind = np.indices(images[0, :, :].shape)
    dst = np.sqrt(((ind[:, :, :, None, None] - ind[:, None, None, :, :]) ** 2).sum(axis=0))
    D = ind.shape[1] * ind.shape[2]
    dst = np.reshape(dst, (D, D))
    select = np.logical_and(dst < 3, dst > 0)
    print(f'MNIST VARIABLES: {select.sum()}')
    Isel, Jsel = np.nonzero(select)
    nsel = select.sum()
    d = 3 * select.sum()
    cls = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    x0Tr = np.zeros((len(cls) * N, d))
    x1Tr = np.zeros(len(cls) * N, dtype=int)
    k0 = np.zeros(10, dtype=int)
    kk = 0
    for k in range(len(images)):
        if labels[k] in cls and k0[labels[k]] < N:
            im = np.ravel(images[k, :, :]) > 128
            im3 = np.logical_and(im[:, None] == 0, im[None, :] == 0)[select]
            im2 = np.logical_and(im[:, None] == 0, im[None, :] == 1)[select]
            im1 = np.logical_and(im[:, None] == 1, im[None, :] == 1)[select]
            x0Tr[kk, :d] = np.concatenate((im1, im2, im3), axis=0)
            x1Tr[kk] = cls.index(labels[k])
            k0[labels[k]] += 1
            kk += 1
    if kk < len(cls) * N:
        x0Tr = x0Tr[0:kk, :]
        x1Tr = x1Tr[0:kk]

    x0Te = np.zeros((len(cls) * NTe, d))
    x1Te = np.zeros(len(cls) * NTe, dtype=int)
    k0 = np.zeros(10, dtype=int)
    kk = 0
    for k in range(len(imTest)):
        if labTest[k] in cls and k0[labTest[k]] < NTe:
            im = np.ravel(imTest[k, :, :]) > 128
            im3 = np.logical_and(im[:, None] == 0, im[None, :] == 0)[select]
            im2 = np.logical_and(im[:, None] == 0, im[None, :] == 1)[select]
            im1 = np.logical_and(im[:, None] == 1, im[None, :] == 1)[select]
            x0Te[kk, :d] = np.concatenate((im1, im2, im3), axis=0)
            x1Te[kk] = cls.index(labTest[k])
            k0[labTest[k]] += 1
            kk += 1
    if kk < len(cls) * NTe:
        x0Te = x0Te[0:kk, :]
        x1Te = x1Te[0:kk]

    if description['targetClass'] is not None:
        x0Te = np.array(x0Te == description['targetClass'], dtype=int)
        x1Te = np.array(x1Te == description['targetClass'], dtype=int)
    varNames = []
    for i in range(x0Tr.shape[1]):
        varNames.append(f'{i}')

    return x0Tr, x1Tr, x0Te, x1Te, varNames
    

    

def readDataset(description):
    name = description['name']
    if 'phenotype' in description:
        phenotype = description['phenotype']
    else:
        phenotype = None
    res = {}
    res['varNames'] = None
    res['X'] = None
    res['labels'] = None
    res['binary'] = False
    res['Xtest'] = None
    res['labelsTest'] = None

    if name == 'BRCA':
        fileIn = 'Data/BR0_data.csv'
        counts0 = pd.read_csv(fileIn, index_col=0)
        meta = pd.read_csv('Data/BR0_meta.csv', index_col=0)
        counts = counts0.copy()
        _, s = np.unique(meta['ER_Status'], return_inverse=True)
        counts['ER_Status'] = s
        _, s = np.unique(meta['PR_Status'], return_inverse=True)
        counts['PR_Status'] = s
        _, s = np.unique(meta['HER2_Status'], return_inverse=True)
        counts['HER2_Status'] = s
        _, s = np.unique(meta['Age'], return_inverse=True)
        counts['Age'] = s
        if phenotype is None:
            labels = meta['ChemoResponse'].to_numpy()
        else:
            labels = meta[phenotype].to_numpy()

        res['varNames'] = counts.columns
        res['X'] = counts.to_numpy()
        res['labels'] = labels
        res['binary'] = False
    elif name == 'BREAST_GSE':
        fileIn = 'Data/Breast_GSE45827.csv'
        counts = pd.read_csv(fileIn, index_col=0)
        res['labels'] = counts['type'].to_numpy()
        counts.drop(['type', 'samples'])
        res['varNames'] = counts.columns
        res['X'] = counts.to_numpy()
        res['binary'] = False

    elif name == 'PPCG':
        alpha = description['alpha']
        baseline = description['baseline']
        meta0 = pd.read_csv('Data/ppcg_metadata.csv', index_col=0)
        metaI = []
        for s in meta0.index:
            metaI.append(s[:8])
        u, ui = np.unique(metaI, return_index=True)
        meta = meta0.iloc[ui, :]
        div0 = pd.read_csv(f'Data/ppcg_divergence_{baseline}_{alpha:.3f}.csv', index_col=0)
        divI = []
        for s in div0.index:
            divI.append(s[:8])
        u, ui = np.unique(divI, return_index=True)
        div = div0.iloc[ui, :]
        res['varNames'] = div.columns
        res['binary'] = True
        if phenotype == 'metastasis':
            res['labels'] = meta.loc[(meta["mets_ind"] == "no mets") | (meta["mets_ind"] == "mets"), "mets_ind"]
            res['X'] = div.loc[res['labels'].index].to_numpy()
            res['labels'] = res['labels'].to_numpy()
        elif phenotype=='relapse':
            res['labels'] = meta.loc[(meta["relapse_ind"] == "no relapse") | (meta["relapse_ind"] == "relapsed"), "relapse_ind"]
            res['X'] = div.loc[res['labels'].index].to_numpy()
            res['labels'] = res['labels'].to_numpy()
    elif name == 'MNIST':
        import ssl
        ssl._create_default_https_context = ssl._create_unverified_context
        N = 60000
        NTe = 10000
        random_split = False

        (images, labels), (imTest, labTest) =  keras.datasets.mnist.load_data()
        if phenotype == 'clusters':
            x0Tr, x1Tr, x0Te, x1Te, varNames = mnist_clusters(images, labels, imTest, labTest, N, NTe,
                                                              nclusters = description['nclusters'],
                                                              patchsize=description['patchsize'],
                                                              readh5='mnist_clusters_.hdf5', saveh5='mnist_clusters.hdf5')
        elif phenotype == 'co-occurrences':
            x0Tr, x1Tr, x0Te, x1Te, varNames = mnist_cooccurrences(images, labels, imTest, labTest, N, NTe)
        else: #Should be 'pixels'
            if phenotype != 'pixels':
                print('unrecognized phenotype: ', phenotype, ', using pixels.')
            d = images.shape[1] * images.shape[2]
            select = None
            cls = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
            x0Tr = np.zeros((len(cls) * N, d))
            x1Tr = np.zeros(len(cls) * N, dtype=int)
            k0 = np.zeros(10, dtype=int)
            kk = 0
            for k in range(len(images)):
                if labels[k] in cls and k0[labels[k]] < N:
                    im = np.ravel(images[k, :, :]) > 128
                    x0Tr[kk, :] = im
                    x1Tr[kk] = cls.index(labels[k])
                    k0[labels[k]] += 1
                    kk += 1
            if kk < len(cls) * N:
                x0Tr = x0Tr[0:kk, :]
                x1Tr = x1Tr[0:kk]

            x0Te = np.zeros((len(cls) * NTe, d))
            x1Te = np.zeros(len(cls) * NTe, dtype=int)
            k0 = np.zeros(10, dtype=int)
            kk = 0
            for k in range(len(imTest)):
                if labTest[k] in cls and k0[labTest[k]] < NTe:
                    im = np.ravel(imTest[k, :, :]) > 128
                    x0Te[kk, :] = im
                    x1Te[kk] = cls.index(labTest[k])
                    k0[labTest[k]] += 1
                    kk += 1
            if kk < len(cls) * NTe:
                x0Te = x0Te[0:kk, :]
                x1Te = x1Te[0:kk]
            varNames = []
            for i in range(x0Tr.shape[1]):
                varNames.append(str(i//28) + ' ' + str(i % 28))

        if 'class' in description and description['class'] in (0,1,2,3,4,5,6,7,8,9):
            x1Tr = (x1Tr == description['class']).astype(int)
            x1Te = (x1Te == description['class']).astype(int)

        res['X'] = x0Tr
        res['labels'] = x1Tr
        res['Xtest'] = x0Te
        res['labelsTest'] = x1Te
        res['binary'] = True
        res['varNames'] = varNames
    elif name == 'reuters':
        import ssl
        ssl._create_default_https_context = ssl._create_unverified_context
        num_words = description['num_words']
        oov_char = '0'
        (images, labels), (imTest, labTest) =  keras.datasets.reuters.load_data(num_words=num_words, oov_char='0')
        x0Tr = np.zeros((len(images), num_words))
        for i in range(x0Tr.shape[0]):
            for k in images[i]:
                if k != oov_char and k > 0:
                    x0Tr[i,k-1] = 1
        x0Te = np.zeros((len(imTest), num_words))
        for i in range(x0Te.shape[0]):
            for k in imTest[i]:
                if k != oov_char and k > 0:
                    x0Te[i,k-1] = 1
        res['X'] = x0Tr
        res['labels'] = np.array(labels, dtype=int)
        res['Xtest'] = x0Te
        res['labelsTest'] = np.array(labTest, dtype=int)
        res['binary'] = True
        res['varNames'] = [str(i) for i in np.arange(1, num_words+1, dtype=int)]

    return res

def plot_dataset(description, variables):
    import matplotlib.pyplot as plt
    if description['name'] == 'MNIST':
        if description['phenotype'] == 'pixels':
            x = np.zeros(len(variables))
            y = np.zeros(len(variables))
            for k, i in enumerate(variables):
                y[k] = i // 28
                x[k] = 27 - i % 28
            plt.figure()
            plt.scatter(x,y)
            plt.xlim((0,28))
            plt.ylim((0,28))

