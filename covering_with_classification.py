import numpy as np
import pandas as pd
from scipy.stats.contingency import crosstab
from entropy import entropy as compute_entropy, cond_entropy
from covering import quadratic_covering, quadratic_covering_proximal, quadratic_covering_greedy, cmim2, covering, covering_greedy
import xgboost as xgb
from sklearn.utils.class_weight import compute_sample_weight, compute_class_weight
from sklearn.svm import SVC
from SVMClassification import SVM
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from datasets import readDataset, plot_dataset
from functools import partial



data = {'name':'MNIST', 'phenotype': 'pixels', 'nclusters':10, 'patchsize':3, 'class': 6}
#data = {'name':'BRCA', 'phenotype': 'ChemoResponse'}
#data = {'name':'PPCG', 'phenotype': 'relapse', 'baseline':'benign', 'alpha':0.005}
#method = {'name':'cmim', 'typeEntropy': 'HResidual', 'panelSize':100, 'nseeds':10}
method = {'name':'HCovering', 'typeEntropy': 'HResidual', 'panelSize':95, 'panelSizeMax': 105, 'coverRate':0.1, 'nCover':1}
#classifier = partial(RandomForestClassifier, n_estimators=10)
#classifier = partial(SVC, gamma='auto')
#classifier = partial(SVM, algorithm='dual', option='dual', zero_one=True, kernel_scale_method='binary', C = 1.)
classifier = partial(xgb.XGBClassifier, enable_categorical=True)
greedy = False

res = readDataset(data)

#df = pd.DataFrame(data=res['X'], columns=res['varNames'])
#df.insert(0, 'Y', res['labels'])
#foo = df.groupby(level=0).sum()

dfgenes = pd.DataFrame(columns=res['varNames'], dtype=int)
dfgenes.loc['sum', :] = 0
dfseeds = pd.DataFrame(columns=res['varNames'], dtype=int)
dfseeds.loc['sum', :] = 0

labels = res['labels']
_, Y0 = np.unique(labels, return_inverse=True)
cls = np.unique(Y0)
genes0 = res['varNames']
counts = res['X']
testData = True
# globalPanelSize = 100
# nseeds = 10
# panelSize = 100
# tr = 'HResidual'
balanced = True
if 'nseeds' in method:
    seeds_filename = f"gene_seeds_counts_{method['name']}_{method['nseeds']}"
    for k in data.keys():
        seeds_filename += f"{data[k]}"
    seeds_filename += '.csv'
else:
    seeds_filename = ''
panel_filename = f"gene_panel_counts_{method['name']}_{method['panelSize']}"
for k in data.keys():
    panel_filename += f"_{data[k]}"
panel_filename += '.csv'

#method = 'HCovering'
#method = 'quadratic'

minSize = 1
minCount = 1
p = 1e-4
nc = cls.shape[0]
d = counts.shape[1]
geneTotal = np.zeros(d)
if testData and res['Xtest'] is None:
    repeats = 50
    combinedRate = np.zeros(repeats)
else:
    repeats = 1

correctAvg = 0
correctAvg0 = 0
correctAvg2 = 0
correctAvg20 = 0
TPRAvg = 0
TNRAvg = 0
TPRAvg0 = 0
TNRAvg0 = 0
PPVAvg = 0
NPVAvg = 0
PPVAvg0 = 0
NPVAvg0 = 0

for nrep in range(repeats):
    print(f'\n\n Repeat {nrep+1}')
    if testData and res['Xtest'] is None:
        inTest = np.zeros(labels.shape[0], dtype=bool)
        for l in cls:
            J = np.nonzero(Y0 == l)[0]
            J0 = np.random.choice(J.shape[0], J.shape[0]//5, replace=False)
            inTest[J[J0]] = True
        inTrain = np.invert(inTest)
        Y = Y0[inTrain]
        X = counts[inTrain, :]
        Ytest = Y0[inTest]
        Xtest = counts[inTest, :]
        print(f'{Y.shape[0]} training samples; {(Y==1).sum()} positive, {(Y==0).sum()} negative')
        print(f'{Ytest.shape[0]} test samples; {(Ytest==1).sum()} positive, {(Ytest==0).sum()} negative')
    elif testData:
        Y = Y0
        X = counts
        Ytest = res['labelsTest']
        Xtest = res['Xtest']
    else:
        Y = Y0
        X = counts
        Ytest = None
        Xtest = None

    sample_weight = compute_sample_weight('balanced', y=Y)
    class_weight = compute_class_weight('balanced', classes=cls, y=Y)
    if res['binary']:
        Z = np.abs(X)
    else:
        Z = np.zeros((X.shape))
        # km = []
        km = np.zeros((2, X.shape[1]))
        km_iter = 100
        km[1, :] = np.quantile(X, 0.75, axis=0)
        km[0, :] = np.quantile(X, 0.25, axis=0)
        km_old = np.copy(km)
        for it in range(km_iter):
            J = (np.abs(X - km[ [0], :]) < np.abs(X-km[[1], :]))*sample_weight[:, None]
            n0 = np.maximum(J.sum(axis=0), 1e-5)
            km[0, :] = (X*J).sum(axis=0)/n0
            J = (np.abs(X - km[ [0], :]) > np.abs(X-km[[1], :]))*sample_weight[:, None]
            n1 = np.maximum(J.sum(axis=0), 1e-5)
            km[1, :] = (X*J).sum(axis=0)/n1
            if (np.abs(km - km_old).max()) < 1e-8:
                break
            km_old = np.copy(km)

        Z = np.abs(X - km[ [0], :]) > np.abs(X-km[[1], :])

        # for j in range(X.shape[1]):
        #     km.append(KMeans(n_clusters=2, n_init=1))
        #     J = np.nonzero(X[:, j] > 1e-10)[0]
        #     km[j].fit(X[J,j][:, None])
        #     Z[:, j] = km[j].predict(X[:,[j]])
    Z0 = Z
    genes = genes0


#    if testData:


    maxSelect = 5000
    if method['name'] == 'cmim':
        entropy0_, entropy_, selection_ = cond_entropy(Y, Z0, type_result=method['typeEntropy'],
                                                       selectThreshold = 0.99, maxSelect=200, balanced=balanced)
        J1 = cmim2(entropy_, entropy0_, method['panelSize'], nseeds=method['nseeds'], timeLimit=300, nodeLimit=None)
        J10_ = selection_[J1['panel']]
        start = np.zeros(Z0.shape[1])
        start[J10_] = 1
        maxSelect = 2000
    # elif method == 'HCovering':
    #     entropy0_, _, selection_ = cond_entropy(Y, Z0, type_result='Univariate', selectThreshold = 0.99, maxSelect=2000,
    #                                                     balanced=True)
    else:
        start = None

    entropy0, entropy, selection = cond_entropy(Y, Z0, type_result=method['typeEntropy'], selectThreshold=1.0,
                                                maxSelect=maxSelect, balanced=balanced)
    # plt.figure()
    # plt.hist(entropy0.ravel())
    # plt.title('entropy0')
    # plt.figure()
    # plt.hist(entropy.ravel())
    # plt.title('entropy')
    # plt.show()

    if method['name'] == 'cmim':
        cm = cmim2(entropy, entropy0, method['panelSize'], nseeds=method['nseeds'], timeLimit=1000,
                   start = start[selection], nodeLimit=1000)
        J10 = selection[cm['panel']]
        J11 = selection[cm['seeds']]
    elif method['name'] == 'HCovering':
        ## Defining a covering based on subsets A(i) containing all j's such that
        #  entropy[j,i] = I(Y,X(i), X(j)) - I(Y, X(i)) < epsilon
        ## weights are given by I(Y, X(i))
        H = compute_entropy(Y, with_correction=False)

        alpha = .0
        # epsilon = np.quantile(entropy, np.minimum(np.linspace(0, 1, 100) + 1 / entropy.shape[0], 1.))#, axis=0)
        # ie = 1
        # while alpha > method['coverRate']:
        #     ## jth subset is the jth column of G: G[i,j] = 1 if i in A(j)
        #     #G = np.zeros(entropy.shape, dtype=int)
        #     G = entropy < epsilon[ie]  #[j]
        #     for j in range(G.shape[1]):
        #         G[j,j] = 0
        #     ## Fraction of rows belonging to no A(j)
        #     alpha = np.mean(G.sum(axis = 1) < method['nCover'])
        #     #q += 0.01
        #     ie += 1
        # print(f'Minimum alpha: {alpha:.4f} {G.mean():.4f} epsilon = {epsilon[ie]:.4f}')
        # J1 = covering(G, minSize=method['nCover'], alpha=alpha, weights=H - entropy0, timeLimit=1000)

        epsilonMax = entropy.max(axis=0).min()
        epsilonMin = entropy.min()


        ## Short exploration to determine epsilon providing a panel size in the targeted window
        epsilon = (epsilonMax + epsilonMin)/2

        # Entropy[i,j] = H(Y|Xj) - H(Y|Xi,Xj)
        # sets for covering: {i: entropy[i,j] < epsilon} = columns[j]
        G = entropy < epsilon #[j]
        if greedy:
            J1= covering_greedy(G, minSize=method['nCover'],  weights=H - entropy0)
            ncov = J1.sum()
        else:
            J1 = covering(G, minSize=method['nCover'], alpha=alpha, weights=H - entropy0, timeLimit=200, return_gurobi=True)
            ncov = (np.array(J1.x)[:entropy.shape[0]] > 0.5).sum()
        print(f'epsilon = {epsilon:.4f} {ncov}')

        while (epsilonMax - epsilonMin) > 1e-6 and (ncov < method['panelSize'] or ncov > method['panelSizeMax']):
            if ncov < method['panelSize']:
                epsilonMax = epsilon
            else:
                epsilonMin = epsilon
            epsilon = (epsilonMin+epsilonMax)/2
            G = entropy < epsilon  #[j]
            if greedy:
                J1 = covering_greedy(G, minSize=method['nCover'], weights=H - entropy0)
                ncov = J1.sum()
            else:
                J1 = covering(G, minSize=method['nCover'], alpha=alpha, weights=H - entropy0, timeLimit=200, return_gurobi=True)
                ncov = (np.array(J1.x)[:entropy.shape[0]] > 0.5).sum()
            print(f'epsilon = {epsilon:.4f} {ncov}')

        ### Continuing solution with computed epsilon
        if greedy:
            J10 = selection[J1==1]
        else:
            J1 = covering(G, minSize=method['nCover'], alpha=alpha, weights=H - entropy0, timeLimit=1000, restart=J1)
            J10 = selection[np.array(J1.x)[:entropy.shape[0]] > 0.5]
        print(f'epsilon = {epsilon:.4f} {J10.size}')

        # while J10.size > method['panelSize']*1.25:
        #     epsilon *= 1.1
        #     G = entropy < epsilon  #[j]
        #     J1 = covering(G, minSize=method['nCover'], alpha=alpha, weights=H - entropy0, timeLimit=1000)
        #     J10 = selection[np.array(J1.x)[:entropy.shape[0]] > 0.5]
        #     print(f'epsilon = {epsilon:.4f} {J10.size}')
        # while J10.size > 50:
        #     epsilon /= 0.75
        #     print(f'epsilon = {epsilon:.4f}')
        #     G = entropy < epsilon  #[j]
        #     J1 = covering(G, minSize=method['nCover'], alpha=alpha, weights=H - entropy0, timeLimit=1000)
        #     J10 = selection[np.array(J1.x)[:entropy.shape[0]] > 0.5]
        J11 = []
    else:
        _, x_ = quadratic_covering_proximal(entropy, method['panelSize'], type_result=method['typeEntropy'], weight=0.)
        J1, x_ = quadratic_covering_proximal(entropy, method['panelSize'], type_result=method['typeEntropy'],
                                             weight=10., init=x_)
        x = np.zeros(Z0.shape[1])
        #J0 = selection[J]
        J10 = selection[J1]
        J11 = []
        x[selection] = x_
        J10 = J10[np.argsort(x[J10])][::-1]

    dfgenes.loc[nrep+1, :] = 0
    Jcount = np.zeros(J10.shape)
    for l, g in enumerate(J10):
        geneTotal[g] += 1
        Jcount[l] = geneTotal[g]
    J10_ = J10[np.argsort(Jcount)][::-1]

    if len(J11) > 0:
        s = 'Seeds: '
        for l, g in enumerate(J11):
            dfseeds.loc[nrep + 1, genes[g]] = 1
            dfseeds.loc['sum', genes[g]] = geneTotal[g]
            s += genes[g] + f' ({geneTotal[g]}) '
            if (l + 1) % 10 == 0 and (l + 1) != len(J11):
                s += '\n'
        print(s)

    s=f'Panel: {J10_.shape[0]} variables\n'
    for l, g in enumerate(J10_):
        dfgenes.loc[nrep+1, genes[g]] = 1
        dfgenes.loc['sum', genes[g]] = geneTotal[g]
        s += genes[g] + f' ({geneTotal[g]}) '
        if (l + 1) % 10 == 0 and (l + 1) != len(J10_):
            s += '\n'
    print(s)
    if len(J11) > 0:
        dfseeds.transpose().to_csv(seeds_filename)
    dfgenes.transpose().to_csv(panel_filename)

    if testData:
        pred = classifier()
        pred.fit(Z0, Y, sample_weight=sample_weight)
        Ypred = pred.predict(Z0)
        TPR = np.logical_and(Ypred == 1, Y == 1).sum() / (Y == 1).sum()
        TNR = np.logical_and(Ypred == 0, Y == 0).sum() / (Y == 0).sum()
        print(f'\nGood classification training on all variables: {np.equal(Ypred, Y).mean():.2f} TPR: {TPR:.3f}; TNR: {TNR:.3f}')

        if res['binary']:
            Z0Test = np.fabs(Xtest)
        else:
            Z0Test = np.abs(Xtest - km[[0], :]) > np.abs(Xtest - km[[1], :])
            # Z0Test[:, j] = km[j].predict(Xtest[:, [j]])
        #Z0Test = Xtest >= med

        Ypred = pred.predict(Z0Test)
        TPR = np.logical_and(Ypred == 1, Ytest == 1).sum() / (Ytest == 1).sum()
        TNR = np.logical_and(Ypred == 0, Ytest == 0).sum() / (Ytest == 0).sum()
        PPV = np.logical_and(Ypred == 1, Ytest == 1).sum() / (Ypred == 1).sum()
        NPV = np.logical_and(Ypred == 0, Ytest == 0).sum() / (Ypred == 0).sum()
        correct = (TPR + TNR) / 2  #np.equal(Ypred, Ytest).mean()
        correctAvg0 += correct
        correctAvg20 += correct ** 2
        mn = correctAvg0 / (nrep + 1)
        stdev = np.sqrt(correctAvg20 / (nrep + 1) - mn ** 2)
        TPRAvg0 += TPR
        TNRAvg0 += TNR
        PPVAvg0 += PPV
        NPVAvg0 += NPV
        print(f'Good classification test on all variables: {correct:.3f}; Average: {mn: .3f} ({stdev:.3f})')
        print(f'TPR: {TPR:.3f} (avg: {TPRAvg0 / (nrep + 1):.3f}); TNR: {TNR:.3f} (avg: {TNRAvg0 / (nrep + 1):.3f})')
        print(f'PPV: {PPV:.3f} (avg: {PPVAvg0 / (nrep + 1):.3f}); NPV: {NPV:.3f} (avg: {NPVAvg0 / (nrep + 1):.3f})')



        #pred = xgb.XGBRFClassifier(n_estimators=50)
        pred = classifier()

        J = np.union1d(J10, J11).astype(int)
        plot_dataset(data, J)
        plt.show()
        X_ = Z0[:, J]
        # _, YYtest = np.unique(Ytest, return_inverse=True)
        pred.fit(X_, Y, sample_weight=sample_weight)
        Ypred = pred.predict(X_)
        TPR = np.logical_and(Ypred == 1, Y==1).sum() / (Y==1).sum()
        TNR = np.logical_and(Ypred == 0, Y==0).sum() / (Y==0).sum()
        print(f'\nGood classification training on selected variables: {np.equal(Ypred, Y).mean():.2f} {TPR:.3f} {TNR:.3f}')
        # Z0Test = Xtest[:, VarSelect] >= minCount
        # Z0Test = Xtest >= med
        X_ = Z0Test[:, J]
        Ypred = pred.predict(X_)
        TPR = np.logical_and(Ypred == 1, Ytest==1).sum() / (Ytest==1).sum()
        TNR = np.logical_and(Ypred == 0, Ytest==0).sum() / (Ytest==0).sum()
        PPV = np.logical_and(Ypred == 1, Ytest==1).sum() / (Ypred==1).sum()
        NPV = np.logical_and(Ypred == 0, Ytest==0).sum() / (Ypred==0).sum()
        correct = (TPR+TNR)/2 #np.equal(Ypred, Ytest).mean()
        correctAvg += correct
        correctAvg2 += correct**2
        mn = correctAvg/(nrep+1)
        stdev = np.sqrt(correctAvg2/(nrep+1) - mn**2)
        TPRAvg += TPR
        TNRAvg += TNR
        PPVAvg += PPV
        NPVAvg += NPV
        print(f'Good classification test on selected variables: {correct:.3f}; Average: {mn: .3f} ({stdev:.3f})')
        print(f'TPR: {TPR:.3f} (avg: {TPRAvg/(nrep+1):.3f}); TNR: {TNR:.3f} (avg: {TNRAvg/(nrep+1):.3f})')
        print(f'PPV: {PPV:.3f} (avg: {PPVAvg/(nrep+1):.3f}); NPV: {NPV:.3f} (avg: {NPVAvg/(nrep+1):.3f})')
        #correct = np.equal(Ypred, Ytest).mean()


