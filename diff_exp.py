import numpy as np
import numpy.random
from numba import jit, prange
from scipy.stats import ttest_ind, ranksums
from fdr import fdr
from sklearn.cross_decomposition import PLSCanonical


#@jit(nppython=True)
def diff_exp(X, labels, test = 'W', perm = None, PLS= True):
    Z, indices = np.unique(labels, return_inverse=True)
    if Z.size != 2:
        print('TSP: more than two classes provided. Only using first two')
    J0 = indices==0
    J1 = indices==1
    X00 = X[J0, :]
    X01 = X[J1, :]
    d = X.shape[1]
    if PLS:
        N0 = X00.shape[0]
        R0 = X00 - X00.mean(axis=0)[None, :]
        R1 = X01 - X01.mean(axis=0)[None, :]
        R = np.concatenate((R0, R1), axis=0)
        pls = PLSCanonical(n_components=1)
        pls.fit(X, R)
        XX = X- np.dot(pls.transform(X), pls.x_loadings_.T)
        X0 = XX[:N0,:]
        X1 = XX[N0:, :]
    else:
        X0 = X00
        X1 = X01

    s = np.zeros(d)
    p = np.zeros(d)
    if test == 't':
        for i in range(d):
            s[i], p[i] = ttest_ind(X0[:,i], X1[:,i], equal_var=False)
            if np.std(X[:,i]) < 1e-10:
                s[i] = 0
                p[i] = 1
            if np.isnan(s[i]):
                s[i] = 0
                p[i] = 1
    else:
        for i in range(d):
            s[i], p[i] = ranksums(X0[:,i], X1[:,i])

    PLS_perm = PLS
    if perm is not None:
        print(f'Max Test: {np.fabs(s).max():.4f}')
        max_stat = np.zeros(perm)
        N = X.shape[0]
        N0 = X0.shape[0]
        for k in range(perm):
            print(f'Permutation = {k}')
            sp = np.zeros(d)
            J = numpy.random.permutation(N)
            J0 = J[:N0]
            J1 = J[N0:]
            Y00 = X[J0, :]
            Y01 = X[J1, :]
            if PLS_perm:
                N0 = Y00.shape[0]
                R0 = Y00 - Y00.mean(axis=0)[None, :]
                R1 = Y01 - Y01.mean(axis=0)[None, :]
                R = np.concatenate((R0, R1), axis=0)
                Y = np.concatenate((Y00, Y01), axis=0)
                pls = PLSCanonical(n_components=1)
                pls.fit(Y, R)
                Y -= np.dot(pls.transform(Y), pls.x_loadings_.T)
                Y0 = Y[:N0, :]
                Y1 = Y[N0:, :]
            else:
                Y0 = Y00
                Y1 = Y01
            if test == 't':
                for i in range(d):
                    sp[i], pi = ttest_ind(Y0[:,i], Y1[:,i], equal_var=False)
                    if np.std(X[:, i]) < 1e-10:
                        sp[i] = 0
                    if np.isnan(sp[i]):
                        sp[i] = 0
            else:
                for i in range(d):
                    sp[i], pi = ranksums(Y0[:,i], Y1[:,i])
            max_stat[k] = np.fabs(sp).max()
            if k>5:
                print(np.quantile(max_stat[:k], np.ceil(0.95*(k+1))/(k+1)))

        for i in range(d):
            p[i] = (1+(max_stat > np.fabs(s[i])).sum())/(1+perm)


    J = np.argsort(p)

    return J, s[J], p[J]

