import numpy as np
import scipy.stats as stats
from scipy.signal import periodogram, csd
from scipy.fft import fft, ifft
import matplotlib
matplotlib.use("qt5agg")
import matplotlib.pyplot as plt


def coherence(x,y):
    f, cxy = csd(x,y)
    cxx = csd(x,x)[1]
    cyy = csd(y,y)[1]
    u =  np.abs(ifft(cxy/np.sqrt(cxx*cyy)))
    return f, u


def random_signal(freq, coeff, n=2, T=1, dt=0.01, sigma = 0.1):
    t = np.arange(0, T, dt)
    N = t.size
    nf = len(freq)
    freq = np.array(freq)
    coeff = np.array(coeff)
    X = np.zeros((N, n))
    for k, om in enumerate(freq):
        X += coeff[k,:] * np.cos(2*np.pi * om * t[:, None])*(1+om/10)
    X += stats.norm.rvs(0, sigma, X.shape)

    return t, X

N = 10
freq = np.arange(5, 51)
a0 = np.zeros(freq.size)
a1 = np.zeros(freq.size)
a0[0] = 5
a0[10] = 5
corr0 = np.zeros(N)
dst0 = np.zeros(N)
corr1 = np.zeros(N)
dst1 = np.zeros(N)
sigCoeff0 = .25
sigCoeff1 = np.sqrt(a0**2 - a1**2+ sigCoeff0**2)
sigNoise = .5
nr = 100
powr = 0
powr2 = 0
dt = 0.001
mc0 = 0
mc1 = 0
alpha = 0.05/21
nps = 256
#coeff = stats.multivariate_normal.rvs(a1, sigCoeff1 ** 2, size=2).T
# coeff = stats.multivariate_normal.rvs(a0, sigCoeff0, size=2).T
# t, X0 = random_signal(freq, coeff, sigma=sigNoise, dt=dt)
# f0, c0 = coherence(X0[:, 0], X0[:, 1])
# print(np.max(c0))
# plt.figure(3)
# plt.plot(np.abs(c0))
# plt.show()
# f0, p0 = periodogram(X0[:,0])
# f1, p1 = periodogram(X0[:,1])
# c00 = np.correlate(X0[:,0], X0[:,0], 'same')[-X0.shape[0]//2:]
# c11 = np.correlate(X0[:,0], X0[:,1], 'same')[-X0.shape[0]//2:]
# c01 = np.correlate(X0[:,1], X0[:,1], 'same')[-X0.shape[0]//2:]
# plt.figure(3)
# plt.subplot(411)
# plt.plot(c00)
# plt.subplot(412)
# plt.plot(c11)
# plt.subplot(413)
# plt.plot(c01)
# plt.subplot(414)
# plt.plot(c01)
# plt.show()
ep = 1e-10
for r in range(nr):
    for k in range(N):
        coeff = stats.multivariate_normal.rvs(a0, sigCoeff0**2, size = 2 ).T
        t, X0 = random_signal(freq, coeff, sigma=sigNoise, dt=dt)
        corr0[k] = np.corrcoef(X0[:, 0], X0[:, 1])[0, 1]
        f0, c0 = coherence(X0[:, 0], X0[:, 1])
        dst0[k] = (c0**2).sum()
        # f0, p0 = periodogram(X0[:, 0])
        # f1, p1 = periodogram(X0[:, 1])
        # dst0[k] = (np.fabs(p0/(p1+ep) + p1/(p0+ep) - 2)).sum() #np.fabs(np.log(p0+ep) - np.log(p1+ep)).sum()
        mc0 += corr0[k]
    for k in range(N):
        coeff = stats.multivariate_normal.rvs(a1, sigCoeff1**2, size = 2 ).T
        t, X1 = random_signal(freq, coeff, sigma=sigNoise, dt = dt)
        corr1[k] = np.corrcoef(X1[:,0], X1[:,1])[0,1]
        f1, c1 = coherence(X1[:, 0], X1[:, 1])
        dst1[k] = (c1**2).sum()
        # f0, p0 = periodogram(X1[:, 0])
        # f1, p1 = periodogram(X1[:, 1])
        # dst1[k] = (np.fabs(p0/(p1+ep) + p1/(p0+ep) - 2)).sum() #np.fabs(np.log(p0+ep) - np.log(p1+ep)).sum()
        mc1 += corr1[k]
    rs = stats.ranksums(corr0, corr1)
    powr += rs.pvalue < alpha
    rs = stats.ranksums(dst0, dst1)
    powr2 += rs.pvalue < alpha

print(f'power: {powr/nr:.4f}')
print(f'power distances: {powr2/nr:.4f}')
print(f'mean correlations: {mc0/(N*nr):.4f}  {mc1/(N*nr):.4f}')
plt.figure(1)
plt.subplot(211)
plt.plot(t, X0[:,0])
plt.subplot(212)
plt.plot(t, X0[:,1])
plt.figure(2)
plt.subplot(211)
plt.plot(t, X1[:,0])
plt.subplot(212)
plt.plot(t, X1[:,1])
plt.show()