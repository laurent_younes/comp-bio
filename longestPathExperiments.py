import numpy as np
from longest_path import longestPath


N = 500
r = 50
Z = np.random.normal(0,1,(N,N))


path = longestPath(Z, r)

print(path.X[N*N:])
X = np.reshape(np.array(path.X[:N*N]), (N, N), order='C')
I = np.nonzero(X>0.5)
for j in range(r+1):
    print(f"{I[0][j]}, {I[1][j]}")

