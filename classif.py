import numpy as np
from functools import partial
from scipy.optimize import minimize
from scipy.stats import ranksums
from scipy.spatial.distance import pdist
from sklearn import svm
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.ensemble import RandomForestClassifier
from inputData import InputData
from getCoverings import getCovering
import gurobipy as grb
from pylatex import Document, Section, Subsection, Subsubsection, Tabular, Math, TikZ, Axis

def grbCallback(model, where):
    if where == grb.CallbackClass.SIMPLEX:
        print('Hello')



if __name__=="__main__":
    n_sde = 5000
    n_sim = 20
    repeat = 30
    sl = 30
    maxVar0 = 16
    #entropyFun = distEntropy
    stypes = (('source', 'target'), ('source',), ('target',),('pair',),('pathway',))
    #stypes = ('target',)
    ttypes = ('Lung', 'Breast', 'Liver','KIRC','KIRP','Prostate', 'Colon')
    #ttypes = ('Colon',)
    doc = Document()
    for ttype in ttypes:
        inp = InputData(ttype=ttype)
        with doc.create(Section(ttype)):
            for stype_ in stypes:
                print(ttype,stype_)
                X = None
                for stype in stype_:
                    if stype == 'target':
                        ms = 5
                    else:
                        ms = 2
                    J = getCovering(ttype, stype, minSize=ms, callback = grbCallback)
                    if X is None:
                        X = inp.df_data[stype].loc[inp.df_data[stype].index[J], :].to_numpy().T
                    else:
                        X_ = inp.df_data[stype].loc[inp.df_data[stype].index[J], :].to_numpy().T
                        X = np.concatenate((X, X_), axis=1)
                    cvg = 100*np.mean(X.sum(axis=1)>=ms)
                    print(X.shape[1], 'variables', cvg, '% coverage')
                    doc.append('{0:d} variables; {1:.2f}% coverage\n'.format(X.shape[1], cvg))
                for st in inp.classes.keys():
                    cl = np.unique(inp.classes[st].labels)
                    ncl = 0
                    for i in cl:
                        I = (inp.classes[st].labels == i)
                        if I.sum() < 20:
                            inp.classes[st].labels[I] = -1
                        elif i>=0:
                            ncl += 1
                    J = inp.classes[st].labels >= 0
                    labs = inp.classes[st].labels[J]
                    if ncl > 1:
                        Y = np.array(inp.classes[st].names)[labs]
                        #clf = RandomForestClassifier(n_estimators=100, class_weight='balanced')
                        #clf = svm.SVC(class_weight='balanced', gamma=1.0)
                        clf = svm.LinearSVC(class_weight='balanced', max_iter=10000)
                        scores = cross_val_score(clf, X[J,:], Y, cv=10, scoring='balanced_accuracy')
                        result = ttype + " " + stype + " " + st + '; {1:d} classes;  ten-fold CV: {0:.2f}'.format(scores.mean(), ncl)
                        print(result)
                        doc.append(result+'\n')

                        for ni in range(len(inp.classes[st].names)):
                            for nj in range(ni+1, len(inp.classes[st].names)):
                                Ji = inp.classes[st].labels == ni
                                Jj = inp.classes[st].labels == nj
                                if Ji.sum()>=20 and Jj.sum() >=20:
                                    J2 = np.logical_or(Ji, Jj)
                                    labs = inp.classes[st].labels[J2]
                                    Y = np.array(inp.classes[st].names)[labs]
                                    clf = svm.LinearSVC(class_weight='balanced')
                                    scores = cross_val_score(clf, X[J2,:], Y, cv=10, scoring='balanced_accuracy')
                                    result = ttype + " " + stype + " " + st + ';' + inp.classes[st].names[ni] + ' vs. '+ inp.classes[st].names[nj] +';  ten-fold CV: {0:.2f}'.format(scores.mean())
                                    print(result)
                                    doc.append(result+'\n')

    doc.generate_pdf('Reports/' + 'Classification', clean_tex=False)


