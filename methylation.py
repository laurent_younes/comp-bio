import numpy as np
import scipy as sp
from scipy.sparse import coo_matrix
import pandas as pd
import matplotlib
matplotlib.use("QT5Agg")
import matplotlib.pyplot as plt
from sklearn.manifold import SpectralEmbedding, LocallyLinearEmbedding, TSNE
from sklearn.cluster import KMeans
from mpl_toolkits.mplot3d import Axes3D

MechPred = "/Users/younes/Dropbox (MechPred)/MechPred"

def plotHist(h0, fig=None):
    if fig is None:
        plt.figure()
    c = (h0[1][1:] + h0[1][:-1])/2
    w = (-h0[1][1:] + h0[1][:-1])*0.9
    plt.bar(c, h0[0], width=w)

def saveProcessedFiles(df_map, df_map2):
    df_meth = pd.read_csv(MechPred+'/DATA/BREAST/METHYLATION_450k/TCGA/TCGA_Beta.csv')
    print(df_map2.columns)
    print('min_dist', df_map['DistanceFromAnn'].min())
    df_sel = df_map.loc[:,'annot.type'] == 'hg19_genes_promoters'
    df_sel = df_map.loc[df_sel.to_numpy(),:]
    df_sel.to_csv(MechPred+'/USER/LAURENT/Signatures/CpGsAnnotatedToGenesHg19Promoters.csv', compression='gzip')
    df_sel2 = df_meth.loc[df_sel['id'],:]
    df_sel2.to_hdf(MechPred+'/USER/LAURENT/Signatures/TCGA_Beta.hdf', complevel=9, key='Promoters')
    print(df_meth.shape, df_sel2.shape )
    df_sel2 = df_meth.loc[df_map2['id'],:]
    df_sel2.to_hdf(MechPred+'/USER/LAURENT/Signatures/TCGA_Beta.hdf', complevel=9, key='Illumina')



if __name__ == "__main__":
    df_map = pd.read_csv(MechPred+'/DATA/ANNOTATIONS/CpGsAnnotatedToGenesHg19.txt.gz', sep='\t')
    df_map2 = pd.read_csv(MechPred+'/DATA/ANNOTATIONS/IlluminaHumanMethylation450kanno.ilmn12.hg19.csv.gz', sep=',',index_col=0)
    df_meth = pd.read_hdf(MechPred+'/USER/LAURENT/Signatures/TCGA_Beta.hdf', key='Illumina').dropna()
    df_pheno = pd.read_csv(MechPred+'/DATA/BREAST/METHYLATION_450k/TCGA/TCGA_Pheno.csv.gz', index_col=1)
    df_rnaseq = pd.read_csv(MechPred+'/DATA/BREAST/RNASeq/TCGA/TCGA_Counts.csv.gz')


    df_map2 = df_map2.loc[:, ['UCSC_RefGene_Name','Regulatory_Feature_Name']].dropna()
    inter = df_map2.index.intersection(df_meth.index)
    df_map2 = df_map2.loc[inter,:]
    df_meth = df_meth.loc[inter,:]

    genes = []
    cpgindx = []
    geneindx = []
    noComma = np.zeros(df_map2.shape[0], dtype=bool)
    for k,g in enumerate(df_map2['UCSC_RefGene_Name']):
        for gg in np.unique(g):
            if gg not in genes:
                genes.append(gg)
            cpgindx.append(k)
            geneindx.append(genes.index(gg))


    nindx = len(cpgindx)
    ng = len(genes)
    nc = df_map2.shape[0]
    m2g = coo_matrix((np.ones(nindx), (geneindx, cpgindx)), shape=(ng, nc))
    print(ng, 'unique genes')
    print(nc, 'CpG sites')
    maxs = np.zeros((df_meth.shape[0], ng))

    nbs = m2g.sum(axis=1)
    mgs = np.array(m2g.dot(df_meth)/nbs)
    I = np.ones(mgs.shape[0], dtype=bool)
    #I = np.intersect1d(genes,df_rnaseq.index, return_indices=True)[1]
    df2 = df_meth*df_meth
    stds = df2.mean(axis=1) - df_meth.mean(axis=1)**2
    h0 = np.histogram(nbs, bins=50)
    plotHist(h0, plt.figure(1))
    h0 = np.histogram(mgs[I,:].mean(axis=1), bins=50)
    plotHist(h0, plt.figure(2))
    h0 = np.histogram(mgs[I,:].ravel(), bins=50)
    plotHist(h0, plt.figure(3))
    h0 = np.histogram(stds, bins=50)
    plotHist(h0, plt.figure(4))
    plt.pause(1)

    #saveProcessedFiles(df_map, df_map2)
    print(np.unique(df_map['annot.type']))
    print(df_map2.shape)
    classColumn = 'ER_Status_nature2012'

    sample_type = df_pheno['sample_type'].to_numpy(dtype=str)
    #print(sample_type)
    classes = np.unique(sample_type)
    for s in classes:
        print(s, (sample_type == s).sum())

    mgs_norm = mgs[:, sample_type == 'Solid Tissue Normal']
    stds = (mgs_norm**2).mean(axis=1) - mgs_norm.mean(axis=1)**2
    low_var = stds < 0.005
    print(low_var.sum(), "low variance genes")
    mgs_low = mgs[low_var, :]
    mgs_low = mgs_low[:, sample_type == 'Primary Tumor']
    df_tumor = df_pheno.loc[sample_type == 'Primary Tumor', :]


    nclusters = 6
    embedding = SpectralEmbedding(n_components=nclusters)
    #X = np.sqrt(1e-10 + mgs_low.T)
    X = np.log(1e-10 + mgs_low.T) + np.log(1 + 1e-10 - mgs_low.T)
    s2 = embedding.fit_transform(X)

    v0 = 0
    v1 = 1
    v2 = 2
    colors = ['r', 'g', 'b', 'm', 'k', '0.75']


    #aff = sp.spatial.distance.squareform(sp.spatial.distance.pdist(np.log(1e-10 + mgs_low.T)))
    #aff = ((mgs[:, np.newaxis, :] - mgs[:, :, np.newaxis])**2).mean(axis=0)
    #embedding = TSNE(n_components=3)


    kmeans = KMeans(n_clusters=nclusters, random_state=0).fit(s2)
    cls = kmeans.predict(s2)



    for statusColumn in ('PAM50_mRNA_nature2012','ER_Status_nature2012'):
        statusData = df_tumor[statusColumn]
        classes = np.unique(df_tumor[statusColumn].dropna())
        nc = classes.size
        status = []
        for cl in classes:
            status.append(statusData == cl)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        for k,st in enumerate(status):
            ax.scatter(s2[st, v0], s2[st, v1], s2[st,v2], label = classes[k], color=colors[k], marker='o')
        stNan = statusData.isna()
        ax.scatter(s2[stNan, v0], s2[stNan, v1], s2[stNan, v2], label='missing', color=colors[-1], marker='o')
        ax.legend()
        plt.title(statusColumn)
        plt.axis('off')
        plt.pause(0.1)


        cmatrix = np.zeros((nclusters,nc+1))
        for k in range(nclusters):
            for l in range(nc):
               cmatrix[k,l] = 100 * np.logical_and(cls==k, statusData == classes[l]).sum()/(cls==k).sum()
            cmatrix[k,nc] = 100 * np.logical_and(cls==k, stNan).sum()/(cls==k).sum()


        str = ''
        for l in range(nc):
            str += classes[l] + " "
        str += "missing"
        print(str)
        for k in range(nclusters):
            str = "Cluster " + "{0:d}".format(k) + ": {0:d} points -- ".format((cls==k).sum())
            for l in range(nc+1):
                str += "{0:2f}".format(cmatrix[k,l]) + " "
            print(str)



    plt.ioff()

    plt.show()
