import numpy as np

def fdr(p):
    J = np.argsort(p)
    ps = np.sort(p)
    V = p.shape[0]
    I = 1+np.arange(V)

    qvs = ps*V/I
    for i in range(V-1):
        qvs[V - i - 2] = min(qvs[V-i-1], qvs[V-i-2])
    qv = np.zeros(J.shape)
    qv[J] = qvs

    return qv