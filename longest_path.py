import numpy as np
import gurobipy as grb


def longestPath(Z, r, output = None, callBack = None,poolSolutions=None, poolSearchMode=None, poolGap = None,
                timeLimit=None):
    N = Z.shape[0]-1
    path = grb.Model()
    X = path.addMVar((N+1, N+1), vtype=grb.GRB.BINARY, name="X")
    P = path.addVars(N+1, vtype=grb.GRB.INTEGER, lb=0, ub=r, name="X")
    expr = grb.LinExpr()
    for i in range(N+1):
        for j in range(N+1):
            expr += X[i, j]*Z[i,j]

    path.setObjective(expr, grb.GRB.MAXIMIZE)

    for i in range(N):
        path.addConstr(X[i, :].sum() <= 1, name = f"start {i}")
        path.addConstr(X[:, i].sum() <= 1, name=f"end {i}")
        path.addConstr(X[:, i].sum() - X[i, :N].sum() >= 0, name=f"end {i}")
        for j in range(N):
            path.addConstr(P[j] - P[i] - 1 + N*(1-X[i,j]) >= 0, name = f"count {i} {j}")

    path.addConstr(X[N, :].sum() == 1, name=f"start path")
    path.addConstr(X[:, N].sum() == 0, name=f"end path")
    path.addConstr(X.sum()==r+1, "name: Bounded length")
    if output is not None:
        path.Params.OutputFlag=output
    if poolSolutions is not None:
        path.Params.PoolSolutions = poolSolutions
    if poolSearchMode is not None:
        path.Params.PoolSearchMode = poolSearchMode
    if poolGap is not None:
        path.Params.PoolGap = poolGap
    if timeLimit is not None:
        path.Params.TimeLimit = timeLimit


    if callBack is None:
        path.optimize()
    else:
        path.optimize(callBack)

    Y = np.reshape(np.array(path.X[:N * N]), (N, N), order='C')
    I = np.nonzero(Y > 0.5)

    return I[1]


def cmim(Z, r, output = None, callBack = None,poolSolutions=None, poolSearchMode=None, poolGap = None,
                timeLimit=None):

    N = Z.shape[0]-1
    M = Z.max()
    path = grb.Model()
    I1 = np.argsort(Z[:N, -1])
    x0 = np.zeros(N)
    x0[I1[-1]] = 1
    X0 = path.addVars(N, vtype=grb.GRB.BINARY, name="X0")
    X0.Start = x0
    x = np.zeros(N)
    X = path.addVars(N, vtype=grb.GRB.BINARY, name="X")
    x[I1[-r:-1]] = 1
    X.Start = x
    P = path.addVars(N, vtype=grb.GRB.CONTINUOUS, lb=0.0, name="P")
    expr = grb.LinExpr()
    for i in range(N):
        expr += P[i] + X0[i] * Z[i,N]

    path.setObjective(expr, grb.GRB.MAXIMIZE)

    for i in range(N):
        path.addConstr(X.sum() == r-1, name = f"in {i}")
        path.addConstr(X0.sum() == 1, name=f"first {i}")
        path.addConstr(X[i] + X0[i] <= 1, name=f"in or first {i}")
        for j in range(N):
            if j==i:
                path.addConstr(P[i] - (X[j]+X0[j]) * M <= 0, name=f"min {i} {j}")
            else:
                path.addConstr(P[i] - (X[j]+X0[j]) * Z[i,j]  - (1-X[j] - X0[j])*M <= 0, name = f"min {i} {j}")


    if output is not None:
        path.Params.OutputFlag=output
    if poolSolutions is not None:
        path.Params.PoolSolutions = poolSolutions
    if poolSearchMode is not None:
        path.Params.PoolSearchMode = poolSearchMode
    if poolGap is not None:
        path.Params.PoolGap = poolGap
    if timeLimit is not None:
        path.Params.TimeLimit = timeLimit

    # path.Params.MIPFocus = 3

    if callBack is None:
        path.optimize()
    else:
        path.optimize(callBack)

    res = np.zeros(r, dtype=int)
    res[0] = np.nonzero(np.array(path.X[:N]) > 0.5)[0][0]
    res[1:] = np.nonzero(np.array(path.X[N:2*N]) > 0.5)[0]

    return res

# def cmim2(Z, Z0, r, relax=False, output = None, callBack = None,poolSolutions=None, poolSearchMode=None, poolGap = None,
#                 timeLimit=None):
#
#     N = Z.shape[0]
#     H = Z0/r
#     print(H.min(), H.max())
#     M = Z.max()
#     path = grb.Model()
#     #I1 = np.argsort(Z[:N, -1])
#     # x = np.zeros(N)
#     if relax:
#         X = path.addVars(N, vtype=grb.GRB.CONTINUOUS, lb=0.0, ub=1.0,  name="X")
#     else:
#         X = path.addVars(N, vtype=grb.GRB.BINARY, name="X")
#     # x[I1[-r:-1]] = 1
#     # X.Start = x
#     P = path.addVars(N, vtype=grb.GRB.CONTINUOUS, lb=0.0, name="P")
#     expr = grb.LinExpr()
#     for i in range(N):
#         expr += P[i] + X[i] * H[i]
#
#     path.setObjective(expr, grb.GRB.MAXIMIZE)
#
#     for i in range(N):
#         path.addConstr(X.sum() == r, name = f"in {i}")
#         for j in range(N):
#             if j==i:
#                 path.addConstr(P[i] - X[i] * M <= 0, name=f"min {i} {j}")
#             else:
#                 path.addConstr(P[i] - X[j] * Z[i,j] - (1-X[j])*M <= 0, name = f"min {i} {j}")
#
#
#     if output is not None:
#         path.Params.OutputFlag=output
#     if poolSolutions is not None:
#         path.Params.PoolSolutions = poolSolutions
#     if poolSearchMode is not None:
#         path.Params.PoolSearchMode = poolSearchMode
#     if poolGap is not None:
#         path.Params.PoolGap = poolGap
#     if timeLimit is not None:
#         path.Params.TimeLimit = timeLimit
#
#     # path.Params.MIPFocus = 3
#
#     if callBack is None:
#         path.optimize()
#     else:
#         path.optimize(callBack)
#
#     if relax:
#         res = np.array(path.X[:N])
#     else:
#         res = np.nonzero(np.array(path.X[:N]) > 0.5)[0]
#     x = np.array(path.X[N:])
#     # opt = 0
#     # for i in res:
#     #     mi = M
#     #     for j in res:
#     #         if j != i and Z[i,j] < mi:
#     #             mi = Z[i,j]
#     #     opt += H[i] + mi
#     #
#     # opt2 = 0
#     # for i in range(r):
#     #     mi = M
#     #     for j in range(r):
#     #         if j != i and Z[i,j] < mi:
#     #             mi = Z[i,j]
#     #     opt2 += H[i] + mi
#     #
#     # print(opt, opt2)
#
#     return res, x