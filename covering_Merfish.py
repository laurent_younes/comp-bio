import numpy as np
#from numpy.char import lstrip
import pandas as pd
from sklearn.cluster import DBSCAN
from covering import covering_run, covering

docovering = True
fileIn = '/Users/younes/Development/Data/Merfish/allen_data2/MERSCOPE_counts_60252010'
if docovering:
#             '/Users/younes/Development/Data/Merfish/datasets_mouse_brain_map_BrainReceptorShowcase_Slice1_Replicate1_cell_by_gene_S1R1.csv.gz'
    counts = pd.read_csv(fileIn +'.csv', index_col=0)
    Z0 = counts.to_numpy() > 0
    prob = Z0.mean(axis =1)
    w = 1/(1e-5 + prob*(1-prob))

    minSize = 5
    alpha = (Z0.sum(axis=1) >= minSize).mean()
    cov = covering(Z0, weights=w, minSize=minSize, alpha=1-alpha)
    J = np.nonzero(np.array(cov.x)[:counts.shape[1]] > 0.5)[0]
    print(J)

    Z = counts.to_numpy()[:, J]
    print(counts.columns[J])

    df = pd.DataFrame(data=Z, columns=counts.columns[J])
    df.to_csv(fileIn + '_covering.csv')
#              '/Users/younes/Development/Data/Merfish/datasets_mouse_brain_map_BrainReceptorShowcase_Slice1_Replicate1_cell_by_covering_S1R1.csv')
else:
    Z = pd.read_csv('/Users/younes/Development/Data/Merfish/datasets_mouse_brain_map_BrainReceptorShowcase_Slice1_Replicate1_cell_by_covering_S1R1.csv').to_numpy()

db = DBSCAN(eps=5).fit(Z>0)
pass